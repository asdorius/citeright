package de.dhbw.citeright.server.report;

import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import com.itextpdf.text.BadElementException;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Image;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfAction;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.PossibleMatch;
import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.DocumentContentService;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.PossibleMatchService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.SectionsService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.essentials.mail.MailingSingleton;

/**
 * Class, which generates a report in pdf format which the data is given by the
 * document id
 * 
 * @author Moritz Ragg
 * @since 12.05.2015
 */
public class ReportPDFGenerator {
	private de.dhbw.citeright.essentials.db.Document source;
	private Document document;
	private Date date;
	private float minMeasurement = 0.1f;

	/**
	 * Generate a Report as PDF
	 * 
	 * @param documentContent_id
	 *            DocumentContent ID in the database
	 */
	public void generate(int documentContent_id) {
		boolean reportCreated = false;

		DocumentService documentService = new DocumentService();
		RoomService roomService = new RoomService();

		DocumentContentService documentContentService = new DocumentContentService();
		source = documentService.getById(documentContentService.getById(
				documentContent_id).getDocumentId());

		SectionsService dbSectionsService = new SectionsService();

		List<Section> sections = new LinkedList<>();
		sections = dbSectionsService.getSectionsOfDocument(documentContent_id);

		date = new Date();
		document = new Document();

		ByteArrayOutputStream stream = null;
		try {
			stream = new ByteArrayOutputStream();
			PdfWriter writer = PdfWriter.getInstance(document, stream);
			document.open();

			document.add(createHeaderTable());
			for (Section section : sections) {
				PdfPTable table = createContentTable(section);
				if(table != null){
				document.add(new Phrase("\n"));
				document.add(table);
				}

			}
			document.close();
			// put report into document
			source.setAuditReport(stream.toByteArray());
			// set status to finished
			source.setStatusId(3);
			documentService.update(source);

			reportCreated = true;
		} catch (DocumentException e) {
			e.printStackTrace();
		} finally {
			if (document.isOpen()) {
				document.close();
			}
			if (stream != null) {
				try {
					stream.close();
				} catch (IOException e) {
				}
			}
		}

		if (reportCreated) {
			// send Email
			UserService userService = new UserService();

			User user = userService.getById(roomService.getById(
					source.getParentId()).getOwnerId());
			int lowerThreshold = user.getLowerThreshold();
			int upperThreshold = user.getUpperThreshold();

			if (source.getPlagiarismDegree() >= (float) lowerThreshold) // upper
																		// Threshold,
																		// send
																		// detailed
																		// report
			{
				MailingSingleton mailingSingleton = MailingSingleton
						.getInstance();
				boolean detailedReport = false;
				if (source.getPlagiarismDegree() >= (float) upperThreshold) {
					detailedReport = true;
				}
				mailingSingleton.sendMailWithDocumentStatusFinished(user,
						documentContentService.getById(documentContent_id),
						detailedReport);
			}

			System.out
					.println("ReportActor finished the Job! For DocumentContent ID: "
							+ documentContent_id);
		} else {
			System.out
					.println("Report was not successfully created. For DocumentContent ID: "
							+ documentContent_id);
		}
	}

	/**
	 * Builds the header table
	 * 
	 * @return the header table
	 */
	public PdfPTable createHeaderTable() {

		PdfPTable table = new PdfPTable(3);

		PdfPCell cell;

		String path = getClass().getProtectionDomain().getCodeSource()
				.getLocation().toString()
				+ "/res/iteRight-logo.png";
		System.out.println(path);
		Image image1 = null;
		try {
			image1 = Image.getInstance(path);
		} catch (BadElementException e) {
		} catch (MalformedURLException e) {
		} catch (IOException e) {
		}
		if (image1 != null) {
			image1.scaleAbsolute(141.1f, 59.5f);
			cell = new PdfPCell(image1);
			cell.setRowspan(3);
		} else {
			cell = new PdfPCell(new Phrase("CiteRight"));
		}
		table.addCell(cell);
		cell = new PdfPCell(new Phrase("Plagiatsprüfungsbericht zu "
				+ source.getFilename()));
		cell.setColspan(2);
		table.addCell(cell);

		table.addCell(source.getStudentMatriculationNumber());
		table.addCell(source.getStudentName());
		table.addCell(date.toString());
		float plagiarismDegree = source.getPlagiarismDegree() * new Float(100);
		table.addCell("Plagiatsmaß: " + plagiarismDegree + "%");
		return table;
	}

	/**
	 * 
	 * @return a content table which handles a complete report of one section
	 *         and all matches
	 */
	public PdfPTable createContentTable(Section sourceSection) {

		PdfPTable table = new PdfPTable(1);
		PdfPCell cellSource = createContentItem(sourceSection);
		PdfPCell cellPlag;

		cellPlag = createPlagItem(sourceSection);
		if (cellPlag == null){
			return null;
		}
		table.addCell(cellSource);
		table.addCell(cellPlag);
		return table;

	}

	/**
	 * 
	 * @return a contentItem which is a section of the document
	 */
	public PdfPCell createContentItem(Section sourceSection) {
		Paragraph content = new Paragraph();
		content.add(sourceSection.getSectionText());
		PdfPCell cell = new PdfPCell(content);
		cell.setBackgroundColor(BaseColor.LIGHT_GRAY);
		cell.setBorder(0);
		return cell;
	}

	/**
	 * 
	 * @return a plagItem which is the source content, the link of the source
	 *         and the dice coefficient
	 */
	public PdfPCell createPlagItem(Section sourceSection) {
		// get the possible Match
		PossibleMatchService dbPossibleMatchService = new PossibleMatchService();
		SectionsService sectionsService = new SectionsService();
		PossibleMatch possibleMatch = dbPossibleMatchService
				.getPossibleMatchBySection(sourceSection, minMeasurement);
		if (possibleMatch == null){
			return null;
		}

		Section plagSection = sectionsService.getById(possibleMatch
				.getSectionId2());

		PdfPTable table = new PdfPTable(6);

		Paragraph content = new Paragraph();
		content.add(plagSection.getSectionText());

		PdfPCell contentCell = new PdfPCell();
		contentCell.setColspan(6);
		contentCell.addElement(content);

		// get URL Name in Document from Section
		DocumentService documentService = new DocumentService();
		DocumentContentService dbDocumentContentService = new DocumentContentService();
		int documentContentID = plagSection.getDocumentId();
		DocumentContent documentContent = dbDocumentContentService
				.getById(documentContentID);
		de.dhbw.citeright.essentials.db.Document plagDocument = documentService
				.getById(documentContent.getDocumentId());

		Chunk link = new Chunk(plagDocument.getFilename());
		link.setAction(new PdfAction(plagDocument.getFilename()));

		Paragraph linkContent = new Paragraph();
		linkContent.add(link);

		PdfPCell linkCell = new PdfPCell();
		linkCell.setColspan(5);
		linkCell.addElement(linkContent);
		linkCell.setBackgroundColor(BaseColor.LIGHT_GRAY);

		PdfPCell measurementCell = new PdfPCell();
		measurementCell.setColspan(1);
		measurementCell.addElement(new Paragraph(Math.round((possibleMatch
				.getMeasurement() * 100) * 100) / 100.0 + "%"));
		measurementCell.setBackgroundColor(BaseColor.RED);

		table.addCell(contentCell);
		table.addCell(linkCell);
		table.addCell(measurementCell);

		PdfPCell cell = new PdfPCell(table);
		cell.setBackgroundColor(BaseColor.ORANGE);
		cell.setBorder(0);
		return cell;

	}
}
