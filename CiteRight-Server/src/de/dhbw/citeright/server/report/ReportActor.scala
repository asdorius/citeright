package de.dhbw.citeright.server.report

import akka.actor.Props
import akka.actor.Actor
import compare.CompareComplete
import de.dhbw.citeright.essentials.db.service.DocumentService
import de.dhbw.citeright.essentials.db.service.DocumentContentService

object ReportActor {
  def props(): Props = Props(new ReportActor())
}

/**
 * Receives the messages from the CompareActor.
 * Starts the generation of the report as pdf file.
 */
class ReportActor extends Actor{
  def receive = {
    case x => {
      if (x.getClass() == classOf[CompareComplete])
      {try{
        val message = x.asInstanceOf[CompareComplete]
        val pdfGenerator = new ReportPDFGenerator
        pdfGenerator.generate(message.getDocumentId())
      }
      catch {
        case t:Throwable => {
          val message = x.asInstanceOf[CompareComplete]
          val docService = new DocumentService
          val docContentService = new DocumentContentService
          val documentBeingChecked = docService.getById(docContentService.getById(message.getDocumentId).getDocumentId)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw t
        }
      }
      }
    }
  }
}