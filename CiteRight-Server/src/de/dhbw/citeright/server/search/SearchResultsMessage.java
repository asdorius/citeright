package de.dhbw.citeright.server.search;

import java.util.List;

import de.dhbw.citeright.essentials.db.Section;

/**
 * Dataclass for search results
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class SearchResultsMessage {
	private final Section section;
	private final List<SearchResult> searchResults;

	/**
	 * Constructor
	 * 
	 * @param section
	 * @param searchResults
	 */
	public SearchResultsMessage(Section section,
			List<SearchResult> searchResults) {
		this.section = section;
		this.searchResults = searchResults;
	}

	/**
	 * 
	 * @return String section which results are in here
	 */
	public Section getSection() {
		return section;
	}

	/**
	 * 
	 * @return List<String> searchResults of the section
	 */
	public List<SearchResult> getSearchResults() {
		return searchResults;
	}

}
