package de.dhbw.citeright.server.search;

import akka.actor._
import extract.ExtractActor.SearchPhrasesMessage
import de.dhbw.citeright.essentials.db.Section
import de.dhbw.citeright.server.search.google.GoogleSearchEngine
import de.dhbw.citeright.server.search.bing.BingSearchEngine
import scala.collection.JavaConversions._
import scala.collection.JavaConversions
import scala.collection.mutable.Buffer
import de.dhbw.citeright.essentials.db.DocumentContent
import de.dhbw.citeright.essentials.db.service.DocumentContentService
import de.dhbw.citeright.essentials.db.service.DocumentService
import de.dhbw.citeright.essentials.db.service.SectionsService
/**
 * Provides Props and the messages this Actor sends
 */
object SearchActor {
  def props(SaveHtmlActor: ActorRef): Props = Props(new SearchActor(SaveHtmlActor))

  case class SectionResults(section: Section, searchResults: List[SearchResult])
  case class DocumentSearchResultsMessage(roomIDs: List[Integer],originalDocument: DocumentContent,sectionResults: List[SectionResults])
}
/**
 * Uses the Google- and Bing-Api to search for the given terms
 * on the internet
 */
class SearchActor(SaveActor: ActorRef) extends Actor {
  import SearchActor._
  def receive = {
    case SearchPhrasesMessage(roomIDs,originalDocumentContent,sections) => {
      try{
      println("SearchActor received SearchPhrasesMessage. RoomIDs: "+ roomIDs +" OriginalDocument: " + originalDocumentContent + " Sections: " + sections)
      val googleEngine = new GoogleSearchEngine()
      val bingEngine = new BingSearchEngine()

      //Map every section from sections to some new SectionResults. These contain the current Section        
      //and the search results for the section's headwords
      //This generates a list of SectionResults
      val sectionResults = sections.map { section =>
        //TODO: Validating, if sectionText is null
        val bingHeadwordResults = bingEngine.search(section.getHeadwords, 15)
        val googleHeadwordResults = googleEngine.search(section.getHeadwords, 5)
        val sentenceResults = section.getSectionText().split(".").flatMap { sentence => {
          val bingSentenceResults = bingEngine.search(sentence, 5)           
          val googleSentenceResults = googleEngine.search(sentence,5)
            (bingSentenceResults ++ googleSentenceResults).toList
          }
        }
        println("Section Search Query: " + section.getHeadwords)
        val results = (googleHeadwordResults ++ bingHeadwordResults ++ sentenceResults).toList
        SectionResults(section, results)
      } toList

      println("SearchActor will send DocumentSearchResultsMessage to SaveActor at: " + SaveActor.path)
      SaveActor ! DocumentSearchResultsMessage(roomIDs,originalDocumentContent, sectionResults)
      }
      catch {
        case t:Throwable => {
          val docService = new DocumentService
          val documentBeingChecked = docService.getById(originalDocumentContent.getDocumentId)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw t
        }
      }
    }
    case x =>  println("SearchActor received non expected Message. Message: "+ x)
  }
}