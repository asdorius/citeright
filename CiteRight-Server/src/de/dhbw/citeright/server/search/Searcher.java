package de.dhbw.citeright.server.search;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * Callable for searching with overgiven SearchEngine
 * 
 * @author Moritz Ragg
 * @since 01.05.2015
 */
public class Searcher implements Callable<List<SearchResult>> {
	private SearchEngine searchEngine;
	private String headwords;
	private int count = 0;

	/**
	 * Constructor
	 * 
	 * @param searchEngine
	 * @param headwords
	 * @param count
	 */
	public Searcher(SearchEngine searchEngine, String headwords, int count) {
		this.searchEngine = searchEngine;
		this.headwords = headwords;
		this.count = count;
	}

	/**
	 * Constructor
	 * 
	 * @param searchEngine
	 * @param headwords
	 */
	public Searcher(SearchEngine searchEngine, String headwords) {
		this.searchEngine = searchEngine;
		this.headwords = headwords;
	}

	/**
	 * @return List of SearchResults
	 */
	@Override
	public List<SearchResult> call() throws Exception {
		return searchEngine.search(headwords, count);
	}
}
