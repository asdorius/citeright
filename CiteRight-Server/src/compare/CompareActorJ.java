package compare;

import java.util.LinkedList;
import java.util.List;
import java.util.function.Predicate;

import scala.collection.JavaConversions;
import akka.actor.ActorRef;
import akka.actor.Props;
import akka.actor.UntypedActor;
import convert.ConvertSearchResultsActor.ConvertedSearchResultsMessage;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.PossibleMatch;
import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.essentials.db.service.DocumentContentService;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.PossibleMatchService;
import de.dhbw.citeright.essentials.db.service.SectionsService;

/**
 * 
 * @author Entwickler
 * 
 * Uses N-Gram Comparison to compare a DocumentContent to all the other Documents that 
 * reside in the Rooms specified by their respective IDs.
 *
 */
public class CompareActorJ extends UntypedActor {
	private ActorRef reportActor = null;

	public static Props getProps(ActorRef reportActor) {
		return Props.create(CompareActorJ.class, reportActor);
	}

	/**
	 * Starts comparison of the Documents supplied by a ConvertedSearchResultsMessage 
	 * if the received Message has this format.
	 * @param message Must be a ConvertSearchResultMessage or nothing will happen
	 * @throws Exception DocumentStatus is updated. Exception must be thrown for handling by the parent Actor.
	 */
	@Override
	public void onReceive(Object message) throws Exception {
		try {
			System.out.println("CompareActorJ received Message: " + message);
			if (message instanceof ConvertedSearchResultsMessage) {
				compare(((ConvertedSearchResultsMessage) message));
			} else {
				System.out
						.println("CompareActorJ received non expected Message. Message: "
								+ message.toString());
			}
		} catch (Exception e) {
			if (message instanceof ConvertedSearchResultsMessage) {
				DocumentService docService = new DocumentService();
				Document documentBeingChecked = docService
						.getById(((ConvertedSearchResultsMessage) message)
								.documentContent().getDocumentId());
				documentBeingChecked.setStatusId(4);
				docService.update(documentBeingChecked);
			}
			throw e;
		}
	}

	/**
	 * Initializes a CompareActorJ with an ActorRef. The results of the comparisont will be sent to this ActorRef.
	 * Normally this is an instance of ReportActor.
	 * @param reportActor
	 */
	public CompareActorJ(ActorRef reportActor) {
		this.reportActor = reportActor;
	}

	/**
	 * Compares the documentContent that resides inside the message with all the results of a web search 
	 * in addition to the documents in all the rooms specified by the roomIds
	 * @param message
	 */
	private void compare(ConvertedSearchResultsMessage message) {

		System.out
				.println("CompareActorJ's received Message has the right Format and contains: RoomIds: "
						+ message.roomIDs().toString()
						+ " Document: "
						+ message.documentContent().toString()
						+ " SearchResults: "
						+ message.searchResults().toString());
		List<DocumentContent> compareDocuments = new LinkedList<DocumentContent>();
		DocumentContentService contentService = new DocumentContentService();
		for (int roomId : JavaConversions.asJavaIterable(message.roomIDs())) {
			compareDocuments.addAll((contentService
					.getDocumentContentsOfRoom(roomId)));

		}
		compareDocuments.addAll(JavaConversions.asJavaCollection(message
				.searchResults()));
		DocumentContent originalDocumentInList = null;
		for (DocumentContent compareDocument : compareDocuments) {
			if (compareDocument != null
					&& compareDocument.getId().equals(
							message.documentContent().getId())) {
				originalDocumentInList = compareDocument;
			}
		}
		if (originalDocumentInList != null) {
			compareDocuments.remove(originalDocumentInList);
		}

		List<PossibleMatch> possibleMatches = new LinkedList<>();
		for (DocumentContent compareDocument : compareDocuments) {
			if (compareDocument != null) {
				possibleMatches.addAll(compareDocuments(message.documentContent(),
						compareDocument));
			}
		}

		// set measurement for the document
		SectionsService dbSectionService = new SectionsService();
		List<Section> sections;
		sections = dbSectionService.getSectionsOfDocument(message
				.documentContent().getId());
		float sumMeasurement = 0;
		float counter = 0;
		for (Section section : sections) {
			float highestMeasurementForThisSection = 0;
			for (PossibleMatch possibleMatch : possibleMatches) {
				if (possibleMatch.getSectionId1().equals(section.getId())) {
					if (possibleMatch.getMeasurement() > highestMeasurementForThisSection) {
						highestMeasurementForThisSection = possibleMatch
								.getMeasurement();
					}
				}
			}
			if (highestMeasurementForThisSection > 0.1){
			sumMeasurement += highestMeasurementForThisSection;
			counter += 1;
			}
		}
		float documentMeasurement = 0;
		if (counter > 0) {
			documentMeasurement = sumMeasurement / counter;
		}
		DocumentService dbDocumentService = new DocumentService();
		Document document = dbDocumentService.getById(message.documentContent()
				.getDocumentId());
		document.setPlagiarismDegree(documentMeasurement);
		dbDocumentService.update(document);

		System.out
				.println("CompareActorJ will send CompareComplete to ReportActor at: "
						+ reportActor.path().toString());
		reportActor.tell(
				new CompareComplete(message.documentContent().getId()), self());

	}

	/**
	 * Compares two DocumentContents using an N-Gram-Comparison. 
	 * Creates PossibleMatches that are being stored in the Database and returns those in a List.
	 * @param baseDocument
	 * @param compareDocument
	 * @return
	 */
	private List<PossibleMatch> compareDocuments(
			DocumentContent baseDocument,
			DocumentContent compareDocument) {
		SectionsService sectionsService = new SectionsService();
		List<Section> sourceSections = sectionsService
				.getSectionsOfDocument(baseDocument.getId());
		List<Section> compareSections = sectionsService
				.getSectionsOfDocument(compareDocument.getId());
		List<PossibleMatch> possibleMatches = new LinkedList<PossibleMatch>();
		PossibleMatchService possibleMatchService = new PossibleMatchService();

		for (Section sourceSegment : sourceSections) {
			for (Section compareSection : compareSections) {
				float measurement = NGramComperator.compare(sourceSegment,
						compareSection);

				PossibleMatch possibleMatch = new PossibleMatch();
				possibleMatch.setSectionId1(sourceSegment.getId());
				possibleMatch.setSectionId2(compareSection.getId());
				possibleMatch.setMeasurement(measurement);

				possibleMatchService.create(possibleMatch);
				possibleMatches.add(possibleMatch);
			}
		}
		return possibleMatches;

	}

}
