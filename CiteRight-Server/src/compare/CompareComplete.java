package compare;

/**
 * Dataclass which represents a documentId
 * 
 */
public class CompareComplete {
	private final int documentId;

	public CompareComplete(int documentId) {
		super();
		this.documentId = documentId;
	};
	
	public int getDocumentId()
	{
		return documentId;
	}
	
}
