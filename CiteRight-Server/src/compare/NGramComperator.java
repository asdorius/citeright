package compare;

import java.util.LinkedHashSet;
import java.util.Set;

import de.dhbw.citeright.essentials.db.Section;

/**
 * Comparator which computes the dice coefficent of two sections
 * 
 * @author Moritz Ragg
 * @since 06.05.2015
 */
public class NGramComperator {

	/**
	 * Computes the dice coefficient of two sections
	 * 
	 * @param sourceSection
	 * @param compareSection
	 * @return diceCoefficent
	 */
	public static float compare(Section sourceSection, Section compareSection) {
		float diceCoefficient = 0;
		int sourceSize = 0;
		int compareSize = 0;
		int intersectionSize = 0;

		prepareSectionText(sourceSection);
		prepareSectionText(compareSection);

		Set<String> sourceSet = toNGramm(sourceSection, 3);
		sourceSize = sourceSet.size();
		Set<String> compareSet = toNGramm(compareSection, 3);
		compareSize = compareSet.size();

		sourceSet.retainAll(compareSet);
		intersectionSize = sourceSet.size();
		
		float absoluteSize;
		
		if(sourceSize >= compareSize){
			absoluteSize = compareSize;
		}else{
			absoluteSize = sourceSize;
		}

		if(absoluteSize != 0)
		{
			diceCoefficient = (float) intersectionSize
					/ absoluteSize;	
		}
		else
		{
			return 0f;
		}
		
		return diceCoefficient;
	}

	/**
	 * prepares the section: no special chars, all chars in lower case
	 * 
	 * @param section
	 */
	private static void prepareSectionText(Section section) {
		section.setSectionText(section.getSectionText()
				.replaceAll("[(\\p{Punct}|§)]", "").toLowerCase());
	}

	/**
	 * Changes an String array into an n-gramm whit n = count
	 * 
	 * @param section
	 * @param count
	 * @return
	 */
	private static Set<String> toNGramm(Section section, int count) {
		String[] sourceWords = section.getSectionText().split(" ");
		Set<String> set = new LinkedHashSet<String>();

		String separation = "§";

		for (int i = 0; i < sourceWords.length - count + 1; i++) {
			for (int j = 1; j < count; j++) {
				if (j != count) {
					sourceWords[i] += separation;
				}
				sourceWords[i] += sourceWords[i + j];
			}
			set.add(sourceWords[i]);
		}

		return set;
	}

}
