package convert

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import main.ServerActor.ConvertThisMessage
import de.dhbw.citeright.essentials.db.DocumentContent
import de.dhbw.citeright.essentials.db.service.DocumentService
/**
 * Defines props for creation and the message sent by this Actor
 */
object ConvertActor {
  def props(extract: ActorRef): Props = Props(new ConvertActor(extract))
  case class ConvertedDocumentMessage(roomIDs : List[Integer], documentContent: DocumentContent)
}

/**
 * Uses a FileParser to convert the file referenced by the DocumentId inside a ConvertThisMessage
 *  to Sections that are being held by a DocumentContent.
 */
class ConvertActor(extract: ActorRef) extends Actor {
  import ConvertActor._
  def receive = {
    case ConvertThisMessage(roomIDs,documentID) => {
      try{
      println("ConvertActor received ConvertThisMessage. RoomIDs: "+ roomIDs +" DocumentID: " + documentID)
      val documentContent = new FileParser().toDocumentContent(new DocumentService().getById(documentID), 3)
      println("ConvertActor will send ConvertedDocumentMessage to ExtractActor at: " + extract.path)
      extract ! new ConvertedDocumentMessage(roomIDs,documentContent)
      }
      catch {
        case x:Throwable => {
          val docService = new DocumentService
          val documentBeingChecked = docService.getById(documentID)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw x
        }
      }
    }
    case x =>  println("ConvertActor received non expected Message. Message: "+ x)
    
  }
}