package convert;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringReader;

import org.apache.tika.exception.TikaException;
import org.apache.tika.Tika;
import org.codehaus.plexus.util.StringUtils;

import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.service.DocumentContentService;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.SectionsService;

public class FileParser {

	/**
	 * Transforms the given File to a Document the application can work with
	 * Splits the text of the file into groups of 5 lines. Those are then
	 * converted to the Sections to be stored in the document.
	 * 
	 * @param file
	 *            A file in any of the formats apache tika supports. See: <a
	 *            href="https://tika.apache.org/1.8/formats.html">https://tika.
	 *            apache.org/1.8/formats.html</a>
	 * @param sectionLength
	 *            How long each of the Sections should be
	 * @return A Document containing the contents of file in plain text,
	 *         splitted into Sections
	 */
	public DocumentContent toDocumentContent(de.dhbw.citeright.essentials.db.Document file, int sectionLength) {
		// set status on "Wird geprüft"
		file.setStatusId(1);
		DocumentService documentService = new DocumentService();
		documentService.update(file);
				
		DocumentContentService dbDocumentContentService = new DocumentContentService();
		// check if the auditreport was already created, if it was delete it from the db
		DocumentContent checkForOldDocumentContent = null;
		try {
		checkForOldDocumentContent = dbDocumentContentService.getDocumentContentByDocumenteId(file.getId());
		if (checkForOldDocumentContent != null) {
			dbDocumentContentService.delete(checkForOldDocumentContent.getId());
			file.setAuditReport(null);
			documentService.update(file);
		}
		} catch (NullPointerException e) {			
		}
		
		DocumentContent documentContent = new DocumentContent();
		documentContent.setDocumentId(file.getId());
		int documentContentId = dbDocumentContentService.create(documentContent);
		SectionsService dbSectionsService = new SectionsService();
		
		InputStream input = null;
		Tika parser = null;
		BufferedReader reader = null;
		try {
			// get document content bytes and prepare
			parser = new Tika();
			input = new ByteArrayInputStream(file.getData());
			String theContent = parser.parseToString(input);
			reader = new BufferedReader(new StringReader(theContent));
			int lineCount = 1;
			int orderingNumber = 1;
			String currentLine = reader.readLine();
			String lastLine = "";
			StringBuilder sb = new StringBuilder();
			// double check, both loops read!
			while (currentLine != null) {
				// using modulo makes splitting easy
				while (!StringUtils.isBlank(currentLine)
						&& lineCount != sectionLength) {
					sb.append(currentLine + " ");
					lastLine=currentLine;
					currentLine = reader.readLine();
					//read until next non empty line
					while (currentLine != null && StringUtils.isBlank(currentLine))
					{
						currentLine = reader.readLine();
					}
					lineCount++;
				}
				// we're done with a Section,remove whitespaces, create it and prepare for next
				// Section.
				//String sectionText, String headWords, int document, int orderingNumber
				String sectionText = sb.toString().replaceAll("\\s+", " ");
				if(!StringUtils.isBlank(sectionText)){					
				dbSectionsService.create(new Section(sectionText, "", documentContentId, orderingNumber++));							
				}
				sb.setLength(0);	
				currentLine = reader.readLine();
				lineCount = 1;
			}

		} catch (FileNotFoundException e) {
			//TODO: statement below is not always correct. handle/log errors!
			// NONE of these should ever occur
			// here!
			// Check document validity on upload!
		} catch (IOException e) { 
			// If anything goes wrong here, application is
			// probably corrupt!
		} catch (TikaException e) {

		} finally {
			if (input != null)
				try {
					input.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block

				}
			if (reader != null)
				try {
					reader.close();
				} catch (IOException e) {
					// TODO Auto-generated catch block

				}
		}

		return documentContent;
	}
}
