package convert

import akka.actor._
import saveHTML.SaveHtmlActor.ConvertSearchResultsMessage
import de.dhbw.citeright.essentials.db.DocumentContent
import de.dhbw.citeright.essentials.db.service.DocumentService
/**
 * Defines props for creation and the message sent by this Actor.
 */
object ConvertSearchResultsActor {
  def props(compare: ActorRef): Props = Props(new ConvertSearchResultsActor(compare))
  case class ConvertedSearchResultsMessage(roomIDs: List[Integer], documentContent: DocumentContent, searchResults: List[DocumentContent])
}
/**
 * Converts each SearchResult that is provided by a ConvertSearchResultsMessage 
 * to a DocumentContent connected to it's respective Sections.
 */
class ConvertSearchResultsActor(compare: ActorRef) extends Actor {
  import ConvertSearchResultsActor._
  def receive = {
    
    case ConvertSearchResultsMessage(roomIDs, originalDocument, savedSearchResultIDs) => {
    try {
      println("ConvertSearchResultsActor received ConvertSearchResultsMessage. RoomIDs: " + roomIDs + " Document: " + originalDocument + " savedSearchResultIDs: " + savedSearchResultIDs)
      
      val convertedSearchResults = savedSearchResultIDs.flatMap { sectionResults =>
        sectionResults.map {
          searchResultDocumentID =>
            new FileParser().toDocumentContent(new DocumentService().getById(searchResultDocumentID), 2)
        }
      }
        
      println("ConvertSearchResultsActor will send ConvertedSearchResultsMessage to CompareActor at: " + compare.path)
        compare ! new ConvertedSearchResultsMessage(roomIDs, originalDocument, convertedSearchResults)
    } catch {
        case x:Throwable => {
          val docService = new DocumentService
          val documentBeingChecked = docService.getById(originalDocument.getDocumentId)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw x
        }
      }
    }
    case x => println("ConvertSearchResultsActor received non expected Message. Message: " + x)
  }
}
