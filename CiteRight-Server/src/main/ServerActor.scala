package main
import akka.actor._
import de.dhbw.citeright.essentials.db.Document
import java.io.File
import de.dhbw.citeright.essentials.remoteactor.ProcessDocumentMessage
import scala.collection.JavaConversions
import de.dhbw.citeright.essentials.db.service.DocumentService
import de.dhbw.citeright.essentials.db.service.DocumentContentService
/**
 * Provides props and the messages sent by this Actor
 */
object ServerActor {
  def props(convertActor: ActorRef): Props = Props(new ServerActor(convertActor))
  case class ConvertThisMessage(roomIDs : List[Integer], documentID: Integer)
}

/**
 * Receives the messages from the WebApplication.
 * Starts the pipeline by sending a message to the first actor in the chain.
 */
class ServerActor(convertActor: ActorRef) extends Actor {
  import ServerActor._
  def receive = {case x =>
    println("ServerActor received message : "+ x.toString())
    if (x.getClass() == classOf[ProcessDocumentMessage]) {
      try{
      println("ServerActor will send message " + x.toString() + " to ConvertActor at " + convertActor.path)
      convertActor ! new ConvertThisMessage(
          JavaConversions.asScalaBuffer(x.asInstanceOf[ProcessDocumentMessage].getRoomIDs).toList,
          x.asInstanceOf[ProcessDocumentMessage].getDocumentID)
    }catch {
        case t:Throwable => {
          val message = x.asInstanceOf[ProcessDocumentMessage]
          val docService = new DocumentService
          val docContentService = new DocumentContentService
          val documentBeingChecked = docService.getById(message.getDocumentID)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw t
        }
      }
    }
  }
}