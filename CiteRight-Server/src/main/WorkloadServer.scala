package main
import akka.actor._
import com.typesafe.config
import com.typesafe.config.ConfigFactory
import convert.ConvertActor
import extract.ExtractActor
import saveHTML.SaveHtmlActor
import de.dhbw.citeright.server.search.SearchActor
import akka.event.Logging
import de.dhbw.citeright.essentials.constants.CiteRightConstants
import compare.CompareActorJ
import de.dhbw.citeright.server.report.ReportActor
import convert.ConvertSearchResultsActor
import com.typesafe.config.Config

/**
 * The main entry Point for the Application
 * Creates the Actorsystem the Application relies on
 * and starts it's top-level actors
 */
object WorkloadServer extends App {
  println("WorkloadServer starting...")
  //Create System
  val config = ConfigFactory parseString "akka {actor {provider = \"akka.remote.RemoteActorRefProvider\"},remote {enabled-transports = [\"akka.remote.netty.tcp\"], netty.tcp {hostname = \"127.0.0.1\", port = 2553}}}" 
  val system = ActorSystem.create("WorkloadServer", config )
  //Create all Actors to be accessible by url
  val report = system.actorOf(ReportActor.props(), "report")
  val compare = system.actorOf(CompareActorJ.getProps(report) , "compare")
  val convertSearchResults = system.actorOf(ConvertSearchResultsActor.props(compare), "convertSearchResults")
  val saveHtml = system.actorOf(SaveHtmlActor.props(convertSearchResults), "saveHtml")
  val search = system.actorOf(SearchActor.props(saveHtml), "search")
  val extract = system.actorOf(ExtractActor.props(search), "extract")
  val convert = system.actorOf(ConvertActor.props(extract), "convert")
  val server = system.actorOf(ServerActor.props(convert), "server")

  println("WorkloadServer online!")
  //omitting system.shutdown to keep the program running
}

