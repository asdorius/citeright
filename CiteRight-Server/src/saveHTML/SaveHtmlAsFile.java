package saveHTML;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.nio.ByteBuffer;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Timestamp;
import java.util.Calendar;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.service.DocumentService;

/**
 * Class to Save a HTML (reachable over an URL) on a HTML file on the DB-Server
 * 
 * @author Antonio
 * @since 29.04.2015
 */
public class SaveHtmlAsFile {

	/**
	 * Check if the website is available
	 * From stackoverflow
	 * http://stackoverflow.com/questions/3584210/preferred-java-way-to-ping-a-http-url-for-availability
	 * @param urlString
	 * @param timeout
	 * @return Boolean true = site is reachable
	 */
	public boolean ping(String url, int timeout) {
	    // Otherwise an exception may be thrown on invalid SSL certificates:
		url = url.replaceFirst("^https", "http");

	    try {
	        HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
	        connection.setConnectTimeout(timeout);
	        connection.setReadTimeout(timeout);
	        connection.setRequestMethod("HEAD");
	        int responseCode = connection.getResponseCode();
	        return (200 <= responseCode && responseCode <= 399);
	    } catch (Exception exception) {
	    	System.out.println("Url \""+url + "\" not available.");
	        return false;
	    }
	}
	
	/**
	 * 
	 * @return Timestamp with the time of creation
	 */
	public Timestamp getTimestampNow() {
		Calendar calendar = Calendar.getInstance();
		Timestamp timestampNow = new Timestamp(calendar.getTime().getTime());
		return timestampNow;
	}
	
	/**
	 * Method to Calculate the Size of the Site
	 * @param urlString
	 * @return the Size of the Site as Byte
	 */
	public static int getSizeOfSite(String urlString) {
		int size = -1;

		URL url;
		URLConnection conn;
		try {
			url = new URL(urlString);
			conn = url.openConnection();
			size = conn.getContentLength();		
		} catch (IOException e) {
		}
		
		if (size > 0)
		{
			return size;
		}
		else // size could not be calculated, return one mb as size
		{
			return 1 * 1024 * 1024;
		}
	}
	
	 /**
	  * Call the website and write its Code into a HTML file on the DB-Server
	  * @param urlString
	 *            URL of the website as String
	  * @return DocumentID of the database, where the file is saved, -1 if url was not reachable
	  */
	public int saveHTML(String urlString) {
		// check if site is reachable
		boolean reachable = ping(urlString, 10000);
		String fileExtension;
		DocumentService dbDocumentService = new DocumentService();
		byte[] documentContent;
		int documentID = -1;

		// if site is not reachable do not create a file
		if(reachable)
		{
			URL url;
			ReadableByteChannel rbc = null;

			try {
				url = new URL(urlString);
				// create fileName
				if(urlString.contains(".pdf"))
				{
					fileExtension = "application/pdf";
				}
				else
				{
					fileExtension = "text/html";
				}
								
				// open stream to the source URL
				rbc = Channels.newChannel(url.openStream());
				ByteBuffer bBuffer = ByteBuffer.allocate(getSizeOfSite(urlString));
				rbc.read(bBuffer);
				documentContent = bBuffer.array();
				
				// create document and put into database
				Document searchResultDocument = new Document(urlString, fileExtension, documentContent, getTimestampNow(), 10);
				searchResultDocument.setForeignSource(true);				
				documentID = dbDocumentService.create(searchResultDocument);
			} catch (MalformedURLException mue) {
			} catch (IOException ioe) {
			} finally {
				try { // Close Output Stream
					if (rbc != null) {
						rbc.close();
					}
				} catch (IOException e) {
					// Stream has not been started
				}
			}
		}
		
		return documentID;
	}

}
