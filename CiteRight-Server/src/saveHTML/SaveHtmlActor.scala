package saveHTML

import akka.actor._
import de.dhbw.citeright.server.search.SearchActor.DocumentSearchResultsMessage
import de.dhbw.citeright.server.search.SearchActor.SectionResults
import de.dhbw.citeright.essentials.db.DocumentContent
import de.dhbw.citeright.essentials.db.service.DocumentContentService
import de.dhbw.citeright.essentials.db.service.DocumentService

/**
 * Defines props and the message sent by this Actor.
 */
object SaveHtmlActor {
  def props(convertSearchResultsActor: ActorRef): Props = Props(new SaveHtmlActor(convertSearchResultsActor))
  case class ConvertSearchResultsMessage(roomIDs : List [Integer], originalDocument:DocumentContent, savedDocuments : List[List[Int]])
}
/**
 * Saves all the SearchResult files provided by a DocumentSearchResultsMessage to the Database 
 * and sends a Message to it's following Actor specified by the constructor parameter.
 */
class SaveHtmlActor(convertSearchResultsActor: ActorRef) extends Actor {
  import SaveHtmlActor._
  def receive = {

    case DocumentSearchResultsMessage(roomIDs,originalDocumentContent,sectionResults) => {
      try{
      println("SaveHtmlActor received DocumentSearchResultsMessage. RoomIDs: "+ roomIDs +" OriginalDocument: " + originalDocumentContent + " SectionResults: " + sectionResults)
     
      val searchResultDocumentIDs = sectionResults.map{ case sectionResults =>
        {
          val searchResults = sectionResults.searchResults
          val saved = searchResults.map { searchResult => new SaveHtmlAsFile saveHTML searchResult.getUrl }
          saved.filter { id => id >=0 }
        }
      }
        
        if (sectionResults.length >=1){
        println("SaveHtmlActor will send ConvertSearchResultsMessage to ConvertSearchResultsActor at: " + convertSearchResultsActor.path)
          convertSearchResultsActor ! new ConvertSearchResultsMessage(roomIDs, originalDocumentContent, searchResultDocumentIDs)
      } 
    }catch {
        case t:Throwable => {
          val docService = new DocumentService
          val docContentService = new DocumentContentService
          val documentBeingChecked = docService.getById(originalDocumentContent.getDocumentId)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw t
        }
      }
   }
   case x =>  println("SaveHtmlActor received non expected Message. Message: "+ x)
  }
}
