package extract

import akka.actor._
import scala.concurrent.Await
import scala.concurrent.Future
import akka.pattern.ask
import akka.util.Timeout
import akka.dispatch.Futures
import scala.concurrent.duration._
import convert.ConvertActor.ConvertedDocumentMessage
import scala.collection.JavaConversions._
import de.dhbw.citeright.essentials.db.Document
import de.dhbw.citeright.essentials.db.Section
import scala.collection.mutable.Buffer
import de.dhbw.citeright.essentials.db.DocumentContent
import de.dhbw.citeright.essentials.db.service.SectionsService
import de.dhbw.citeright.essentials.db.service.DocumentService

/**
 * Provides props and other static members
 *
 */
object ExtractActor {
  def props(searchActor: ActorRef): Props = Props(new ExtractActor(searchActor))
  case class SearchPhrasesMessage(roomIDs: List[Integer], originalDocument: DocumentContent, phrases: List[Section])
}
/**
 * removes nonrelevant words of a ConvertedDocumentMessage
 *  extracts headwords of the sections
 *  creates a SearchPhrasesMessage and sends it to a SearchActor
 */
class ExtractActor(searchActor: ActorRef) extends Actor {
  import ExtractActor._
  def receive = {
    case ConvertedDocumentMessage(roomIDs, documentContent) => {
      try {
        println("ExtractActor received ConvertedDocumentMessage. RoomIDs: " + roomIDs + " DocumentContent: " + documentContent)
        import context.dispatcher
        val nonRelevantWords = List("die", "der", "und", "in", "zu", "den", "das", "nicht", "von", "sie", "ist", "des", "sich", "mit", "dem", "dass", "er", "es", "ein", "ich", "auf", "so", "eine", "auch", "als", "an", "nach", "wie", "im", "für", "man", "aber", "aus", "durch", "wenn", "nur", "war", "noch", "werden", "bei", "hat", "wir", "was", "wird", "sein", "einen", "welche", "sind", "oder", "zur", "um", "haben", "einer", "mir", "über", "ihm", "diese", "einem", "ihr", "uns", "da", "zum", "kann", "doch", "vor", "dieser", "mich", "ihn", "du", "hatte", "seine", "mehr", "am", "denn", "nun", "unter", "sehr", "selbst", "schon", "hier", "bis", "habe", "ihre", "dann", "ihnen", "seiner", "alle", "wieder", "meine", "Zeit", "gegen", "vom", "ganz", "einzelnen", "wo", "muss", "ohne", "eines", "können", "sei", "ja", "wurde", "jetzt", "immer", "seinen", "wohl", "dieses", "ihren", "würde", "diesen", "sondern", "weil", "welcher", "nichts", "diesem", "alles", "waren", "will", "Herr", "viel", "mein", "also", "soll", "worden", "lassen", "dies", "machen", "ihrer", "weiter", "Leben", "recht", "etwas", "keine", "seinem", "ob", "dir", "allen", "großen", "Jahre", "Weise", "müssen", "welches", "wäre", "erst", "einmal", "Mann", "hätte", "zwei", "dich", "allein", "Herren", "während", "Paragraph", "anders", "Liebe", "kein", "damit", "gar", "Hand", "Herrn", "euch", "sollte", "konnte", "ersten", "deren", "zwischen", "wollen", "denen", "dessen", "sagen", "bin", "Menschen", "gut", "darauf", "wurden", "weiß", "gewesen", "Seite", "bald", "weit", "große", "solche", "hatten", "eben", "andern", "beiden", "macht", "sehen", "ganze", "anderen", "lange", "wer", "ihrem", "zwar", "gemacht", "dort", "kommen", "Welt", "heute", "Frau", "werde", "derselben", "ganzen", "deutschen", "lässt", "vielleicht", "meiner", "le", "de", "un", "à", "être", "et", "en", "avoir", "que", "pour", "dans", "ce", "il", "qui", "ne", "sur", "se", "pas", "plus", "pouvoir", "par", "je", "avec", "tout", "faire", "son", "mettre", "autre", "on", "mais", "nous", "comme", "ou", "si", "leur", "y", "dire", "elle", "devoir", "avant", "deux", "même", "prendre", "aussi", "celui", "donner", "bien", "où", "fois", "vous", "encore", "nouveau", "aller", "cela", "entre", "premier", "vouloir", "déjà", "grand", "mon", "me", "moins", "aucun", "lui", "temps", "très", "savoir", "falloir", "voir", "quelque", "sans", "raison", "notre", "dont", "non", "an", "monde", "jour", "monsieur", "demander", "alors", "après", "trouver", "personne", "rendre", "part", "dernier", "venir", "pendant", "passer", "peu", "lequel", "suite", "bon", "comprendre", "depuis", "point", "ainsi", "heure", "rester", "time", "person", "year", "way", "day", "thing", "man", "world", "life", "hand", "part", "child", "eye", "woman", "place", "work", "week", "case", "point", "government", "company", "number", "group", "problem", "fact", "be", "have", "do", "say", "get", "make", "go", "know", "take", "see", "come", "think", "look", "want", "give", "use", "find", "tell", "ask", "work", "seem", "feel", "try", "leave", "call", "good", "new", "first", "last", "long", "great", "little", "own", "other", "old", "right", "big", "high", "different", "small", "large", "next", "early", "young", "important", "few", "public", "bad", "same", "able", "to", "of", "in", "for", "on", "with", "at", "by", "from", "up", "about", "into", "over", "after", "beneath", "under", "above", "the", "and", "a", "that", "I", "it", "not", "he", "as", "you", "this", "but", "his", "they", "her", "she", "or", "an", "will", "my", "one", "all", "would", "there", "their")
        // TODO control new database classes
        val sectionsService = new SectionsService
        val sections = sectionsService.getSectionsOfDocument(documentContent.getId)
        val repliesFiltered = sections.map {
          section => Future { new RemoveNonRelevantWords(section, nonRelevantWords).call() }
        }
        Future.sequence(repliesFiltered).onSuccess {
          case filteredSections =>
            val repliesRelevant = filteredSections.map {
              case filteredSection => Future {
                try {
                  val dirtyWords = WordFrequency.getWordsInDescendingFreqOrder(filteredSection.getSectionText)
                  println("DirtyWords: " + dirtyWords)
                  val cleanWords = dirtyWords.replaceAll("[^A-ZÄÖÜa-zäöü0-9ß+-]", "").replaceAll("\\+", " ")
                  println("CleanWords: " + cleanWords)
                  filteredSection.setHeadwords(
                    cleanWords)
                    } catch { case _: Throwable => }
                filteredSection
              }
            }
            Future.sequence(repliesRelevant.toList).onSuccess {

              case extractedSections => {
                println("ExtractActor will send SearchPhrasesMessage to SearchActor at: " + searchActor.path)
                searchActor ! new SearchPhrasesMessage(roomIDs, documentContent, extractedSections)
              }
            }

        }
      }
      catch {
        case t:Throwable => {
          val docService = new DocumentService
          val documentBeingChecked = docService.getById(documentContent.getDocumentId)
          documentBeingChecked.setStatusId(4)
          docService.update(documentBeingChecked)
          throw t
        }
      }
    }
    case x => println("ExtractActor received non expected Message. Message: " + x)
  }
}
