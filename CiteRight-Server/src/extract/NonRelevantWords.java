package extract;

import java.util.List;

/**
 * Dataclass for non relevantWords
 * 
 * @author Moritz Ragg
 * @since 29.04.2015
 */
public class NonRelevantWords {
	private List<String> words;

	/**
	 * Constructor
	 * 
	 * @param words
	 */
	public NonRelevantWords(List<String> words) {
		this.words = words;
	}

	/**
	 * adds a word
	 * 
	 * @param word
	 */
	public void addWord(String word) {
		this.words.add(word);
	}

	/**
	 * adds list of words
	 * 
	 * @param words
	 */
	public void addWords(List<String> words) {
		this.words.addAll(words);
	}

	/**
	 * 
	 * @param id
	 * @return word by id
	 */
	public String getWordByID(int id) {
		return words.get(id);
	}

	/**
	 * 
	 * @return all words
	 */
	public List<String> getAll() {
		return this.words;
	}

}
