package extract;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class to get a comma seperated list of words out of a String the words are
 * ordered descending after the frequency
 * 
 * @author Antonio
 * @since 29.04.2015
 */
public class WordFrequency {

	/**
	 * Separate a String into words and orders them descending by frequency
	 * appearance
	 * 
	 * @param textOfSection
	 * @return String String with the most frequent words sorted and comma
	 *         seperated
	 */
	public static String getWordsInDescendingFreqOrder(String textOfSection) {
		final int NumberOfWordsForSearchQuery = 5;
		String[] singleWords = textOfSection.split(" ");

		Map<String, Integer> map = new HashMap<>();
		for (int i = 0; i < singleWords.length; i++) {
			if (map.containsKey(singleWords[i])) {
				map.put(singleWords[i],
						new Integer(map.get(singleWords[i]) + 1));
			} else { // word is not in the map
				// put word into map with frequency 1
				map.put(singleWords[i], new Integer(1));
			}
		}

		List<String> sortedList = new ArrayList<String>();
		sortedList = orderInDescendingFreqSequence(map);

		String result = createSearchQuery(sortedList,
				NumberOfWordsForSearchQuery);
		return result;
	}

	private static String createSearchQuery(List<String> sortedList,
			int limitForWords) {
		String searchQuery = ""; // put list into one string comma seperated
		
		if (sortedList.size() < limitForWords) { // look if the limit for words in the querey is bigger than the size of the list
			for (int i = 0; i < (sortedList.size() - 1); i++) {
				searchQuery += sortedList.get(i);
				searchQuery += "+";
			}
			searchQuery += sortedList.get((sortedList.size() - 1));
		} else { // make a query with an limited amount of words
			for (int i = 0; i < (limitForWords - 1); i++) {
				searchQuery += sortedList.get(i);
				searchQuery += "+";
			}
			searchQuery += sortedList.get((limitForWords - 1));
		}

		return searchQuery;
	}

	/**
	 * Sorts the map in descending order of the frequency into a list
	 * 
	 * @param wordCount
	 * @return List of Strings as sorted list descending
	 */
	private static List<String> orderInDescendingFreqSequence(
			Map<String, Integer> wordCount) {

		List<Map.Entry<String, Integer>> list = new ArrayList<>(
				wordCount.entrySet());

		// Sort list by integer values
		Collections.sort(list, new Comparator<Map.Entry<String, Integer>>() {
			public int compare(Map.Entry<String, Integer> term1,
					Map.Entry<String, Integer> term2) {
				return (term2.getValue()).compareTo(term1.getValue());
			}
		});

		// Put sorted words into a list
		List<String> sortedList = new ArrayList<String>();
		for (Map.Entry<String, Integer> entry : list) {
			sortedList.add(entry.getKey());
		}

		return sortedList;
	}
}
