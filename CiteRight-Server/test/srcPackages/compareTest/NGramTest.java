package srcPackages.compareTest;

import org.junit.Ignore;
import org.junit.Test;

import compare.NGramComperator;
import de.dhbw.citeright.essentials.db.Section;
import static org.junit.Assert.*;

public class NGramTest {
	@Test	
	public void compareEquals(){
		Section section = new Section();
		section.setSectionText("Lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam");
		
		assertEquals(1f, NGramComperator.compare(section, section),0.001);
	}
	
	@Test	
	public void compareDifferent(){
		Section section1 = new Section();
		section1.setSectionText("consetetur sadipscing elitr sed diam");
		Section section2 = new Section();
		section2.setSectionText("Lorem ipsum dolor sit amet");
		
		assertEquals(0f, NGramComperator.compare(section1, section2),0.001);
	}
	
	@Test	
	public void compareEmpty(){
		Section section = new Section();
		section.setSectionText("");
		
		assertEquals(0f, NGramComperator.compare(section, section),0.001);
	}
	
	@Test	
	public void compareFirstEmpty(){
		Section section1 = new Section();
		section1.setSectionText("");
		Section section2 = new Section();
		section2.setSectionText("Lorem ipsum dolor sit amet");
		
		assertEquals(0f, NGramComperator.compare(section1, section2),0.001);
	}
	
	@Test	
	public void compareSecondEmpty(){
		Section section1 = new Section();
		section1.setSectionText("Lorem ipsum dolor sit amet");
		Section section2 = new Section();
		section2.setSectionText("");
		
		assertEquals(0f, NGramComperator.compare(section1, section2),0.001);
	}
	

	@Test	
	public void compareHalfEquals(){
		Section section1 = new Section();
		section1.setSectionText("Lorem ipsum dolor sit amet consetetur sadipscing elitr sed diam");
		Section section2 = new Section();
		section2.setSectionText("consetetur sadipscing elitr sed diam nonumy eirmod tempor invidunt ut");
		
		assertNotNull(NGramComperator.compare(section1, section2));
	}
}
