package srcPackages.mainTest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import convert.ConvertActor.ConvertedDocumentMessage;
import compare.NGramComperator;
import scala.collection.JavaConversions;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.server.search.SearchEngine;
import de.dhbw.citeright.server.search.SearchResult;
import de.dhbw.citeright.server.search.google.GoogleSearchEngine;
import extract.ExtractActor;
import extract.ExtractActor.SearchPhrasesMessage;

import org.junit.Ignore;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class SemanticTest {
    @Ignore
	@Test
	public void testSearchAndCompare() {
		ActorSystem system = ActorSystem.create("system");
		ActorRef mockedReceiver = system.actorOf(ReboundMockActor.props());
		List<Section> sections = new LinkedList<Section>();

		// prepare a Section for searching
		Section section1 = new Section();
		section1.setSectionText("Um die Qualität des Projekts hochhalten zu können, gilt es, bestimmte Maßgaben festzusetzen. Ohne diese ist eine zielgerichtete Festhaltung der erbrachten Leistungen und Ergebnisse nicht zu gewährleisten. Dazu sind die Qualitätsstandards nach den wesentlichen Punkten Code-Konventionen, der internen und der externen Dokumentation aufgeteilt. Die interne Dokumentation dient der Festhaltung der geleisteten Arbeit sowie der Dokumentation für spätere Arbeiten. Die externe Dokumentation dient der einheitlichen Darstellung von Ergebnissen für die Präsentation nach außen hin. Code-Konventionen sollen der Lesbarkeit und Einheitlichkeit des Quellcodes dienlich sein.");
		Section section2 = new Section();
		section2.setSectionText("Um die Qualität eines Projekts hochhalten zu können, müssen bestimmte Maßgaben festgesetzt werden. Ohne diese ist eine Festhaltung der erbrachten Leistungen und Arbeiten nicht nachzuweisen. Dazu sind die Qualitätsstandards nach den Punkten Code-Konventionen, der internen und der externen Dokumentation aufgeteilt. Die interne Dokumentation hilft bei der Festhaltung der geleisteten Arbeit sowie der Dokumentation für andere Arbeiten. Die externe Dokumentation dient der einheitlichen Darstellung von Ergebnissen für die Präsentation für den Kunden. Code-Konventionen sollen der Lesbarkeit und Einheitlichkeit des Quellcodes dienen.");
		sections.add(section1);
		DocumentContent document = new DocumentContent();
		LinkedList<Integer> roomIDs =new LinkedList<Integer>();
		roomIDs.add(5);roomIDs.add(6);roomIDs.add(7);
		ConvertedDocumentMessage mockedMessage = new ConvertedDocumentMessage(
				JavaConversions.asScalaBuffer(roomIDs).toList(), document);
		Timeout timeout = new Timeout(Duration.create(500, "seconds"));

		System.out.println("Starte Anfrage an ExtractActor");
		Future<Object> replyOfExtract = Patterns.ask(
				mockedReceiver,
				new ReboundMockActor.MockMessage(ExtractActor
						.props(mockedReceiver), mockedMessage), timeout);
		Object result = null;
		try {
			result = Await.result(replyOfExtract,
					Duration.create(1000, "seconds"));
			if (result instanceof SearchPhrasesMessage) {
				SearchPhrasesMessage message = (SearchPhrasesMessage) result;
				scala.collection.immutable.List<Section> buffer = message.phrases();
				// TODO: Wie bekomme ich hier die Strings raus
				System.out.println("Die Extraktion ergab die Suchwörter:\n");
				for (Section section : JavaConversions.asJavaIterable(buffer)) {
					System.out.println(section.getHeadwords());
				}
			}
		} catch (Exception e) {
			System.out.println("Fehler beim Antworten des ExtractActors");
			fail();
		}
		// search with headwords
		SearchEngine searchEngine = new GoogleSearchEngine();
		List<SearchResult> results = new ArrayList<SearchResult>();
		try {
			results = searchEngine.search("TODO");
		} catch (IOException e) {
			System.out.println("Fehler bei der Suchanfrage");
			e.printStackTrace();
			fail();
		}
		System.out.println("Suchergebnisse:\n");

		for (SearchResult item : results) {
			System.out.println(item.getUrl());
		}

		// compare two sections
		System.out.println("Vergleich der zwei Sectionen:\n"
				+ section1.getSectionText() + "\n\n"
				+ section2.getSectionText());
		float diceCoefficient = NGramComperator.compare(section1, section2);
		System.out.println("Der Vergleich ergab ein Plagiatsmaß von: "
				+ diceCoefficient);

		assertEquals(true, true);
	}
	
}
