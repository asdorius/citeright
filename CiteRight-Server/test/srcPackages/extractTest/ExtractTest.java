package srcPackages.extractTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Test;

import scala.collection.JavaConversions;
import scala.concurrent.Await;
import scala.concurrent.Future;
import scala.concurrent.duration.Duration;
import utility.ReboundMockActor;
import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.pattern.Patterns;
import akka.util.Timeout;
import convert.ConvertActor.ConvertedDocumentMessage;
import de.dhbw.citeright.essentials.db.DocumentContent;
import extract.ExtractActor;
import extract.NonRelevantWords;
import extract.RemoveNonRelevantWords;
import de.dhbw.citeright.essentials.db.Section;
import extract.WordFrequency;

public class ExtractTest {
	
	@Test
	public void testWordFrequency() {
				
		// create a string and test its' frequency
		String textRecord = "Vier Vier Drei Eins Zwei Vier Vier Drei Drei Zwei";
		String returnValue = "";
		returnValue = WordFrequency.getWordsInDescendingFreqOrder(textRecord);	
				
		// check if the list is correct
		assertEquals("Vier+Drei+Zwei+Eins", returnValue);

	}
	
	@Test
	public void testNonRelevantWords() {

		List<String> listWithRemovableWords = new ArrayList<>();
		listWithRemovableWords.add("der");
		listWithRemovableWords.add("die");
		listWithRemovableWords.add("das");		
		// create Instance of NonRelevantWords with the list of removable words
		NonRelevantWords myNonRelevantWords = new NonRelevantWords(listWithRemovableWords);
		
		// get List from NonRelevantWords
		List<String> testListWithRemovableWords = new ArrayList<>();
		testListWithRemovableWords.addAll(myNonRelevantWords.getAll());
		
		// check if the list is correct
		assertEquals("der", testListWithRemovableWords.get(0));
		assertEquals("die", testListWithRemovableWords.get(1));
		assertEquals("das", testListWithRemovableWords.get(2));

	}
	
	@Test
	public void testRemoveNonRelevantWords() {
		
		// prepare a list with removable words
		List<String> listWithRemovableWords = new ArrayList<>();
		listWithRemovableWords.add("der");
		listWithRemovableWords.add("die");
		listWithRemovableWords.add("das");
		listWithRemovableWords.add("Der");
		listWithRemovableWords.add("Die");
		listWithRemovableWords.add("Das");
		NonRelevantWords myNonRelevantWords = new NonRelevantWords(listWithRemovableWords);
		
		//
		Section mySection = new Section("hier ist mein text der Die das test", "defaultHeadword", 22, 2);
		RemoveNonRelevantWords myRemoveNonRelevantWords = new RemoveNonRelevantWords(mySection, myNonRelevantWords.getAll());
		
		Section myOverWorkedSection = mySection;
		try {
			// get the over-worked section
			myOverWorkedSection = myRemoveNonRelevantWords.call();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		// get attributes of the over-worked section and check if the attributes are correct
		assertEquals("hier ist mein text test", myOverWorkedSection.getSectionText());
		assertEquals("defaultHeadword", myOverWorkedSection.getHeadwords());
		assertEquals((Integer)22, myOverWorkedSection.getDocumentId());
		assertEquals((Integer)2, myOverWorkedSection.getOrderingNumber());
		
	}
	
}
