package de.dhbw.cirteright.server.search.test;

import java.io.IOException;
import java.util.List;

import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import de.dhbw.citeright.server.search.SearchResult;
import de.dhbw.citeright.server.search.bing.BingSearchEngine;

public class BingTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void test() {
		BingSearchEngine bing = new BingSearchEngine();
		try {
			List<SearchResult> results = bing.search("straße vz deutschland linien straßenverkehrs",5);
			Assert.assertNotNull(results);
			Assert.assertFalse(results.isEmpty());
			System.out.println(results.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
