package de.dhbw.citeright.webapplication.controller.connection;

import java.util.LinkedList;
import java.util.List;

import akka.actor.ActorRef;
import akka.actor.ActorSystem;
import akka.actor.Props;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.remoteactor.ProcessDocumentMessage;

/**
 * Provides the possibility of sending a message to the CiteRight-WorkloadServer
 * @author Florian Knecht
 *
 */
public class WorkloadServerConnector {
	private static final Config config = ConfigFactory.parseString("akka {actor {provider = \"akka.remote.RemoteActorRefProvider\"},remote {enabled-transports = [\"akka.remote.netty.tcp\"],netty.tcp {hostname = \"127.0.0.1\", port = 2552}}}");
	private static final ActorSystem system = ActorSystem.create("WebApp", config);
	private static final ActorRef connectionActor = system.actorOf(Props.create(ConnectionActor.class, CiteRightConstants.SERVER_INSTANCE_LOCATION));
	
	/**
	 * Sends a message to the CiteRight-WorkloadServer telling it to process a specific document.
	 * Checks against all rooms the owner of the Document has access to in addition to the internet.
	 * @param documentID Database-ID of the document to be processed.
	 */
	 public static void processDocument(int documentID){
		 Document doc = new DocumentService().getById(documentID);
		 Room room = new RoomService().getById(doc.getParentId());
		 List<Room> roomsAccessibleByDocumentOwner = new RoomService().getRoomsForUser(room.getOwnerId());
		 List<Integer> roomIDs = new LinkedList<Integer>();
		 for(Room accessibleRoom : roomsAccessibleByDocumentOwner){
			 roomIDs.add(accessibleRoom.getId());
		 }
		 processDocument(roomIDs, documentID);
	 }
	 /**
	  * Sends a message to the CiteRight-WorkloadServer telling it to process a specific document.
	  * Checks against all rooms provided in the RoomIDs parameter in addition to the internet.
	  * @param RoomIDs The rooms in which to check for plagiarisms. 
	  * Leaving this list empty results in only the internet being searched.
	  * @param documentID Database-ID of the document to be processed.
	  */
	 public static void processDocument(List<Integer> roomIDs, int documentID){
		 
		 connectionActor.tell(new ProcessDocumentMessage(roomIDs,documentID), null);
	 }
}
