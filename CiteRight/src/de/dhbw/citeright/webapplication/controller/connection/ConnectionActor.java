package de.dhbw.citeright.webapplication.controller.connection;

import static java.util.concurrent.TimeUnit.SECONDS;

import java.util.LinkedList;
import java.util.List;

import de.dhbw.citeright.essentials.remoteactor.ProcessDocumentMessage;
import de.dhbw.citeright.webapplication.localmodel.exceptions.MessageCacheFullException;
import scala.concurrent.duration.Duration;
import akka.actor.ActorRef;
import akka.actor.ActorIdentity;
import akka.actor.Identify;
import akka.actor.Terminated;
import akka.actor.UntypedActor;
import akka.actor.ReceiveTimeout;
import akka.japi.Procedure;

/**
 * Establishes and holds the connection to an Actor given by it's path. 
 * Only applicable for establishing connections to a CiteRight-WorkloadServer Application.
 * @author Florian Knecht
 *
 */
public class ConnectionActor extends UntypedActor {

  private final String path;
  private ActorRef citeRightServer = null;
  private List<Object> cachedMessages = new LinkedList<Object>();

  /**
   * 
   * @param path Akka path on which to look for an Actor.
   */
  public ConnectionActor(String path) {
    this.path = path;
    sendIdentifyRequest();
  }

  private void sendIdentifyRequest() {
    getContext().actorSelection(path).tell(new Identify(path), getSelf());
    getContext()
        .system()
        .scheduler()
        .scheduleOnce(Duration.create(3, SECONDS), getSelf(),
            ReceiveTimeout.getInstance(), getContext().dispatcher(), getSelf());
  }

  /**
   * Waits for an answer from the remoteActor, then switches to different behaviour.
   * Caches incoming requests for processing when the remote actor becomes available.
   * Only caches requests until a certain amount is reached to prevent extensive memory usage.
   */
  @Override
  public void onReceive(Object message) throws Exception {
    if (message instanceof ActorIdentity) {
    	citeRightServer = ((ActorIdentity) message).getRef();
      if (citeRightServer == null) {
        System.out.println("Remote actor not available: " + path);
      } else {
        getContext().watch(citeRightServer);
        //empty cache when entering activeMode.
        for (Object msg:cachedMessages){
        	citeRightServer.tell(msg, getSelf());
        }
        cachedMessages.clear();
        getContext().become(active, true);
      }

    } else if (message instanceof ReceiveTimeout) {
      sendIdentifyRequest();

    } else {
    	if (cachedMessages.size() <= 1000){
    		   cachedMessages.add(message);
    	}
    	else {
    		throw new MessageCacheFullException();
    	}

      System.out.println("Not ready yet");

    }
  }

  Procedure<Object> active = new Procedure<Object>() {
    @Override
    public void apply(Object message) {
      if (message instanceof ProcessDocumentMessage) {
        // send message to server actor
    	  citeRightServer.tell(message, getSelf());
      } else if (message instanceof Terminated) {
        System.out.println("WorkloadServer terminated");
        sendIdentifyRequest();
        getContext().unbecome();

      } else if (message instanceof ReceiveTimeout) {
        // ignore

      } else {
        unhandled(message);
      }

    }
  };
}