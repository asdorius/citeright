package de.dhbw.citeright.webapplication.controller;

import java.util.Random;

import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.RoomService;

/**
 * Generates room tokens.
 * 
 * @author Jan (i13015)
 */
public class TokenGenerator {
	public static final int standardTokenLength = 10;

	/**
	 * Generates a pseudo-random token for a room that is not assigned to any
	 * other room.
	 * 
	 * @return a valid, unique token.
	 */
	public static String generateUnique() {

		String generatedToken = "";
		boolean tokenIsUnique = false;

		while (!tokenIsUnique) {
			generatedToken = generate(standardTokenLength);

			if (!isTokenAlreadyInUse(generatedToken)) {
				tokenIsUnique = true;
			}
		}

		return generatedToken;
	}

	/**
	 * Generates a pseudo-random token.
	 * @param length how long the token should be.
	 * @return the generated token.
	 */
	public static String generate(int length) {
		Random random = new Random();
		String alphabet = "1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

		String generatedToken = "";
		for (int i = 0; i < length; i++) {
			generatedToken += alphabet
					.charAt(random.nextInt(alphabet.length()));
		}
		return generatedToken;
	}

	/**
	 * Checks whether a token is already assigned to a room.
	 * @param token token to check.
	 * @return true if token is already in use, false otherwise.
	 */
	public static boolean isTokenAlreadyInUse(String token) {
		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());
		Room collidingRoom = roomService.getRoomByToken(token);

		if (collidingRoom == null) {
			return false;
		} else {
			return true;
		}
	}
}