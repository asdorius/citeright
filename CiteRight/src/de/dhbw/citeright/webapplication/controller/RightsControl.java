/**
 * 
 */
package de.dhbw.citeright.webapplication.controller;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;

/**
 * This class tests if the user has enough rights to see something in the view.
 * 
 * @author Kai Holzer
 *
 */
public class RightsControl {

	/**
	 * Checks if the user is an administrator.
	 * 
	 * @param email
	 *            the email of the currently logged in user
	 * @return returns true if the user is an administrator.
	 * @throws UserNotFoundException
	 *             if an invalid user is requested
	 */
	public static boolean isAdmin(String email) throws UserNotFoundException {
		UserService userService = new UserService(
				HibernateUtils.getSessionFactory());
		User user = userService.getUserByEmail(email);
		int role;

		try {
			role = user.getRoleId();
		} catch (NullPointerException e) {
			throw new UserNotFoundException("User with email address " + email
					+ " is not existent.");
		}

		if (role == CiteRightConstants.ADMINISTRATOR) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Checks if the given user has the Administrator-role assigned
	 * 
	 * @param userId
	 *            user ID of the user
	 * @return true if user has Admin role, false otherwise
	 * @throws UserNotFoundException
	 *             if an invalid user is requested
	 */
	public static boolean isAdmin(int userId) throws UserNotFoundException {
		UserService userService = new UserService(
				HibernateUtils.getSessionFactory());
		User user = userService.getById(userId);
		int role;

		try {
			role = user.getRoleId();
		} catch (NullPointerException e) {
			throw new UserNotFoundException("User with ID " + userId
					+ " is not existent.");
		}

		if (role == 1) {
			return true;
		} else {
			return false;
		}
	}
}
