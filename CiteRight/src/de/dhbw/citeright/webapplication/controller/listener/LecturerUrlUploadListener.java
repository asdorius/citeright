package de.dhbw.citeright.webapplication.controller.listener;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Reference;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.ReferenceService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;

/**
 * Listener for upload of an url by a lecturer
 * 
 * @author Kai Holzer
 *
 */
public class LecturerUrlUploadListener implements Button.ClickListener {
	private static final long serialVersionUID = 6442464133823678780L;

	LecturerUploadView lecturerUploadView;

	public LecturerUrlUploadListener(LecturerUploadView lecturerUploadView) {
		this.lecturerUploadView = lecturerUploadView;
	}

	/**
	 * temporary saves a html file and upload it to the database.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String urlTitle = lecturerUploadView.getUrlUploadTitle();
		String urlString = lecturerUploadView.getUrlUploadLink();
		String mimeType = "text/html";
		Timestamp uploadTime = new Timestamp(new Date().getTime());

		String filename = urlTitle + ".html";

		byte[] data = null;

		if (lecturerUploadView.getAuthor().equals("")) {
			Notification.show("Fehler!", "Bitte geben Sie einen Autor an",
					Type.ERROR_MESSAGE);
			return;
		}

		try {
			URL url = new URL(urlString);
			InputStream stream = url.openStream();
			data = IOUtils.toByteArray(stream);
			
		} catch (IOException e) {
			Notification.show("Fehler!", "Upload fehlgeschlagen",
					Type.ERROR_MESSAGE);
		}

		// Upload to the chosen room
		if (data != null) {

			Reference reference = new Reference(urlTitle, urlTitle, urlString);
			ReferenceService referenceService = new ReferenceService(
					HibernateUtils.getSessionFactory());
			int referenceId = referenceService.create(reference);

			// Limit the maximal size of a file to value of the constant
			if (data.length > CiteRightConstants.MAX_FILESIZE) {
				Notification.show("Fehler", "Die Datei ist zu groß",
						Type.ERROR_MESSAGE);
				return;
			}

			Room room = lecturerUploadView.getSelectedRoom();

			if (room != null && room.getId() > 0) {
				Document document = new Document(filename, mimeType, data,
						uploadTime, room.getId());
				document.setStudentName(lecturerUploadView.getAuthor());
				document.setStudentMatriculationNumber("DOZENTENUPLOAD");
				document.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);
				document.setReferenceId(referenceId);

				DocumentService documentService = new DocumentService(
						HibernateUtils.getSessionFactory());

				if (documentService.create(document) > 0) {
					Notification.show("Erfolgreich", "Datei wurde in Raum "
							+ room.getRoomName() + " hochgeladen",
							Type.HUMANIZED_MESSAGE);
					// Trigger start at compute server.
					WorkloadServerConnector.processDocument(document.getId());

				} else {
					Notification.show("Fehler!",
							"Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
				}
			} else {
				Notification.show("Fehler!", "Es wurde kein Raum ausgewählt",
						Type.ERROR_MESSAGE);
			}
		}
		// Deletes the temporary file

	}

}
