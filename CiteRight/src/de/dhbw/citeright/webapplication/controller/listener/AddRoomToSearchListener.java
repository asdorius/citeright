package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.IncludedRoomService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * Listener to add a room to the search.
 * 
 * @author Kai Holzer
 *
 */
public class AddRoomToSearchListener implements Button.ClickListener {
	private static final long serialVersionUID = 6056328610928233596L;

	RoomSettingsView roomSettingsView;
	IncludedRoomService includedRoomService;

	public AddRoomToSearchListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		includedRoomService = new IncludedRoomService();
	}

	/**
	 * Includes an other room to the search
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		Room room = roomSettingsView.getSelectedRoom();

		if (room == null) {
			Notification.show("Fehler!", "Bitte einen Raum auswählen",
					Type.ERROR_MESSAGE);
			return;
		}
		int currentRoomId = roomSettingsView.getRoomId();
		int roomToInclude = room.getId();

		// Check if the room is already included if not include it.
		if (includedRoomService.get(currentRoomId, roomToInclude) == null) {
			IncludedRoom includedRoom = new IncludedRoom(currentRoomId,
					roomToInclude);
			includedRoomService.create(includedRoom);

			roomSettingsView.loadIncludedRooms();
			Notification.show("Erfolgreich!",
					"Der Raum wurde zur Suche hinzugefügt",
					Type.HUMANIZED_MESSAGE);
		} else {
			Notification.show("Fehler!", "Der Raum wurde bereits hinzugefügt",
					Type.ERROR_MESSAGE);
		}

	}

}
