package de.dhbw.citeright.webapplication.controller.listener;

import java.nio.charset.Charset;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;

/**
 * Listener for the upload of a text by a lecturer
 * 
 * @author Kai Holzer
 *
 */
public class LecturerTextUploadListener implements Button.ClickListener {
	private static final long serialVersionUID = -4045799695365486876L;

	LecturerUploadView lecturerUploadView;

	public LecturerTextUploadListener(LecturerUploadView lecturerUploadView) {
		this.lecturerUploadView = lecturerUploadView;
	}

	/**
	 * Creates a text file and upload it to the database.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String textTitle = lecturerUploadView.getTextUploadTitle();
		String textContent = lecturerUploadView.getTextUploadContent();

		Timestamp uploadTime = new Timestamp(new Date().getTime());
		String mimeType = "text/plain";
		String filename = textTitle + ".txt";
		byte[] data = null;


		if (textTitle.equals("") || textContent.equals("")) {
			Notification.show("Fehler!", "Bitte alle Felder ausfüllen",
					Type.ERROR_MESSAGE);
			return;
		}

		if (lecturerUploadView.getAuthor().equals("")) {
			Notification.show("Fehler!", "Bitte geben Sie einen Autor an",
					Type.ERROR_MESSAGE);
			return;
		}
		// Check if there is a linebreak in the last 100 characters and if not
		// it makes one.
		for (int i = 0; i < textContent.length(); i++) {
			if (i % 100 == 0 && i != 0) {
				String substring = textContent.substring(i - 100, i);
				if (!substring.contains("\n")) {
					boolean newLineSet = false;
					for (int j = 0; j < 100; j++) {
						if (textContent.charAt(i - j) == ' ' && !newLineSet) {
							StringBuilder stringBuilder = new StringBuilder(
									textContent);
							stringBuilder.insert(i - j, "\n");
							System.out.println("LineBreak wurde eingefügt");
							textContent = stringBuilder.toString();
							newLineSet = true;
						}
					}
				}
			}
		}

		System.out.println(textContent);

		// get the byte[] for textContent
		data = textContent.getBytes(Charset.forName("UTF-8"));

		// Upload to the chosen room
		if (data != null) {

			// Limit the maximal size of a file to value of the constant
			if (data.length > CiteRightConstants.MAX_FILESIZE) {
				Notification.show("Fehler", "Die Datei ist zu groß",
						Type.ERROR_MESSAGE);
				return;
			}

			Room room = lecturerUploadView.getSelectedRoom();

			if (room != null && room.getId() > 0) {
				Document document = new Document(filename, mimeType, data,
						uploadTime, room.getId());
				document.setStudentName(lecturerUploadView.getAuthor());
				document.setStudentMatriculationNumber("DOZENTENUPLOAD");
				document.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);

				DocumentService documentService = new DocumentService(
						HibernateUtils.getSessionFactory());

				if (documentService.create(document) > 0) {
					Notification.show("Erfolgreich", "Datei wurde in Raum "
							+ room.getRoomName() + " hochgeladen",
							Type.HUMANIZED_MESSAGE);
					// Trigger start at compute server.
					WorkloadServerConnector.processDocument(document.getId());

				} else {
					Notification.show("Fehler!",
							"Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
				}
			} else {
				Notification.show("Fehler!", "Es wurde kein Raum ausgewählt",
						Type.ERROR_MESSAGE);
			}
		}
	}

}