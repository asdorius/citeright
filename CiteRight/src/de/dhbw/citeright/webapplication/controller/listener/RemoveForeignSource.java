package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * Listener to remove an foreign source.
 * 
 * @author Kai Holzer
 *
 */
public class RemoveForeignSource implements Button.ClickListener {
	private static final long serialVersionUID = -4216199695782730589L;

	RoomSettingsView roomSettingsView;
	DocumentService documentService;

	public RemoveForeignSource(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		documentService = new DocumentService();
	}

	/**
	 * Removes an foreign source in the database.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		Document document = (Document) event.getButton().getData();

		if (documentService.delete(document)) {
			Notification.show("Erfolgreich!",
					"Die Webseite wurde von der Suche entfernt",
					Type.HUMANIZED_MESSAGE);
			roomSettingsView.deleteForeignSearch(document.getId());
			roomSettingsView.loadForeignSources();
		} else {
			Notification.show("Fehler",
					"Die Webseite konnte nicht entfernt werden",
					Type.ERROR_MESSAGE);
		}

	}

}
