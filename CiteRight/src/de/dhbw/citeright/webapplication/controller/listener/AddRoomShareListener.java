package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.AccessService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * This listener is called when the user adds a new room share to a given room.
 * 
 * @author Jan (i13015)
 *
 */
public class AddRoomShareListener implements Button.ClickListener {
	private static final long serialVersionUID = 7256074074291457656L;
	private RoomSettingsView roomSettingsView;
	private UserService userService;
	private AccessService accessService;

	/**
	 * Creates a new listener to add a room share.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on which the event will be triggered.
	 */
	public AddRoomShareListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
		accessService = new AccessService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed when the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		// Try to get the e-mail address of the user with whom the room shall be
		// shared.
		String newUserEmail = roomSettingsView.getNewRoomShareUserEmail();

		// Ensure that an e-mail address / username was provided.
		if (newUserEmail.equals("")) {
			Notification
					.show("Fehler!",
							"Bitte geben Sie die E-Mail-Adresse des Benutzers ein, den Sie für diesen Raum freigeben möchten.",
							Type.ERROR_MESSAGE);
			return;
		}

		// Ensure that the User exists
		User newUser = userService.getUserByEmail(newUserEmail);

		if (newUser == null) {
			Notification
					.show("Fehler!",
							"Die Freigabe konnte erstellt werden, da kein Benutzer mit dieser E-Mail-Adresse gefunden wurde.",
							Type.ERROR_MESSAGE);
			return;
		}

		// Ensure that the user has not shared the room with himself
		Integer currentUserId = Integer.valueOf(roomSettingsView
				.getCurrentUserId());

		if (newUser.getId() == currentUserId) {
			Notification.show("Fehler!",
					"Sie können sich den Raum nicht selbst freigeben.",
					Type.ERROR_MESSAGE);
			return;
		}

		// Ensure that the room is not already shared with this user
		Access currentShare = accessService.getRoomShare(newUser.getId(),
				roomSettingsView.getRoomId());
		if (currentShare != null) {
			Notification.show("Fehler!",
					"Der Raum wurde diesem Benutzer bereits freigegeben.",
					Type.ERROR_MESSAGE);
			return;
		}

		// Add the room share to the database.
		Access newRoomShare = new Access();
		newRoomShare.setUserId(newUser.getId());
		newRoomShare.setRoomId(roomSettingsView.getRoomId());

		try {
			accessService.create(newRoomShare);
		} catch (Exception e) {
			// Entry couldn't be saved.
			Notification
					.show("Fehler!",
							"Die Freigabe konnte leider nicht angelegt werden, da ein Fehler aufgetreten ist.",
							Type.ERROR_MESSAGE);
			return;
		}

		roomSettingsView.updateSharedUsers();
	}
}
