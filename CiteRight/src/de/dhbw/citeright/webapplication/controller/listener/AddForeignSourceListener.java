package de.dhbw.citeright.webapplication.controller.listener;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Reference;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.ReferenceService;
import de.dhbw.citeright.essentials.helper.RandomStringHelper;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * Adds a foreign source to a room which gets included to the search.
 * 
 * @author Kai Holzer
 *
 */
public class AddForeignSourceListener implements Button.ClickListener {
	private static final long serialVersionUID = 6056328610928233596L;

	RoomSettingsView roomSettingsView;

	public AddForeignSourceListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
	}

	/**
	 * Adds the website as a foreign source.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		String urlString = roomSettingsView.getWebsiteToSearch();
		String mimeType = "text/html";
		Timestamp uploadTime = new Timestamp(new Date().getTime());

		String filename = "ForeignSource_"
				+ RandomStringHelper.getRandomString(10) + ".html";
		File file = new File(filename);

		byte[] data = null;
		FileInputStream inputStream;

		// Download the website temporary as a html file.
		try {
			URL url = new URL(urlString);
			ReadableByteChannel readableByteChannel = Channels.newChannel(url
					.openStream());
			FileOutputStream fos = new FileOutputStream(file);
			fos.getChannel().transferFrom(readableByteChannel, 0,
					Long.MAX_VALUE);
			fos.close();
			inputStream = new FileInputStream(file);
			data = IOUtils.toByteArray(inputStream);
		} catch (IOException e) {
			Notification.show("Fehler!", "Hinzufügen fehlgeschlagen",
					Type.ERROR_MESSAGE);
		}

		// Upload to the chosen room
		if (data != null) {

			Room room = roomSettingsView.getThisRoom();

			Reference reference = new Reference("ForeignSource in "
					+ room.getRoomName(), "ForeignSource in "
					+ room.getRoomName(), urlString);
			ReferenceService referenceService = new ReferenceService(
					HibernateUtils.getSessionFactory());
			int referenceId = referenceService.create(reference);

			// Limit the maximal size of a file to 40 000 000 Bytes
			if (data.length > 40000000) {
				Notification.show("Fehler", "Die Datei ist zu groß",
						Type.ERROR_MESSAGE);
				file.delete();
				return;
			}

			if (room != null && room.getId() > 0) {
				Document document = new Document(filename, mimeType, data,
						uploadTime, room.getId());
				document.setReferenceId(referenceId);
				document.setForeignSource(true);

				DocumentService documentService = new DocumentService(
						HibernateUtils.getSessionFactory());

				if (documentService.create(document) > 0) {
					Notification.show("Erfolgreich",
							"Datei wurde zur Suche hinzugefügt",
							Type.HUMANIZED_MESSAGE);
					roomSettingsView.loadForeignSources();
					roomSettingsView.setWebsiteToSearch("");

					// Trigger start at compute server.
					WorkloadServerConnector.processDocument(document.getId());

				} else {
					Notification.show("Fehler!",
							"Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
				}
			} else {
				Notification.show("Fehler!", "Es ist ein Fehler aufgetreten.",
						Type.ERROR_MESSAGE);
			}
		}
		// Deletes the temporary file
		file.delete();
	}

}
