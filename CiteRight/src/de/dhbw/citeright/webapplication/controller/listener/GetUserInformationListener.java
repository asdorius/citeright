package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Notification;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * Sets the user information in the administrator settings.
 * 
 * @author Kai Holzer
 *
 */
public class GetUserInformationListener implements Button.ClickListener {
	private static final long serialVersionUID = 2578807146651624851L;

	private SettingsView settingsView;
	private UserService userService;

	public GetUserInformationListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Sets the values like first name, lastname and email in the user
	 * information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = settingsView.getUserInformationEmailRequest();
		User user = userService.getUserByEmail(email);

		if (user != null) {
			String firstName = user.getFirstName();
			String lastName = user.getLastName();

			settingsView.setUserInformation(email, firstName, lastName);
		} else {
			Notification.show("Fehler!",
					"Der angegebene Benutzer existiert nicht",
					Type.ERROR_MESSAGE);
		}

	}

}
