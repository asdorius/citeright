package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.service.IncludedRoomService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * Listener to remove a included room from the search
 * 
 * @author Kai Holzer
 *
 */
public class RemoveRoomFromSearch implements Button.ClickListener {
	private static final long serialVersionUID = 4327710348119254065L;

	RoomSettingsView roomSettingsView;
	IncludedRoomService includedRoomService;

	public RemoveRoomFromSearch(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		includedRoomService = new IncludedRoomService();
	}

	/**
	 * Deletes an included room from the search.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		int includedRoomId = (int) event.getButton().getData();
		int currentRoomId = roomSettingsView.getRoomId();
		
		IncludedRoom includedRoom  = includedRoomService.get(currentRoomId, includedRoomId);
		if (includedRoomService.delete(includedRoom)) {
			Notification.show("Erfolgreich", "Der Raum wurde von der Suche ausgeschlossen", Type.HUMANIZED_MESSAGE);
			roomSettingsView.deleteIncludedRoom(includedRoomId);
			roomSettingsView.loadIncludedRooms();
		} else {
			Notification.show("Fehler", "Der Raum konnte nicht entfernt werden", Type.ERROR_MESSAGE);
		}
				
	}

}
