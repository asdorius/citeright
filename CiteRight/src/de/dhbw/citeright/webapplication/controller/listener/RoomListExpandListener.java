package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Tree;
import com.vaadin.ui.TreeTable;

import de.dhbw.citeright.webapplication.view.pages.RoomListView;

/**
 * This listener is called when the user expands a branch in a tree table.
 * 
 * @author Jan (i13015)
 *
 */
public class RoomListExpandListener implements Tree.ExpandListener {
	private static final long serialVersionUID = 8490629595577370861L;
	private RoomListView roomSettingsView;

	/**
	 * Creates a new listener on {@link TreeTable} collapsing.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on which the event will be triggered.
	 */
	public RoomListExpandListener(RoomListView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
	}

	/**
	 * Will be executed when a branch is collapsed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void nodeExpand(Tree.ExpandEvent event) {

		// Update table height
		roomSettingsView.updateRoomTableSize();
	}
}
