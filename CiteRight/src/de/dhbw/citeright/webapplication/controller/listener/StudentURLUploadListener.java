package de.dhbw.citeright.webapplication.controller.listener;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.sql.Timestamp;
import java.util.Date;

import org.apache.commons.io.IOUtils;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Reference;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.ReferenceService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * This listener is called when a user hands in an document via url input.
 * 
 * @author Kai Holzer
 *
 */
@SuppressWarnings("serial")
public class StudentURLUploadListener implements Button.ClickListener {

	private StudentUploadView studentUploadView;

	/**
	 * Initializes the listener for a given View.
	 * 
	 * @param studentUploadView
	 *            The view where the listener is used.
	 */
	public StudentURLUploadListener(StudentUploadView studentUploadView) {
		this.studentUploadView = studentUploadView;
	}

	/**
	 * This method will be executed, when a button is clicked. It will check if
	 * a given URL is correct and upload the file to the database.
	 * 
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		if (!studentUploadView.formFilledOut()
				|| studentUploadView.getUploadUrl().equals("")) {
			Notification.show("Fehler!",
					"Es sind nicht alle Felder ausgefüllt", Type.ERROR_MESSAGE);
			return;
		}
		String urlTitle = studentUploadView.getUploadUrlTitle();
		String urlString = studentUploadView.getUploadUrl();

		String mimeType = "text/html";
		Timestamp uploadTime = new Timestamp(new Date().getTime());

		String filename = urlTitle + ".html";

		byte[] data = null;

		// Download the website temporary as a html file.
		try {
			URL url = new URL(urlString);
			InputStream stream = url.openStream();
			data = IOUtils.toByteArray(stream);
			
		} catch (IOException e) {
			Notification.show("Fehler!", "Upload fehlgeschlagen",
					Type.ERROR_MESSAGE);
		}

		// Upload to the chosen room
		if (data != null) {

			Reference reference = new Reference(urlTitle, urlTitle, urlString);
			ReferenceService referenceService = new ReferenceService(
					HibernateUtils.getSessionFactory());
			int referenceId = referenceService.create(reference);

			// Limit the maximal size of a file to value of the constant
			if (data.length > CiteRightConstants.MAX_FILESIZE) {
				Notification.show("Fehler", "Die Datei ist zu groß",
						Type.ERROR_MESSAGE);
				return;
			}

			RoomService roomService = new RoomService(
					HibernateUtils.getSessionFactory());

			Room room = roomService.getRoomByToken(studentUploadView
					.getTokenCode());

			if (room != null && room.getId() > 0) {
				Document document = new Document(filename, mimeType, data,
						uploadTime, room.getId());
				document.setStudentName(studentUploadView.getStudentName());
				document.setStudentMatriculationNumber(studentUploadView
						.getStudentMatriculationNumber());
				document.setStudentEmail(studentUploadView.getStudentEmail());
				document.setReferenceId(referenceId);
				document.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);

				DocumentService documentService = new DocumentService(
						HibernateUtils.getSessionFactory());

				if (documentService.create(document) > 0) {
					Notification.show("Erfolgreich", "Datei wurde in Raum "
							+ room.getRoomName() + " hochgeladen",
							Type.HUMANIZED_MESSAGE);
					// Trigger start at compute server.
					WorkloadServerConnector.processDocument(document.getId());

				} else {
					Notification.show("Fehler!",
							"Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
				}
			} else {
				Notification.show("Fehler!", "Es wurde kein Raum ausgewählt",
						Type.ERROR_MESSAGE);
			}
		}
	}

}
