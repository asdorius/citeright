package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Notification;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * Listener if an administrator want to change the password from a user.
 * 
 * @author Kai Holzer
 *
 */
public class AdminChangePasswordListener implements Button.ClickListener {
	private static final long serialVersionUID = -1834035774112395610L;

	private SettingsView settingsView;
	private UserService userService;

	public AdminChangePasswordListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Changes the password of the user.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = settingsView.getEmailForAdminPasswordChange();
		String password = settingsView.getPasswordForAdminPasswordChange();
		String passwordRepeat = settingsView
				.getPasswordRepeatForAdminPasswordChange();
		if (!email.equals("")
				&& !password.equals("")
				&& passwordRepeat.equals(settingsView
						.getPasswordForAdminPasswordChange())) {

			if (userService.changePassword(email, password)) {
				Notification.show("Erfolgreich", "Das Passwort wurde geändert",
						Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("Fehler", "Passwort ändern fehlgeschlagen",
						Type.HUMANIZED_MESSAGE);
			}

		} else {
			Notification.show("Fehler!", "Überprüfen Sie Ihre Eingabe",
					Type.ERROR_MESSAGE);
		}
	}
}
