package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * The listener gets called if an administrator accept a user request
 * 
 * @author Kai Holzer
 *
 */
public class AcceptUserRequestListener implements Button.ClickListener {
	private static final long serialVersionUID = 4168423392283402829L;

	private SettingsView settingsView;
	private UserService userService;

	public AcceptUserRequestListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Set the roleID from the user who should get activated.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		int id = (int) event.getButton().getData();

		if (userService.acceptUserRequest(id)) {
			Notification.show("Erfolgreich", "Der User mit der ID " + id
					+ " wurde freigeschaltet", Type.HUMANIZED_MESSAGE);
			settingsView.deleteUserRequest(id);
		} else {
			Notification.show("Fehler",
					"Der User konnte nicht freigeschaltet werden",
					Type.ERROR_MESSAGE);
		}
	}

}
