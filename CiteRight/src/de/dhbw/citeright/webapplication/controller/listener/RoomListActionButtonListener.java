package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.view.pages.RoomListView;

/**
 * This listener is called when the user adds a button in the room list.
 * 
 * @author Jan (i13015)
 *
 */
public class RoomListActionButtonListener implements Button.ClickListener {
	private static final long serialVersionUID = -3737343559037981443L;
	private RoomListView roomListView;

	/**
	 * Creates a new listener on the room list's buttons.
	 * 
	 * @param roomListView
	 *            reference to the view on which the event will be triggered.
	 */
	public RoomListActionButtonListener(RoomListView roomListView) {
		this.roomListView = roomListView;
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		AttributedButton button = (AttributedButton) event.getButton();

		Integer roomId = (Integer) button.getAttribute("roomId");
		String action = (String) button.getAttribute("action");

		if (roomId != null && action != null) {
			switch (action) {
			case "showFilesInRoom":
				roomListView.gotoToDocumentsInRoomView(roomId);
				break;
			case "createSubroom":
				roomListView.gotoToCreateSubroomView(roomId);
				break;
			case "modifySettings":
				roomListView.gotoToChangeRoomSettingsView(roomId);
				break;
			}
		}
	}

}
