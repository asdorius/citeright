package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * This listener is called when the user changes the name of a room.
 * 
 * @author Jan (i13015)
 *
 */
public class ChangeRoomNameListener implements Button.ClickListener {
	private static final long serialVersionUID = 8635822334455964185L;

	private RoomSettingsView roomSettingsView;
	private RoomService roomService;

	/**
	 * Creates a new listener to change a room's name.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on that the event will be triggered.
	 */
	public ChangeRoomNameListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		roomService = new RoomService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String newRoomName = roomSettingsView.getRoomName();

		if (newRoomName.equals("")) {
			Notification.show("Fehler!",
					"Bitte geben Sie einen Namen für den Raum ein.",
					Type.ERROR_MESSAGE);
		} else {
			// Update room with given information
			roomSettingsView.thisRoom.setRoomName(newRoomName);

			boolean saveSuccessful;

			try {
				roomService.update(roomSettingsView.thisRoom);
				saveSuccessful = true;
			} catch (Exception e) {
				// Entry couldn't be saved.
				saveSuccessful = false;
			}

			if (saveSuccessful) {
				Notification.show("Erfolg", "Der Raumname wurde geändert.",
						Type.HUMANIZED_MESSAGE);
				roomSettingsView.resetRoomName();
			} else {
				Notification
						.show("Fehler!",
								"Der Raumname konnte nicht geändert werden, da ein Fehler aufgetreten ist.",
								Type.ERROR_MESSAGE);
			}
		}
	}
}
