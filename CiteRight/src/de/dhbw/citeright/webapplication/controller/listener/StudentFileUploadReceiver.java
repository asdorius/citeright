package de.dhbw.citeright.webapplication.controller.listener;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * Receives a file from the StudentUploadView and saves it it the database.
 * 
 * @author Kai Holzer
 *
 */
public class StudentFileUploadReceiver implements SucceededListener,
		FailedListener, Receiver {
	private static final long serialVersionUID = 4350719034429968497L;
	private String filename;
	private String mimeType;
	private ByteArrayOutputStream output;
	private Timestamp uploadTime;
	private StudentUploadView studentUploadView;

	public StudentFileUploadReceiver(StudentUploadView studentUploadView) {
		this.studentUploadView = studentUploadView;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		this.uploadTime = new Timestamp(new Date().getTime());

		output = new ByteArrayOutputStream();

		return output;
	}

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Upload fehlgeschlagen",
				event.getReason().toString(), Type.ERROR_MESSAGE);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {

		// Tests if all fields are filled out
		if (!studentUploadView.formFilledOut()) {
			Notification.show("Fehler!",
					"Bitte füllen Sie alle Pflichtfelder aus.",
					Type.ERROR_MESSAGE);
			return;
		}

		if (filename.length() == 0) {
			Notification
					.show("Fehler!", "Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
			return;
		}

		if (!supportedMimeType()) {
			Notification.show("Fehler",
					"Dieser Dateityp wird nicht unterstützt",
					Type.ERROR_MESSAGE);
			return;
		}

		// Limit the maximal size of a file to value of the constant
		if (((ByteArrayOutputStream) output).toByteArray().length > CiteRightConstants.MAX_FILESIZE) {
			Notification.show("Fehler", "Die Datei ist zu groß",
					Type.ERROR_MESSAGE);
			return;
		}

		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());

		Room room = roomService
				.getRoomByToken(studentUploadView.getTokenCode());

		if (room != null && room.getId() > 0) {

			Document document = new Document(filename, mimeType,
					((ByteArrayOutputStream) output).toByteArray(), uploadTime,
					room.getId());
			document.setStudentEmail(studentUploadView.getStudentEmail());
			document.setStudentName(studentUploadView.getStudentName());
			document.setStudentMatriculationNumber(studentUploadView
					.getStudentMatriculationNumber());
			document.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);

			DocumentService documentService = new DocumentService(
					HibernateUtils.getSessionFactory());
			if (documentService.create(document) > 0) {
				Notification.show("Erfolgreich",
						"Datei wurde in Raum " + room.getRoomName()
								+ " hochgeladen", Type.HUMANIZED_MESSAGE);

				// Trigger start at compute server.
				WorkloadServerConnector.processDocument(document.getId());

			} else {
				Notification.show("Fehler!",
						"Schreiben in Datenbank fehlgeschlagen",
						Type.ERROR_MESSAGE);
			}
		} else {
			Notification
					.show("Fehler!", "Invalider Token.", Type.ERROR_MESSAGE);
		}

	}

	/**
	 * tests if the document is a supported file type
	 * 
	 * @return true if its a valid type, else false
	 */
	private boolean supportedMimeType() {
		if (mimeType.equalsIgnoreCase("application/pdf")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("text/html")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("text/plain")) {
			return true;
		} else if (mimeType
				.equalsIgnoreCase("application/vns.openxmlformats-officedocument.wordprocessingml.document")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("application/msword")) {
			return true;
		} else if (mimeType
				.equalsIgnoreCase("application/vnd.oasis.opendocument.text")) {
			return true;
		} else {
			return false;
		}
	}

}
