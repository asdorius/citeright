package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.AccessService;
import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * This listener is called when the user tries to revoke a room share from a
 * given room.
 * 
 * @author Jan (i13015)
 *
 */
public class RevokeRoomShareListener implements Button.ClickListener {
	private static final long serialVersionUID = -8089210153450791693L;
	private RoomSettingsView roomSettingsView;

	private AccessService accessService;

	/**
	 * Creates a new listener to revoke a room share.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on which the event will be triggered.
	 */
	public RevokeRoomShareListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		accessService = new AccessService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		// Try to get the user ID of the user whose share shall be revoked.
		AttributedButton triggeredButton = (AttributedButton) event.getButton();

		Integer sharedUserId;
		sharedUserId = (Integer) triggeredButton.getAttribute("sharedUserId");
		if (sharedUserId == null) {
			Notification.show("Fehler!",
					"Es wurden keine Benutzerdaten übergeben.",
					Type.ERROR_MESSAGE);
			return;
		}

		// Find the assignment in the database
		Access currentShare = accessService.getRoomShare(sharedUserId,
				roomSettingsView.getRoomId());
		if (currentShare == null) {
			Notification.show("Fehler!", "Diese Raumfreigabe existiert nicht.",
					Type.ERROR_MESSAGE);
			return;
		}

		// Try to remove the assignment from the database.
		try {
			accessService.delete(currentShare);
		} catch (Exception e) {
			// Entry couldn't be saved.
			Notification
					.show("Fehler!",
							"Die Freigabe konnte leider nicht gelöscht werden, da ein Fehler aufgetreten ist.",
							Type.ERROR_MESSAGE);
			return;
		}

		roomSettingsView.updateSharedUsers();
	}
}
