package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * Listener for the change of the first name of an user by an administrator
 * 
 * @author Kai Holzer
 *
 */
public class ChangeFirstNameListener implements Button.ClickListener {
	private static final long serialVersionUID = -2262139898712942320L;

	private SettingsView settingsView;
	private UserService userService;

	public ChangeFirstNameListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Changes the last name of an user
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = settingsView.getUserInformationEmailRequest();
		String firstName = settingsView.getUserInformationFirstName();

		if (userService.changeFirstName(email, firstName)) {
			Notification.show("Erfolgreich", "Der Vorname wurde geändert",
					Type.HUMANIZED_MESSAGE);
		} else {
			Notification.show("Fehler",
					"Der Vorname konnte nicht geändert werden",
					Type.ERROR_MESSAGE);
		}
	}
}
