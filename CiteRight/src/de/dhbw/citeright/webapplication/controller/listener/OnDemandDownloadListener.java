package de.dhbw.citeright.webapplication.controller.listener;

import java.io.IOException;

import com.vaadin.server.FileDownloader;
import com.vaadin.server.StreamResource;
import com.vaadin.server.StreamResource.StreamSource;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinResponse;

/**
 * This specializes {@link FileDownloader} in a way, such that both the file
 * name and content can be determined on-demand, i.e. when the user has clicked
 * the component.
 * https://vaadin.com/wiki/-/wiki/Main/Letting+the+user+download+a+file
 */
public class OnDemandDownloadListener extends FileDownloader {
	private static final long serialVersionUID = 2843244586823616346L;

	/**
	 * Provide both the {@link StreamSource} and the filename in an on-demand
	 * way.
	 */
	public interface OnDemandStreamResource extends StreamSource {
		String getFilename();
	}

	private final OnDemandStreamResource onDemandStreamResource;

	/**
	 * Instantiates the class with a given Resource.
	 * 
	 * @param onDemandStreamResource
	 */
	public OnDemandDownloadListener(
			OnDemandStreamResource onDemandStreamResource) {

		super(new StreamResource(onDemandStreamResource, ""));

		this.onDemandStreamResource = onDemandStreamResource;

		if (onDemandStreamResource == null) {
			System.out
					.println("The given on-demand stream resource may never be null!");
			return;
		}
	}

	/**
	 * A wrapper for the handleConnectorRequest in {@link FileDownloader} which
	 * will determine the correct resource on call and pass it to the
	 * super-method.
	 */
	@Override
	public boolean handleConnectorRequest(VaadinRequest request,
			VaadinResponse response, String path) throws IOException {
		this.setOverrideContentType(false);
		getResource().setFilename(onDemandStreamResource.getFilename());
		return super.handleConnectorRequest(request, response, path);
	}

	/**
	 * Outputs the current value for the {@link StreamResource}
	 * 
	 * @return a stream resource.
	 */
	private StreamResource getResource() {
		return (StreamResource) this.getResource("dl");
	}

}