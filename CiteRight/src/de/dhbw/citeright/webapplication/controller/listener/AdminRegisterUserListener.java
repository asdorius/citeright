package de.dhbw.citeright.webapplication.controller.listener;

import java.util.regex.Pattern;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * Listener if a administrator want to register a new user
 * 
 * @author Kai Holzer
 *
 */
public class AdminRegisterUserListener implements Button.ClickListener {
	private static final long serialVersionUID = 2918580765960388688L;

	SettingsView settingsView;
	UserService userService;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public AdminRegisterUserListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		this.userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Proofs the entry and register a new user if everything is fine.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String firstName = settingsView.getNewUserFirstName();
		String lastName = settingsView.getNewUserLastName();
		String email = settingsView.getNewUserEmail();
		String password = settingsView.getNewUserPassword();
		String passwordRepeat = settingsView.getNewUserPasswordRepeat();

		boolean validEmail = VALID_EMAIL_ADDRESS_REGEX.matcher(email).matches();

		if (firstName.equals("")) {
			Notification.show("Fehler!", "Bitte Vorname eingeben",
					Type.ERROR_MESSAGE);
		} else if (lastName.equals("")) {
			Notification.show("Fehler!", "Bitte Nachname eingeben",
					Type.ERROR_MESSAGE);
		} else if (email.equals("") || !validEmail) {
			Notification.show("Fehler!", "Bitte korrekte Email eingeben",
					Type.ERROR_MESSAGE);
		} else if (password.equals("") || !passwordRepeat.equals(password)) {
			Notification.show("Fehler!", "Passwörter überprüfen",
					Type.ERROR_MESSAGE);
		} else {
			User user = new User(firstName, lastName, email, password, 3);
			int id = userService.registrate(user);
			if (id != -1) {
				if (userService.acceptUserRequest(id)) {
					Notification.show("Erfolgreich",
							"Der Benutzer wurde angelegt",
							Type.HUMANIZED_MESSAGE);
				} else {
					Notification.show("Fehler!",
							"Der Benutzer konnte nicht freigeschaltet werden",
							Type.ERROR_MESSAGE);
				}
			} else {
				Notification.show("Fehler!",
						"Der Benutzer konnte nicht angelegt werden",
						Type.ERROR_MESSAGE);
			}

		}

	}

}
