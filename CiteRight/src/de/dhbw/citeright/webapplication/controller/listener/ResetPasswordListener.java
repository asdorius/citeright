package de.dhbw.citeright.webapplication.controller.listener;

import org.vaadin.dialogs.ConfirmDialog;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.essentials.mail.MailingSingleton;
import de.dhbw.citeright.webapplication.view.pages.StartView;

/**
 * This listener is called, when a user want to reset his password
 * 
 * @author Kai Holzer
 */
public class ResetPasswordListener implements Button.ClickListener {
	private static final long serialVersionUID = -3498085229299416419L;
	private StartView startView;
	private UserService userService;

	public ResetPasswordListener(StartView startView) {
		this.startView = startView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * This method gets called, if a user click on the button to reset his
	 * password.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = startView.getEmail();
		if (!email.equals("")) {
			ConfirmDialog
					.show(UI.getCurrent(),
							"Sind Sie sicher, dass Sie das Passwort zurücksetzen möchten?",
							new ConfirmDialog.Listener() {
								private static final long serialVersionUID = 8447034081784521488L;

								public void onClose(ConfirmDialog dialog) {
									if (dialog.isConfirmed()) {
										if (userService.resetPassword(email)) {
											User user = userService
													.getUserByEmail(email);
											MailingSingleton mailing = MailingSingleton
													.getInstance();
											mailing.sendPasswordToUser(user);
											Notification
													.show("Erfolgreich",
															"Ein neues Passwort wurde Ihnen per Email zugesendet.",
															Type.HUMANIZED_MESSAGE);
										} else {
											Notification
													.show("Fehler!",
															"Das Passwort konnte nicht zurückgesetzt werden! \n Überprüfen Sie ob die Email korrekt eingegeben wurde.",
															Type.ERROR_MESSAGE);
										}
									}
								}
							});
		} else {
			Notification.show("Fehler!",
					"Geben Sie im entsprechenden Feld die Email-Adresse an.",
					Type.ERROR_MESSAGE);
		}

	}

}
