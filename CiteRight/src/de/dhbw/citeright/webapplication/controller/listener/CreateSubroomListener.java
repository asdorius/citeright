package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.webapplication.controller.TokenGenerator;
import de.dhbw.citeright.webapplication.view.pages.CreateSubroomView;

/**
 * This listener is called when the user adds a new room share to a given room.
 * 
 * @author Jan (i13015)
 *
 */
public class CreateSubroomListener implements Button.ClickListener {
	private static final long serialVersionUID = -1532880809106317095L;

	private CreateSubroomView createSubroomView;
	private RoomService roomService;

	/**
	 * Creates a new listener to add a room share.
	 * 
	 * @param createSubroomView
	 *            reference to the view on that the event will be triggered.
	 */
	public CreateSubroomListener(CreateSubroomView createSubroomView) {
		this.createSubroomView = createSubroomView;
		roomService = new RoomService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String subroomName = createSubroomView.getSubroomName();
		int parentRoomId = createSubroomView.getParentRoomId();
		int roomOwnerId = createSubroomView.getCurrentUserId();

		if (subroomName.equals("")) {
			Notification.show("Fehler!",
					"Bitte geben Sie einen Namen für den Raum ein.",
					Type.ERROR_MESSAGE);
		} else {
			// Create room with given information
			Room newRoom = new Room();

			newRoom.setRoomName(subroomName);
			newRoom.setOwnerId(roomOwnerId);
			newRoom.setParentId(parentRoomId);
			newRoom.setToken(TokenGenerator.generateUnique());

			int newRoomId;
			try {
				newRoomId = roomService.create(newRoom);
			} catch (Exception e) {
				// Entry couldn't be saved.
				Notification
						.show("Fehler!",
								"Der Raum konnte leider nicht angelegt werden, da ein Fehler aufgetreten ist.",
								Type.ERROR_MESSAGE);
				return;
			}

			// Room creation was successful. Change to settings page of this
			// room.
			createSubroomView.gotoToChangeRoomSettingsView(newRoomId);
		}
	}
}
