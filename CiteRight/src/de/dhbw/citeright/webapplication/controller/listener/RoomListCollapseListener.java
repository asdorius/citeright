package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Tree;
import com.vaadin.ui.TreeTable;

import de.dhbw.citeright.webapplication.view.pages.RoomListView;

/**
 * This listener is called when the user collapses a branch in a tree table.
 * 
 * @author Jan (i13015)
 *
 */
public class RoomListCollapseListener implements Tree.CollapseListener {
	private static final long serialVersionUID = 1898715512554465694L;
	private RoomListView roomSettingsView;

	/**
	 * Creates a new listener on {@link TreeTable} expanding.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on which the event will be triggered.
	 */
	public RoomListCollapseListener(RoomListView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
	}

	/**
	 * Will be executed when a branch is collapsed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void nodeCollapse(Tree.CollapseEvent event) {

		// Update table height
		roomSettingsView.updateRoomTableSize();
	}
}
