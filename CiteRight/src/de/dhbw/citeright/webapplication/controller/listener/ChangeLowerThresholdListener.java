package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * This listener gets called if a user change the lower threshold in the
 * settings
 * 
 * @author Kai Holzer
 *
 */
public class ChangeLowerThresholdListener implements Button.ClickListener {
	private static final long serialVersionUID = 166430495724181818L;

	private SettingsView settingsView;
	private UserService userService;

	public ChangeLowerThresholdListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * this method change the lower threshold to the selected value
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		if (settingsView.getNewLowerThreshold() <= settingsView
				.getUpperThreshold()) {
			boolean result = userService.changeLowerThreshold(
					settingsView.getCurrentEmail(),
					settingsView.getNewLowerThreshold());
			if (result) {
				Notification.show("Erfolgreich!",
						"Der untere Schwellwert wurde geändert",
						Type.HUMANIZED_MESSAGE);
				settingsView.setLowerThresholdAtSlider();
			} else {
				Notification.show("Fehler!",
						"Schwellwert wurde nicht geändert", Type.ERROR_MESSAGE);
			}
		} else {
			Notification
					.show("Fehler!",
							"Der untere Schwellwert darf nicht größer als der obere sein.",
							Type.ERROR_MESSAGE);
			settingsView.setLowerThresholdAtSlider();
		}
	}

}
