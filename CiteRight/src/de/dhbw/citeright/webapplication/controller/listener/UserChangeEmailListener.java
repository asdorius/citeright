package de.dhbw.citeright.webapplication.controller.listener;

import java.util.regex.Pattern;

import com.vaadin.ui.Button;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * This listener is called, when a user want to change his email.
 * 
 * @author Kai Holzer
 *
 */
public class UserChangeEmailListener implements Button.ClickListener {
	private static final long serialVersionUID = -8295245593943946777L;
	private SettingsView settingsView;
	private UserService userService;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public UserChangeEmailListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * This method gets called if the user click the button to change his email.
	 * The method validate the entry and changes the password.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		boolean validEmail = VALID_EMAIL_ADDRESS_REGEX.matcher(
				settingsView.getNewEmail()).matches();
		if (validEmail
				&& settingsView.getNewEmailRepeat().equals(
						settingsView.getNewEmail())
				&& settingsView.getCurrentEmail() != null) {

			if (userService.changeEmail(settingsView.getCurrentEmail(),
					settingsView.getNewEmail())) {
				Notification.show("Erfolgreich!", "Email wurde geändert",
						Type.HUMANIZED_MESSAGE);
				UI.getCurrent().getSession()
						.setAttribute("email", settingsView.getNewEmail());
				settingsView.resetEmailPanel();
				settingsView.setCurrentEmailLabel();
			} else {
				Notification.show("Fehler!", "Email ändern fehlgeschlagen",
						Type.ERROR_MESSAGE);
			}

		} else {
			Notification
					.show("Fehler!",
							"Email ändern fehlgeschlagen. Überprüfen Sie Ihre Eingabe.",
							Type.ERROR_MESSAGE);
		}

	}

}
