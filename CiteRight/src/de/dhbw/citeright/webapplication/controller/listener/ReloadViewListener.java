package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.citeright.webapplication.view.pages.DashboardView;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;
import de.dhbw.citeright.webapplication.view.pages.RoomListView;
/**
 * Reloads the current view 
 * @author Kai Holzer
 *
 */
public class ReloadViewListener implements Button.ClickListener {
	private static final long serialVersionUID = -5115101950275776316L;
	
	String type;
	
	public ReloadViewListener(String type) {
		this.type = type;
	}

	/**
	 * Loads the method from the current view to reload the data
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		if (type.equals(DashboardView.NAME)) {
			DashboardView dashboardView = (DashboardView) event.getButton().getData();
			dashboardView.updateView();
			
		} else if (type.equals(RoomListView.NAME)){
			RoomListView roomListView = (RoomListView) event.getButton().getData();
			roomListView.updateView();
		} else if (type.equals(DocumentsInRoomView.NAME)) {
			DocumentsInRoomView documentsInRoomView = (DocumentsInRoomView) event.getButton().getData();
			documentsInRoomView.updateView();
		}
		
	}

}
