package de.dhbw.citeright.webapplication.controller.listener;

import java.util.regex.Pattern;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.RegistrationView;

/**
 * This listener is called when a lecturer tries to register himself with the
 * system.
 * 
 * @author Kai Holzer
 *
 */
public class RegistrationPerformedListener implements Button.ClickListener {
	private static final long serialVersionUID = 6523482882582308247L;
	private RegistrationView registrationView;
	private UserService userService;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	/**
	 * Initializes the listener for a given View.
	 * 
	 * @param studentUploadView
	 *            The view where the listener is used.
	 */
	public RegistrationPerformedListener(RegistrationView registrationView) {
		this.registrationView = registrationView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * This method will be executed, when a button is clicked. It will check if
	 * the given data is correct.
	 * 
	 * @param event
	 *            An Object holding details to the event.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		String firstName = registrationView.getFirstName();
		String lastName = registrationView.getLastName();
		String email = registrationView.getEmail();
		String password = registrationView.getPassword();
		String passwordRepeat = registrationView.getPasswordRepeat();

		boolean validEmail = VALID_EMAIL_ADDRESS_REGEX.matcher(email).matches();

		if (firstName.equals("")) {
			Notification.show("Fehler!", "Bitte Vorname eingeben",
					Type.ERROR_MESSAGE);
		} else if (lastName.equals("")) {
			Notification.show("Fehler!", "Bitte Nachname eingeben",
					Type.ERROR_MESSAGE);
		} else if (email.equals("") || !validEmail) {
			Notification.show("Fehler!", "Bitte korrekte Email eingeben",
					Type.ERROR_MESSAGE);
		} else if (password.equals("") || !passwordRepeat.equals(password)) {
			Notification.show("Fehler!", "Passwörter überprüfen",
					Type.ERROR_MESSAGE);
		} else {
			User user = new User(firstName, lastName, email, password, 3);
			int id = userService.registrate(user);
			if (id != -1) {
				Notification.show("Erfolgreich", "Registrierung erfolgreich",
						Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("Fehler!",
						"Der Benutzer konnte nicht angelegt werden",
						Type.ERROR_MESSAGE);
			}

		}

	}

}
