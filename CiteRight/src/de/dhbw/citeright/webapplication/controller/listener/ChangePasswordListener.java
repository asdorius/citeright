package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * This listener is called, when a user want to change his password.
 * 
 * @author Kai Holzer
 *
 */
public class ChangePasswordListener implements Button.ClickListener {
	private static final long serialVersionUID = 4140545252919284447L;
	private SettingsView settingsView;
	private UserService userService;

	public ChangePasswordListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * This method gets called if the user click the button to change his
	 * password. The method validate the entry and changes the password.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		if (!settingsView.getOldPassword().equals("")
				&& !settingsView.getNewPassword().equals("")
				&& settingsView.getNewPasswordRepeat().equals(
						settingsView.getNewPassword())) {

			if (userService.changePassword(settingsView.getCurrentEmail(),
					settingsView.getOldPassword(),
					settingsView.getNewPassword())) {
				Notification.show("Erfolgreich!", "Passwort wurde geändert",
						Type.HUMANIZED_MESSAGE);
				settingsView.resetPasswordPanel();
			} else {
				Notification.show("Fehler!", "Passwort ändern fehlgeschlagen",
						Type.ERROR_MESSAGE);
				settingsView.resetPasswordPanel();
			}
		} else {
			Notification.show("Fehler!", "Passwort ändern fehlgeschlagen.",
					Type.ERROR_MESSAGE);
			settingsView.resetPasswordPanel();
		}
	}

}
