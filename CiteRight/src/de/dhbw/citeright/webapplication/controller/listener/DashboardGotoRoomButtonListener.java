package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.view.pages.DashboardView;

/**
 * This listener is called when the user clicks the button to go to the room.
 * 
 * @author Jan (i13015)
 *
 */
public class DashboardGotoRoomButtonListener implements Button.ClickListener {
	private static final long serialVersionUID = -5425289335241115486L;
	private DashboardView dashboardView;

	/**
	 * Creates a new listener to add a room share.
	 * 
	 * @param dashboardView
	 *            reference to the view on that the event will be triggered.
	 */
	public DashboardGotoRoomButtonListener(DashboardView dashboardView) {
		this.dashboardView = dashboardView;
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		AttributedButton button = (AttributedButton) event.getButton();

		Integer roomId = (Integer) button.getAttribute("roomId");

		if (roomId != null) {
			dashboardView.gotoToDocumentsInRoomView(roomId);
		}
	}

}
