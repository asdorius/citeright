package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;

/**
 * This listener is called when the user tries to restart the audit for a document.
 * 
 * @author Jan (i13015)
 *
 */
public class DocumentsInRoomRestartAuditButtonListener implements
		Button.ClickListener {
	private static final long serialVersionUID = -1563665818908479539L;
	private DocumentsInRoomView documentsInRoomView;

	private DocumentService documentService;

	/**
	 * Creates a new listener to restart the audit for a document.
	 * 
	 * @param documentsInRoomView
	 *            reference to the view on which the event will be triggered.
	 */
	public DocumentsInRoomRestartAuditButtonListener(
			DocumentsInRoomView documentsInRoomView) {
		this.documentsInRoomView = documentsInRoomView;

		documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		// Retrieve document ID of document to restart.
		AttributedButton sourceButton = (AttributedButton) event.getButton();
		Integer documentId = (Integer) sourceButton.getAttribute("documentId");

		if (documentId == null) {
			System.out.println("No document ID given.");
			return;
		}

		// Ensure document exists.
		Document currentDocument = documentService.getById(documentId);

		if (currentDocument == null) {
			Notification.show("Fehler", "Das Dokument mit der ID " + documentId
					+ " wurde nicht gefunden.", Type.ERROR_MESSAGE);
			return;
		}

		// Check if document status is in (AUDIT_FINISHED, AUDIT_FAILED).
		if (currentDocument.getStatusId() != CiteRightConstants.STATUS_AUDIT_FINISHED
				&& currentDocument.getStatusId() != CiteRightConstants.STATUS_AUDIT_FAILED) {
			Notification.show("Fehler", "Das Dokument mit der ID " + documentId
					+ " kann aktuell nicht neu gestartet werden.",
					Type.ERROR_MESSAGE);
			return;
		}

		// Trigger restart with compute server.
		WorkloadServerConnector.processDocument(documentId);

		// Change document status to IN_QUEUE in database
		currentDocument.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);
		documentService.update(currentDocument);

		// Change document status to IN_QUEUE in documents list
		documentsInRoomView.updateDocumentsTable();

		Notification.show("Erfolgreich",
				"Die Prüfung für das Dokument wurde neu gestartet.",
				Type.HUMANIZED_MESSAGE);
	}
}
