package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * This listener gets called if a user change the upper threshold in the
 * settings
 * 
 * @author Kai Holzer
 *
 */
public class ChangeUpperThresholdListener implements Button.ClickListener {
	private static final long serialVersionUID = -107844684548836497L;

	private SettingsView settingsView;
	private UserService userService;

	public ChangeUpperThresholdListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * this method change the upper threshold to the selected value
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		if (settingsView.getNewUpperThreshold() >= settingsView
				.getLowerThreshold()) {
			boolean result = userService.changeUpperThreshold(
					settingsView.getCurrentEmail(),
					settingsView.getNewUpperThreshold());
			if (result) {
				Notification.show("Erfolgreich!",
						"Der Schwellwert wurde geändert",
						Type.HUMANIZED_MESSAGE);
				settingsView.setUpperThresholdAtSlider();
			} else {
				Notification.show("Fehler!",
						"Schwellwert wurde nicht geändert", Type.ERROR_MESSAGE);
			}
		} else {
			Notification
					.show("Fehler!",
							"Der obere Schwellwert darf nicht kleiner als der untere sein.",
							Type.ERROR_MESSAGE);
			settingsView.setUpperThresholdAtSlider();
		}
	}

}
