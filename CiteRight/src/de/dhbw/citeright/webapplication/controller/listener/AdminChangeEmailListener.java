package de.dhbw.citeright.webapplication.controller.listener;

import java.util.regex.Pattern;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Notification;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;

/**
 * The listener if a admin want to change the email from a user;
 * 
 * @author Kai Holzer
 *
 */
public class AdminChangeEmailListener implements Button.ClickListener {
	private static final long serialVersionUID = -2262139898712942320L;

	private SettingsView settingsView;
	private UserService userService;
	public static final Pattern VALID_EMAIL_ADDRESS_REGEX = Pattern.compile(
			"^[A-Z0-9._%+-]+@[A-Z0-9.-]+\\.[A-Z]{2,6}$",
			Pattern.CASE_INSENSITIVE);

	public AdminChangeEmailListener(SettingsView settingsView) {
		this.settingsView = settingsView;
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Checks if its a valid email and changes the email of the user.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		String email = settingsView.getUserInformationEmail();
		boolean validEmail = VALID_EMAIL_ADDRESS_REGEX.matcher(email).matches();
		if (validEmail) {
			if (userService.changeEmail(
					settingsView.getUserInformationEmailRequest(), email)) {
				Notification.show("Erfolgreich", "Die Email wurde geändert",
						Type.HUMANIZED_MESSAGE);
			} else {
				Notification.show("Fehler",
						"Die Email konnte nicht geändert werden",
						Type.ERROR_MESSAGE);
			}
		} else {
			Notification.show("Fehler", "Die eingegebene Email ist ungültig",
					Type.ERROR_MESSAGE);
		}

	}

}
