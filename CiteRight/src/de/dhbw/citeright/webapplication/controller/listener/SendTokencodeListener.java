package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.citeright.webapplication.view.pages.StudentTokenView;

/**
 * This listener is called when a user sends a token.
 * 
 * @author Kai Holzer
 */
public class SendTokencodeListener implements Button.ClickListener {
	private static final long serialVersionUID = 577919187966756254L;
	StudentTokenView studentTokenView;

	/**
	 * Initializes the listener for a given View.
	 * 
	 * @param studentUploadView
	 *            The view where the listener is used.
	 */
	public SendTokencodeListener(StudentTokenView studentTokenView) {
		this.studentTokenView = studentTokenView;
	}

	/**
	 * This method will be executed, when a button is clicked. It will check the
	 * token and eventually redirect the student to the upload page.
	 * 
	 * @param event
	 *            An Object holding details to the event.
	 */
	@Override
	public void buttonClick(ClickEvent event) {
		studentTokenView.gotoUpload();
	}

}
