package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;

/**
 * This listener is called when the user tries to delete a room.
 * 
 * @author Jan (i13015)
 *
 */
public class DeleteRoomListener implements Button.ClickListener {
	private static final long serialVersionUID = -7192469575083469993L;

	private RoomSettingsView roomSettingsView;
	private RoomService roomService;

	/**
	 * Creates a new listener to delete a room.
	 * 
	 * @param roomSettingsView
	 *            reference to the view on which the event will be triggered.
	 */
	public DeleteRoomListener(RoomSettingsView roomSettingsView) {
		this.roomSettingsView = roomSettingsView;
		roomService = new RoomService(HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		// Try to delete the room. The documents and shares will be removed by
		// constraints.
		try {
			roomService.delete(roomSettingsView.thisRoom);
		} catch (Exception e) {
			Notification.show("Fehler!",
					"Der Raum konnte nicht gelöscht werden.",
					Type.ERROR_MESSAGE);
			return;
		}

		Notification.show("Erfolg", "Der Raum wurde gelöscht.",
				Type.HUMANIZED_MESSAGE);
		roomSettingsView.gotoToRoomListView();
	}
}
