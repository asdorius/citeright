package de.dhbw.citeright.webapplication.controller.listener;

import java.io.ByteArrayOutputStream;
import java.io.OutputStream;
import java.sql.Timestamp;
import java.util.Date;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.Upload.FailedEvent;
import com.vaadin.ui.Upload.FailedListener;
import com.vaadin.ui.Upload.Receiver;
import com.vaadin.ui.Upload.SucceededEvent;
import com.vaadin.ui.Upload.SucceededListener;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.controller.connection.WorkloadServerConnector;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;

/**
 * Receives a file from the LecturerUploadView and saves it it the database.
 * 
 * @author Kai Holzer
 *
 */
public class LecturerFileUploadReceiver implements SucceededListener,
		FailedListener, Receiver {
	private static final long serialVersionUID = 6142845679474717063L;
	private String filename;
	private ByteArrayOutputStream output;
	private String mimeType;
	private Timestamp uploadTime;
	private LecturerUploadView lecturerUploadView;

	public LecturerFileUploadReceiver(LecturerUploadView lecturerUploadView) {
		this.lecturerUploadView = lecturerUploadView;
	}

	@Override
	public OutputStream receiveUpload(String filename, String mimeType) {
		this.filename = filename;
		this.mimeType = mimeType;
		this.uploadTime = new Timestamp(new Date().getTime());

		output = new ByteArrayOutputStream();

		return output;
	}

	@Override
	public void uploadFailed(FailedEvent event) {
		Notification.show("Fehler!", "Upload fehlgeschlagen",
				Type.ERROR_MESSAGE);
	}

	@Override
	public void uploadSucceeded(SucceededEvent event) {
		System.out.println("Upload fertig");
		if (filename.length() == 0) {
			Notification
					.show("Fehler!", "Schreiben in Datenbank fehlgeschlagen",
							Type.ERROR_MESSAGE);
			return;
		}
		if (lecturerUploadView.getAuthor().equals("")) {
			Notification.show("Fehler!", "Bitte geben Sie einen Autor an",
					Type.ERROR_MESSAGE);
			return;
		}
		if (!supportedMimeType()) {
			Notification.show("Fehler",
					"Dieser Dateityp wird nicht unterstützt",
					Type.ERROR_MESSAGE);
			return;
		}

		// Limit the maximal size of a file to value of the constant
		if (((ByteArrayOutputStream) output).toByteArray().length > CiteRightConstants.MAX_FILESIZE) {
			Notification.show("Fehler", "Die Datei ist zu groß",
					Type.ERROR_MESSAGE);
			return;
		}

		Room room = lecturerUploadView.getSelectedRoom();

		if (room != null && room.getId() > 0) {
			Document document = new Document(filename, mimeType,
					((ByteArrayOutputStream) output).toByteArray(), uploadTime,
					room.getId());
			document.setStudentName(lecturerUploadView.getAuthor());
			document.setStudentMatriculationNumber("DOZENTENUPLOAD");
			document.setStatusId(CiteRightConstants.STATUS_WAITING_IN_QUEUE);

			DocumentService documentService = new DocumentService(
					HibernateUtils.getSessionFactory());

			if (documentService.create(document) > 0) {
				Notification.show("Erfolgreich",
						"Datei wurde in Raum " + room.getRoomName()
								+ " hochgeladen", Type.HUMANIZED_MESSAGE);
				// Trigger start at compute server.
				WorkloadServerConnector.processDocument(document.getId());

			} else {
				Notification.show("Fehler!",
						"Schreiben in Datenbank fehlgeschlagen",
						Type.ERROR_MESSAGE);
			}
		} else {
			Notification.show("Fehler!", "Es wurde kein Raum ausgewählt",
					Type.ERROR_MESSAGE);
		}
	}

	/**
	 * tests if the document is a supported file type
	 * 
	 * @return true if its a valid type, else false
	 */
	private boolean supportedMimeType() {
		if (mimeType.equalsIgnoreCase("application/pdf")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("text/html")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("text/plain")) {
			return true;
		} else if (mimeType
				.equalsIgnoreCase("application/vns.openxmlformats-officedocument.wordprocessingml.document")) {
			return true;
		} else if (mimeType.equalsIgnoreCase("application/msword")) {
			return true;
		} else if (mimeType
				.equalsIgnoreCase("application/vnd.oasis.opendocument.text")) {
			return true;
		} else {
			return false;
		}
	}

}
