package de.dhbw.citeright.webapplication.controller.listener;

import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;

/**
 * This listener is called when the user tries to remove a document.
 * 
 * @author Jan (i13015)
 *
 */
public class DocumentsInRoomRemoveDocumentButtonListener implements
		Button.ClickListener {
	private static final long serialVersionUID = 6125921654888825511L;

	private DocumentsInRoomView documentsInRoomView;

	private DocumentService documentService;

	/**
	 * Creates a new listener to remove a document.
	 * 
	 * @param documentsInRoomView
	 *            reference to the view on which the event will be triggered.
	 */
	public DocumentsInRoomRemoveDocumentButtonListener(
			DocumentsInRoomView documentsInRoomView) {
		this.documentsInRoomView = documentsInRoomView;

		documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Will be executed if the assigned button is pressed.
	 * 
	 * @param event
	 *            Event object with detailed information.
	 */
	@Override
	public void buttonClick(ClickEvent event) {

		// Retrieve document ID of document to remove.
		AttributedButton sourceButton = (AttributedButton) event.getButton();
		Integer documentId = (Integer) sourceButton.getAttribute("documentId");

		if (documentId == null) {
			System.out.println("No document ID given.");
			return;
		}

		// Ensure document exists.
		Document currentDocument = documentService.getById(documentId);

		if (currentDocument == null) {
			Notification.show("Fehler", "Das Dokument mit der ID " + documentId
					+ " wurde nicht gefunden.", Type.ERROR_MESSAGE);
			return;
		}

		// Remove document
		if (documentService.delete(currentDocument)) {
			Notification.show("Erfolgreich", "Das Dokument wurde gelöscht.",
					Type.HUMANIZED_MESSAGE);
			documentsInRoomView.updateDocumentsTable();
		} else {
			Notification.show("Fehler",
					"Das Dokument konnte nicht gelöscht werden.",
					Type.ERROR_MESSAGE);
		}
	}
}
