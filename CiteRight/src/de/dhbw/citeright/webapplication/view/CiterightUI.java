package de.dhbw.citeright.webapplication.view;

import javax.servlet.annotation.WebServlet;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.navigator.Navigator;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.UI;

import de.dhbw.citeright.webapplication.view.pages.CreateSubroomView;
import de.dhbw.citeright.webapplication.view.pages.DashboardView;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
import de.dhbw.citeright.webapplication.view.pages.RegistrationView;
import de.dhbw.citeright.webapplication.view.pages.RoomListView;
import de.dhbw.citeright.webapplication.view.pages.RoomSettingsView;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;
import de.dhbw.citeright.webapplication.view.pages.StartView;
import de.dhbw.citeright.webapplication.view.pages.StudentTokenView;
import de.dhbw.citeright.webapplication.view.pages.StudentUploadView;

/**
 * is the startpoint of the application where the Views gets added
 * 
 * @author Kai Holzer
 */
@Theme("citeright")
public class CiterightUI extends UI {
	private static final long serialVersionUID = 2202271154542437840L;

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = CiterightUI.class)
	public static class Servlet extends VaadinServlet {
		private static final long serialVersionUID = 4746749317287244237L;
	}

	/**
	 * Initializes the UI. Furthermore the navigation elements are set here.
	 * Every new or changed View has to be registered here.
	 */
	@Override
	protected void init(VaadinRequest request) {

		// Initialize user data stored in the session
		UI.getCurrent().getSession().setAttribute("email", "");
		UI.getCurrent().getSession().setAttribute("userId", -1);

		// Navigation
		Navigator navigator = new Navigator(this, this);

		/*
		 * Not logged in
		 */
		
		// Login, registration and forgot password
		navigator.addView(StartView.NAME, new StartView());
		navigator.addView(RegistrationView.NAME, new RegistrationView());

		// Student upload and token input
		navigator.addView(StudentUploadView.NAME, new StudentUploadView());
		navigator.addView(StudentTokenView.NAME, new StudentTokenView());

		/*
		 * Logged in
		 */

		// Dashboard and settings
		navigator.addView(DashboardView.NAME, new DashboardView());
		navigator.addView(SettingsView.NAME, new SettingsView());

		// Upload for lecturers
		navigator.addView(LecturerUploadView.NAME, new LecturerUploadView());

		// Rooms
		navigator.addView(RoomListView.NAME, new RoomListView());
		navigator.addView(CreateSubroomView.NAME, new CreateSubroomView());
		navigator.addView(RoomSettingsView.NAME, new RoomSettingsView());
		navigator.addView(DocumentsInRoomView.NAME, new DocumentsInRoomView());
	}

}