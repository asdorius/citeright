package de.dhbw.citeright.webapplication.view.pages;

import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TextArea;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.LecturerFileUploadReceiver;
import de.dhbw.citeright.webapplication.controller.listener.LecturerTextUploadListener;
import de.dhbw.citeright.webapplication.controller.listener.LecturerUrlUploadListener;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * In this view a lecturer can upload files, urls or text to a room
 * 
 * @author Kai Holzer
 *
 */
public class LecturerUploadView extends MemberAreaLayout {
	private static final long serialVersionUID = -573684749686911172L;

	public static final String NAME = "lecturerupload";

	public UserService userService;

	VerticalLayout contentComponent;

	private LecturerFileUploadReceiver uploadReceiver;
	private NativeSelect rooms;
	private TextField authorTextField;

	private Panel fileUploadPanel;
	private VerticalLayout fileUploadLayout;

	private Panel urlUploadPanel;
	private VerticalLayout urlUploadLayout;
	private TextField urlUploadTitle;
	private TextField urlUploadLink;

	private Panel textUploadPanel;
	private VerticalLayout textUploadLayout;
	private TextField textUploadTitle;
	private TextArea textUploadArea;

	public LecturerUploadView() {
		super();

		userService = new UserService();
	}

	/**
	 * initializes the view for the lecturer upload
	 */
	private void initializeLecturerUploadView() {
		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		rooms = new NativeSelect("Bitte zuerst einen Raum auswählen");

		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());

		// If the user has admin rights, he may see all rooms.
		boolean currentUserHasAdminRights = false;
		try {
			currentUserHasAdminRights = RightsControl.isAdmin(currentUserID);
		} catch (UserNotFoundException e) {
			// Nothing needs to be done here as we initialized the variable with
			// false.
		}

		List<Room> accessibleRooms;

		// Loads all Rooms if the user is an administrator else load just those
		// where the user got has the access to write into.
		if (currentUserHasAdminRights) {
			accessibleRooms = roomService.getAll();
		} else {
			accessibleRooms = roomService.getRoomsForUser(currentUserID, true);
		}

		// Load the rooms into the table
		for (Room currentRoom : accessibleRooms) {
			if (currentRoom.getId() != CiteRightConstants.ROOT_ROOM_ID) {
				rooms.addItem(currentRoom);
				rooms.setItemCaption(currentRoom, currentRoom.getRoomName());
			}
		}

		rooms.setNullSelectionAllowed(false);
		rooms.setMultiSelect(false);
		rooms.setWidth("300px");

		contentComponent.addComponent(rooms);

		// Sets a text field for the author
		authorTextField = new TextField(
				"Geben Sie hier den Autor des Dokuments an");
		authorTextField.setWidth("300px");
		contentComponent.addComponent(authorTextField);

		initializeLecturerFileUpload();
		initializeLecturerURLUpload();
		initializeLecutrerTextUpload();

	}

	/**
	 * initializes the components for the lecturer file upload
	 */
	private void initializeLecturerFileUpload() {
		fileUploadPanel = new Panel("Hier können Sie eine Datei hochladen");

		fileUploadLayout = new VerticalLayout();
		fileUploadLayout.setMargin(true);
		fileUploadLayout.setSpacing(true);

		uploadReceiver = new LecturerFileUploadReceiver(this);
		Upload upload = new Upload("Bitte Datei auswählen", uploadReceiver);
		upload.addSucceededListener(uploadReceiver);
		upload.addFailedListener(uploadReceiver);
		upload.setButtonCaption("Dokument uploaden");

		fileUploadLayout.addComponent(upload);
		fileUploadPanel.setContent(fileUploadLayout);
		contentComponent.addComponent(fileUploadPanel);

	}

	/**
	 * initializes the components for the lecturer url upload
	 */
	private void initializeLecturerURLUpload() {
		urlUploadPanel = new Panel("Hier können Sie eine URL hochladen");

		urlUploadLayout = new VerticalLayout();
		urlUploadLayout.setMargin(true);
		urlUploadLayout.setSpacing(true);

		urlUploadTitle = new TextField("Titel der URL angeben");
		urlUploadTitle.setWidth("400px");
		urlUploadLayout.addComponent(urlUploadTitle);

		urlUploadLink = new TextField("URL angeben");
		urlUploadLink.setWidth("400px");
		urlUploadLayout.addComponent(urlUploadLink);

		urlUploadLayout.addComponent(new Button("URL uploaden",
				new LecturerUrlUploadListener(this)));

		urlUploadPanel.setContent(urlUploadLayout);

		contentComponent.addComponent(urlUploadPanel);
	}

	/**
	 * initializes the components for the lecturer text upload
	 */
	private void initializeLecutrerTextUpload() {
		textUploadPanel = new Panel("Hier können Sie einen Text hochladen");

		textUploadLayout = new VerticalLayout();
		textUploadLayout.setMargin(true);
		textUploadLayout.setSpacing(true);

		textUploadTitle = new TextField("Titel des Textes angeben");
		textUploadTitle.setWidth("400px");
		textUploadLayout.addComponent(textUploadTitle);

		textUploadArea = new TextArea("Hier den Text angeben");
		textUploadArea.setSizeFull();
		textUploadLayout.addComponent(textUploadArea);

		textUploadLayout.addComponent(new Button("Text uploaden",
				new LecturerTextUploadListener(this)));

		textUploadPanel.setContent(textUploadLayout);

		contentComponent.addComponent(textUploadPanel);

	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Returns the url of the upload.
	 * 
	 * @return the value of the urlUploadLink text field.
	 */
	public String getUrlUploadLink() {
		return urlUploadLink.getValue();
	}

	/**
	 * Returns the title of an url upload.
	 * 
	 * @return the value of the urlUploadTitle text field.
	 */
	public String getUrlUploadTitle() {
		return urlUploadTitle.getValue();
	}

	/**
	 * Returns the title of a text upload
	 * 
	 * @return the value of the textUploadTitle
	 */
	public String getTextUploadTitle() {
		return textUploadTitle.getValue();
	}

	/**
	 * Returns the content of a text upload
	 * 
	 * @return the value of the textUploadArea
	 */
	public String getTextUploadContent() {
		return textUploadArea.getValue();
	}

	/**
	 * Returns the author from the text field
	 * 
	 * @return the value of the authorTextField
	 */
	public String getAuthor() {
		return authorTextField.getValue();
	}

	/**
	 * Returns the current marked room
	 * 
	 * @return the name of the marked room
	 */
	public Room getSelectedRoom() {
		return (Room) rooms.getValue();
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the tokenCode field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		initializeLecturerUploadView();
	}
}
