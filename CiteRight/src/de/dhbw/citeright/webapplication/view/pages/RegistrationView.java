package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.listener.RegistrationPerformedListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * The registration view
 * 
 * @author Kai Holzer
 */
public class RegistrationView extends PreLoginLayout {
	private static final long serialVersionUID = 5861684626401429939L;

	public static final String NAME = "registration";
	
	public UserService userService;

	VerticalLayout contentComponent;

	private TextField firstName;
	private TextField lastName;
	private TextField email;
	private PasswordField password;
	private PasswordField passwordRepeat;
	private Button sendRegistration;

	/**
	 * Initializes the registration view.
	 */
	public RegistrationView() {
		super();
		
		userService = new UserService();
	}

	/**
	 * Initializes the registration form and it's elements.
	 */
	private void initializeRegistrationForm() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		content.addComponent(contentComponent);

		FormLayout registrationForm = new FormLayout();
		registrationForm.setCaption("Geben Sie hier Ihre Daten ein");

		// Create the textfields and button and set their labels.
		firstName = new TextField("Vorname:");
		firstName.setRequired(true);

		lastName = new TextField("Nachname:");
		lastName.setRequired(true);

		email = new TextField("Email:");
		email.setRequired(true);

		password = new PasswordField("Passwort");
		password.setRequired(true);

		passwordRepeat = new PasswordField("Passwort wiederholen:");
		passwordRepeat.setRequired(true);

		sendRegistration = new Button("registrieren");
		sendRegistration.addClickListener(new RegistrationPerformedListener(
				this));

		// Add form fields to the registrationForm
		registrationForm.addComponent(firstName);
		registrationForm.addComponent(lastName);
		registrationForm.addComponent(email);
		registrationForm.addComponent(password);
		registrationForm.addComponent(passwordRepeat);
		registrationForm.addComponent(sendRegistration);

		// Add registrationForm to the View.
		contentComponent.addComponent(registrationForm);
	}
	
	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Will return the current value of the firstName-field.
	 * 
	 * @return firstName as entered in the textfield.
	 */
	public String getFirstName() {
		return firstName.getValue();
	}

	/**
	 * Will return the current value of the lastName-field.
	 * 
	 * @return lastName as entered in the textfield.
	 */
	public String getLastName() {
		return lastName.getValue();
	}

	/**
	 * Will return the current value of the email-field.
	 * 
	 * @return e-mail as entered in the textfield.
	 */
	public String getEmail() {
		return email.getValue();
	}

	/**
	 * Will return the current value of the password-field.
	 * 
	 * @return password as entered in the textfield.
	 */
	public String getPassword() {
		return password.getValue();
	}

	/**
	 * Will return the current value of the passwordRepeat-field.
	 * 
	 * @return repeated password as entered in the textfield.
	 */
	public String getPasswordRepeat() {
		return passwordRepeat.getValue();
	}

	/**
	 * Is called when a view was loaded. Will enter the view and set the focus
	 * on the input element of firstName.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() != -1) {
			return;
		}

		initializeRegistrationForm();

		firstName.focus();
	}
}
