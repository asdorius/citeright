package de.dhbw.citeright.webapplication.view.pages;

import java.util.List;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.listener.LoginPerformedListener;
import de.dhbw.citeright.webapplication.controller.listener.ResetPasswordListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * The start view where a user is redirected to if he isn't logged in.
 * 
 * @author Kai Holzer
 */
public class StartView extends PreLoginLayout implements View {

	private static final long serialVersionUID = 3589679579456491196L;

	public static final String NAME = "";

	VerticalLayout contentComponent;
	
	UserService userService;

	private TextField email;
	private PasswordField password;
	private Button loginButton;
	private Button resetPasswordButton;
	private Label resetPasswordLabel;

	/**
	 * Initializes the View and the login form.
	 */
	public StartView() {
		super();
		
		userService = new UserService();
	}

	/**
	 * Initialize the StartView.
	 */
	private void initializeStartView() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		initializeLogin();
	}

	/**
	 * Sets the textfields for the user login.
	 */
	private void initializeLogin() {

		email = new TextField("Email-Adresse:");
		email.setWidth("300px");
		email.setRequired(true);
		email.setInputPrompt("max@mustermann.de");
		email.setInvalidAllowed(false);

		password = new PasswordField("Passwort:");
		password.setWidth("300px");
		password.setRequired(true);
		password.setNullRepresentation("");

		loginButton = new Button("Login");
		loginButton.addClickListener(new LoginPerformedListener(this));
		loginButton.setClickShortcut(KeyCode.ENTER);

		resetPasswordLabel = new Label(
				"Zum Passwort zurücksetzen, geben Sie bitte die E-Mail-Adresse ein, von der das Passwort zurückgesetzt werden soll und bestätigen den Button unterhalb dieses Textes.");
		resetPasswordLabel.setWidth("300px");
		resetPasswordButton = new Button("Passwort zurücksetzen",
				new ResetPasswordListener(this));

		contentComponent.addComponent(email);
		contentComponent.addComponent(password);
		contentComponent.addComponent(loginButton);
		contentComponent.addComponent(resetPasswordLabel);
		contentComponent.addComponent(resetPasswordButton);

	}

	/**
	 * Returns the current value of the username TextField.
	 * 
	 * @return the text in the username textfield.
	 */
	public String getEmail() {
		return email.getValue();
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}
	
	/**
	 * Returns the current value of the password TextField.
	 * 
	 * @return the password in the password textfield
	 */
	public String getPassword() {
		return password.getValue();
	}

	/**
	 * Is called when the user was logged in successfully. Resets the login form
	 * and redirects the user to the dashboard.
	 */
	public void successfulLogin() {

		UserService service = new UserService(
				HibernateUtils.getSessionFactory());
		Integer userId;
		try {
			userId = service.getUserByEmail(email.getValue()).getId();
		} catch (Exception e) {
			userId = -1;
		}
		UI.getCurrent().getSession().setAttribute("email", email.getValue());
		UI.getCurrent().getSession().setAttribute("userId", userId);

		// Reset the values of the input fields for potential usage later on.
		email.setValue("");
		password.setValue("");

		getUI().getNavigator().navigateTo("dashboard");
	}

	/**
	 * Clears the password textfield and sets the focus on it.
	 */
	public void clearPasswordField() {
		password.setValue("");
		password.focus();
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the username field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() != -1) {
			return;
		}
		
		initializeStartView();
		
		email.focus();
	}
}
