package de.dhbw.citeright.webapplication.view.pages;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.NativeSelect;
import com.vaadin.ui.Panel;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Reference;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.AccessService;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.ReferenceService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.AddForeignSourceListener;
import de.dhbw.citeright.webapplication.controller.listener.AddRoomShareListener;
import de.dhbw.citeright.webapplication.controller.listener.AddRoomToSearchListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangeRoomNameListener;
import de.dhbw.citeright.webapplication.controller.listener.DeleteRoomListener;
import de.dhbw.citeright.webapplication.controller.listener.RemoveForeignSource;
import de.dhbw.citeright.webapplication.controller.listener.RemoveRoomFromSearch;
import de.dhbw.citeright.webapplication.controller.listener.RevokeRoomShareListener;
import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * This is the View where the User can change settings for a room.
 * 
 * @author Jan (i13015)
 *
 */
public class RoomSettingsView extends MemberAreaLayout {
	private static final long serialVersionUID = 7830579852269428139L;
	public static final String NAME = "roomSettings";

	private int roomId = -1;

	VerticalLayout contentComponent;

	private UserService userService;
	private RoomService roomService;
	private AccessService accessService;
	private DocumentService documentService;
	private ReferenceService referenceService;

	public Room thisRoom;

	private TabSheet settingsTabSheet;
	private VerticalLayout generateRoomTokenLayout;
	private VerticalLayout changeRoomNameLayout;
	private VerticalLayout shareRoomLayout;
	private VerticalLayout deleteRoomLayout;
	private VerticalLayout sharedUsersLayout;
	private VerticalLayout roomSearchSettingsLayout;

	private TextField roomName;
	private TextField newShareUser;

	private Table roomToSearchTable;
	private Table websiteToSearchTable;
	private TextField websiteToSearchTF;

	private NativeSelect availableRooms;

	/**
	 * Initializes the View and it's components.
	 */
	public RoomSettingsView() {
		userService = new UserService(HibernateUtils.getSessionFactory());
		roomService = new RoomService(HibernateUtils.getSessionFactory());
		accessService = new AccessService(HibernateUtils.getSessionFactory());
		documentService = new DocumentService();
		referenceService = new ReferenceService();
	}

	/**
	 * Initializes the View and fills the components.
	 */
	private void initializeRoomSettingsView() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		content.addComponent(contentComponent);

		// Ensure that the room exists.
		thisRoom = roomService.getById(roomId);

		String roomName;

		try {
			roomName = thisRoom.getRoomName();
		} catch (NullPointerException e) {
			// Room doesn't exist
			contentComponent.addComponent(new Label(
					"Auf den Raum konnte nicht zugegriffen werden."));
			return;
		}

		// Ensure that the user is allowed to change the settings of this room.
		// Check if the user owns the room or has administrator rights.
		boolean isUserAdmin = false;
		boolean isUserRoomOwner = false;

		try {
			isUserAdmin = RightsControl.isAdmin(getCurrentUserId());
		} catch (UserNotFoundException e) {
			// User not found, nothing to do here.
		}

		if (thisRoom.getOwnerId().intValue() == getCurrentUserId()) {
			isUserRoomOwner = true;
		}

		// Check if one was true
		if (!isUserAdmin && !isUserRoomOwner) {
			contentComponent
					.addComponent(new Label(
							"Sie haben keine Berechtigung die Einstellungen des Raums zu aktualisieren."));
			return;
		}

		// Create information text for this View
		Label informationText = new Label(
				"Sie bearbeiten die Einstellungen des Raums '" + roomName + "'");
		contentComponent.addComponent(informationText);

		// Create the tab sheet and initialize the sheet views.
		settingsTabSheet = new TabSheet();

		initializeGenerateRoomTokenLayout();
		initializeChangeRoomNameLayout();
		initializeShareRoomLayout();
		initializeDeleteRoomLayout();
		initializeRoomSearchSettingsLayout();

		contentComponent.addComponent(settingsTabSheet);
	}

	/**
	 * Will initialize the tab where the room token is displayed.
	 */
	private void initializeGenerateRoomTokenLayout() {

		// Generate the upload link
		String roomToken = thisRoom.getToken();
		String linkToStudentUpload = CiteRightConstants.ADDRESS_WEB_APP
				+ "/#!studentupload/" + roomToken;
		System.out.println(linkToStudentUpload);

		generateRoomTokenLayout = new VerticalLayout();
		generateRoomTokenLayout.setSpacing(true);
		settingsTabSheet.addTab(generateRoomTokenLayout, "Zugangscode");

		Label labelForLink = new Label(
				"Auf folgender Seite können Ihre Studenten Dateien in diesen Raum einreichen:");
		generateRoomTokenLayout.addComponent(labelForLink);

		TextField tokenLink = new TextField();
		tokenLink.setWidth(100, Sizeable.Unit.PERCENTAGE);
		tokenLink.setValue(linkToStudentUpload);
		tokenLink.setReadOnly(true);
		generateRoomTokenLayout.addComponent(tokenLink);
	}

	/**
	 * Will initialize the tab where the room name can be changed.
	 */
	private void initializeChangeRoomNameLayout() {

		changeRoomNameLayout = new VerticalLayout();
		changeRoomNameLayout.setSpacing(true);
		settingsTabSheet.addTab(changeRoomNameLayout, "Raumname");

		Label labelForNameField = new Label(
				"Hier können Sie den Raum umbenennen. Bitte geben Sie den neuen Namen ein.");
		changeRoomNameLayout.addComponent(labelForNameField);

		roomName = new TextField();
		roomName.setValue(thisRoom.getRoomName());
		changeRoomNameLayout.addComponent(roomName);

		Button submitChangeRoomName = new Button();
		submitChangeRoomName.setCaption("Speichern");
		submitChangeRoomName.addClickListener(new ChangeRoomNameListener(this));
		changeRoomNameLayout.addComponent(submitChangeRoomName);
	}

	/**
	 * Will initialize the tab where the room can be shared with other users.
	 */
	private void initializeShareRoomLayout() {

		shareRoomLayout = new VerticalLayout();
		shareRoomLayout.setSpacing(true);
		settingsTabSheet.addTab(shareRoomLayout, "Freigaben");

		Label labelForRoomshare = new Label(
				"Sie können den Raum für andere Benutzer freigeben. Der Raum steht ihnen dann zur Einsicht bereit und kann für Plagiatsprüfungen verwendet werden. Aktuell haben folgende Benutzer Zugriff:");
		shareRoomLayout.addComponent(labelForRoomshare);

		// Generate the table showing all already existing shares.
		sharedUsersLayout = new VerticalLayout();
		sharedUsersLayout.setSpacing(true);
		shareRoomLayout.addComponent(sharedUsersLayout);
		updateSharedUsers();

		// Add fields to share the room with a new user.
		Label labelForNewShareUser = new Label(
				"Einen weiteren Benutzer für den Raum freigeben:");
		shareRoomLayout.addComponent(labelForNewShareUser);

		newShareUser = new TextField();
		newShareUser.setInputPrompt("max.mustermann@example.com");
		shareRoomLayout.addComponent(newShareUser);

		Button submitNewShareUser = new Button("Benutzer freigeben");
		submitNewShareUser.addClickListener(new AddRoomShareListener(this));
		shareRoomLayout.addComponent(submitNewShareUser);
	}

	/**
	 * Will initialize the tab where the room can be deleted.
	 */
	private void initializeDeleteRoomLayout() {

		deleteRoomLayout = new VerticalLayout();
		deleteRoomLayout.setSpacing(true);
		settingsTabSheet.addTab(deleteRoomLayout, "Raum löschen");

		Label infotextDeleteRoom = new Label(
				"Hier können Sie den Raum löschen. ACHTUNG: Alle Dokumente in diesem Raum werden unwiederbringlich gelöscht, wenn Sie auf den Button Löschen klicken! Es erfolgt KEINE Sicherheitsabfrage.");
		deleteRoomLayout.addComponent(infotextDeleteRoom);

		Button deleteButton = new Button("Raum und alle Dokumente löschen");
		deleteButton.addClickListener(new DeleteRoomListener(this));
		deleteRoomLayout.addComponent(deleteButton);
	}

	/**
	 * Will initialize the tab where search settings for rooms can be changed.
	 */
	private void initializeRoomSearchSettingsLayout() {

		roomSearchSettingsLayout = new VerticalLayout();
		roomSearchSettingsLayout.setSpacing(true);
		settingsTabSheet.addTab(roomSearchSettingsLayout, "Sucheinstellungen");

		Label infotextRoomSearchSettings = new Label(
				"In diesem Tab können Sie die Sucheinstellungen eines Raumes ändern.");
		roomSearchSettingsLayout.addComponent(infotextRoomSearchSettings);

		initializeRoomToSearchPanel();
		initializeWebsiteToSearchPanel();

	}

	/**
	 * Initializes the panel where a user can add a room to the search
	 */
	private void initializeRoomToSearchPanel() {
		Panel roomToSearchPanel = new Panel(
				"Hier können Sie einen Raum in die Suche einbinden");

		VerticalLayout roomToSearchLayout = new VerticalLayout();
		roomToSearchLayout.setSpacing(true);
		roomToSearchLayout.setMargin(true);

		roomToSearchTable = new Table();
		roomToSearchTable.setSizeFull();
		roomToSearchTable.setHeight("200px");

		roomToSearchTable.addContainerProperty("ID", Integer.class, null);
		roomToSearchTable.addContainerProperty("Raumname", String.class, null);
		roomToSearchTable.addContainerProperty("Entfernen", Button.class, null);

		// Loads the currently included rooms
		loadIncludedRooms();

		roomToSearchLayout.addComponent(roomToSearchTable);

		availableRooms = new NativeSelect("Bitte einen Raum auswählen");

		// Load the rooms that the currently logged in user is allowed to write
		// into.
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());
		for (Room currentRoom : roomService.getRoomsForUser(currentUserID,
				false)) {

			// Don't add the current room and the root room to the native
			// select.
			if (currentRoom.getId() != roomId
					&& currentRoom.getId() != CiteRightConstants.ROOT_ROOM_ID) {
				availableRooms.addItem(currentRoom);
				availableRooms.setItemCaption(currentRoom,
						currentRoom.getRoomName());
			}
		}

		availableRooms.setNullSelectionAllowed(false);
		availableRooms.setMultiSelect(false);
		availableRooms.setWidth("300px");

		roomToSearchLayout.addComponent(availableRooms);

		Button roomToSearchButton = new Button("Raum hinzufügen",
				new AddRoomToSearchListener(this));
		roomToSearchLayout.addComponent(roomToSearchButton);

		roomToSearchPanel.setContent(roomToSearchLayout);
		roomSearchSettingsLayout.addComponent(roomToSearchPanel);

	}

	public void loadIncludedRooms() {
		roomToSearchTable.clear();

		List<Room> rooms = roomService.getIncludedRooms(thisRoom);

		for (Room room : rooms) {
			Integer includedRoomId = room.getId();
			String roomName = room.getRoomName();

			Button deleteIncludedRoom = new Button("Entfernen",
					new RemoveRoomFromSearch(this));
			deleteIncludedRoom.setData(includedRoomId);

			roomToSearchTable.addItem(new Object[] { includedRoomId, roomName,
					deleteIncludedRoom }, includedRoomId);

		}
	}

	/**
	 * Initializes the panel where the user can add a website to the search.
	 */
	private void initializeWebsiteToSearchPanel() {
		Panel websiteToSearchPanel = new Panel(
				"Hier können Sie eine Webseite in die Suche einbinden");

		VerticalLayout websiteToSearchLayout = new VerticalLayout();
		websiteToSearchLayout.setSpacing(true);
		websiteToSearchLayout.setMargin(true);

		websiteToSearchTable = new Table();
		websiteToSearchTable.setSizeFull();
		websiteToSearchTable.setHeight("200px");

		websiteToSearchTable.addContainerProperty("URL", String.class, null);
		websiteToSearchTable.addContainerProperty("Stand", String.class, null);
		websiteToSearchTable.addContainerProperty("Entfernen", Button.class,
				null);

		websiteToSearchLayout.addComponent(websiteToSearchTable);

		websiteToSearchTF = new TextField(
				"URL angeben die zur Suche hinzugefügt werden soll");
		websiteToSearchTF.setWidth("400px");
		websiteToSearchLayout.addComponent(websiteToSearchTF);

		websiteToSearchLayout.addComponent(new Button("Webseite hinzufügen",
				new AddForeignSourceListener(this)));

		websiteToSearchPanel.setContent(websiteToSearchLayout);
		roomSearchSettingsLayout.addComponent(websiteToSearchPanel);

		loadForeignSources();
	}

	/**
	 * loads all foreign sources which are added to this room
	 */
	public void loadForeignSources() {

		websiteToSearchTable.clear();

		List<Document> documents = documentService.getDocumentsInRoom(roomId);

		for (Document document : documents) {

			if (document.isForeignSource()) {
				int documentId = document.getId();

				Timestamp timestamp = document.getTimestamp();

				Button deleteForeignSource = new Button("Entfernen",
						new RemoveForeignSource(this));
				deleteForeignSource.setData(document);

				int referenceId = document.getReferenceId();
				Reference reference = referenceService.getById(referenceId);

				String urlString = reference.getAdress();

				websiteToSearchTable
						.addItem(
								new Object[] {
										urlString,
										new SimpleDateFormat(
												CiteRightConstants.SIMPLEDATEFORMAT_DATETIME)
												.format(timestamp),
										deleteForeignSource }, documentId);
			}

		}
	}

	/**
	 * Updates the list of shared users by (re)drawing the underlying layout.
	 */
	public void updateSharedUsers() {
		sharedUsersLayout.removeAllComponents();

		for (User currentUser : getAllRoomShareUsers()) {
			sharedUsersLayout.addComponent(getSharedUserLayouted(currentUser));
		}
	}

	/**
	 * Returns a list containing the User objects of all users that this room is
	 * currently shared with.
	 * 
	 * @return the users list.
	 */
	public List<User> getAllRoomShareUsers() {
		List<User> returnList = new LinkedList<User>();

		List<Access> availableRoomShares = accessService
				.getRoomSharesForRoom(this.roomId);

		if (availableRoomShares != null) {
			for (Access currentRoomShare : availableRoomShares) {
				User foundUser = userService.getById(currentRoomShare
						.getUserId());

				if (foundUser != null) {
					returnList.add(foundUser);
				}
			}
		}

		return returnList;
	}

	/**
	 * Generates a HorizontalLayout where the username is displayed and a button
	 * to revoke the sharement.
	 * 
	 * @param userObject
	 *            the user to display
	 * @return a HorizontalLayout for use at the share room tab.
	 */
	public HorizontalLayout getSharedUserLayouted(User userObject) {

		HorizontalLayout returnLayout = new HorizontalLayout();

		String userNameToDisplay = userObject.getLastName() + ", "
				+ userObject.getFirstName();
		Label userNameLabel = new Label(userNameToDisplay);
		returnLayout.addComponent(userNameLabel);

		AttributedButton revokeButton = new AttributedButton(
				"Freigabe widerrufen");
		revokeButton.addClickListener(new RevokeRoomShareListener(this));
		revokeButton.setAttribute("sharedUserId", userObject.getId());
		returnLayout.addComponent(revokeButton);

		return returnLayout;
	}

	/**
	 * the website who should get included to the search
	 * 
	 * @return the value of the websiteToSearchTF
	 */
	public String getWebsiteToSearch() {
		return websiteToSearchTF.getValue();
	}

	/**
	 * Sets the content of the text field websiteToSearchTF
	 * 
	 * @param newValue
	 *            the new Value in the text field
	 */
	public void setWebsiteToSearch(String newValue) {
		websiteToSearchTF.setValue(newValue);
	}

	/**
	 * Deletes a foreign search in the table
	 * 
	 * @param documentId
	 *            the documentId from the foreign source
	 */
	public void deleteForeignSearch(int documentId) {
		websiteToSearchTable.removeItem(documentId);
	}

	/**
	 * Deletes an included room from the table
	 * 
	 * @param includedRoomId
	 *            the room which should get deleted
	 */
	public void deleteIncludedRoom(int includedRoomId) {
		roomToSearchTable.removeItem(includedRoomId);

	}

	/**
	 * Returns the current marked room
	 * 
	 * @return the name of the marked room
	 */
	public Room getSelectedRoom() {
		return (Room) availableRooms.getValue();
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Returns the current room
	 * 
	 * @return the room object of the current room.
	 */
	public Room getThisRoom() {
		return thisRoom;
	}

	/**
	 * Returns the room ID that has been stored for this View.
	 * 
	 * @return the room ID.
	 */
	public int getRoomId() {
		return roomId;
	}

	/**
	 * Set the room ID of the room whose settings are to be changed.
	 * 
	 * @param roomId
	 *            the room ID.
	 */
	public void setRoomId(int roomId) {
		this.roomId = roomId;
	}

	/**
	 * Returns the value of the room name input field.
	 * 
	 * @return the inserted room name.
	 */
	public String getRoomName() {
		return roomName.getValue();
	}

	/**
	 * Sets the roomName field's value to the current Rooms name.
	 */
	public void resetRoomName() {
		roomName.setValue(thisRoom.getRoomName());
	}

	/**
	 * Returns the value of the new room share input field.
	 * 
	 * @return the inserted email address.
	 */
	public String getNewRoomShareUserEmail() {
		return newShareUser.getValue();
	}

	/**
	 * Sets the roomName field's value to the current Rooms name.
	 */
	public void resetNewRoomShareUserEmail() {
		newShareUser.setValue("");
	}

	/**
	 * Navigates to the room list.
	 */
	public void gotoToRoomListView() {
		getUI().getNavigator().navigateTo("Raumliste");
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		// Get the roomId out of the URL parameters.
		if (!event.getParameters().equals("")) {
			roomId = Integer.valueOf(event.getParameters());
		} else {
			roomId = -1;
		}

		initializeRoomSettingsView();
	}
}
