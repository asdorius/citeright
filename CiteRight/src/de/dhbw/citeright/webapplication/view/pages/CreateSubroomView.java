package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.event.ShortcutAction.KeyCode;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.CreateSubroomListener;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * This View enables the user to create a new subroom under a given room.
 * 
 * @author Jan (i13015)
 */
public class CreateSubroomView extends MemberAreaLayout {
	private static final long serialVersionUID = 5194831523890008804L;

	public static final String NAME = "createNewSubroom";

	private VerticalLayout contentComponent;

	private TextField subroomName;

	private int parentRoomId = -1;

	/**
	 * Initializes the View by calling the constructor of the parent class.
	 */
	public CreateSubroomView() {
		super();
	}

	/**
	 * Initializes the subroom view by placing an input field for the name on
	 * the page.
	 */
	private void initializeSubroomView() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		// Ensure, that the given parent room exists.
		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());

		Room parentRoom = roomService.getById(parentRoomId);

		String parentRoomName;

		try {
			parentRoomName = parentRoom.getRoomName();
		} catch (NullPointerException e) {
			// Parent room doesn't exist
			contentComponent
					.addComponent(new Label(
							"Der Unterraum kann nicht angelegt werden, weil der gewünschte übergeordnete Raum nicht existiert."));
			return;
		}

		// Ensure, that the user has rights to create a new room beneath that
		// parent room. This is the case if the user is owner of the parent room
		// or if the user has the administrator role assigned.
		boolean isUserAdmin = false;
		boolean isUserParentRoomOwner = false;

		try {
			isUserAdmin = RightsControl.isAdmin(getCurrentUserId());
		} catch (UserNotFoundException e) {
			// User not found, nothing to do here.
		}

		if (parentRoom.getOwnerId().intValue() == getCurrentUserId()) {
			isUserParentRoomOwner = true;
		}

		// Check if one was true
		if (!isUserAdmin && !isUserParentRoomOwner) {
			contentComponent
					.addComponent(new Label(
							"Der Unterraum kann nicht angelegt werden, da Sie keine Berechtigung haben, den Raum hier anzulegen."));
			return;
		}

		// Generate the GUI contents.
		VerticalLayout subroomVertical = new VerticalLayout();
		subroomVertical.setSpacing(true);

		// Create infomation text
		Label informationText = new Label();
		informationText
				.setValue("Bitte geben Sie einen Namen für den neuen Raum ein. Der Raum wird als Unterraum unter dem Raum '"
						+ parentRoomName + "' erstellt.");
		subroomVertical.addComponent(informationText);

		// Add a blank line
		subroomVertical.addComponent(new Label(""));

		// Add input field and submit button
		subroomName = new TextField();
		subroomName.setCaption("Raumname");
		subroomName.setRequired(true);
		subroomName.focus();
		subroomVertical.addComponent(subroomName);

		Button submitButton = new Button();
		submitButton.setCaption("Raum anlegen");
		submitButton.addClickListener(new CreateSubroomListener(this));
		submitButton.setClickShortcut(KeyCode.ENTER);
		subroomVertical.addComponent(submitButton);

		contentComponent.addComponent(subroomVertical);
	}

	/**
	 * Returns the desired subroom name as entered by the user.
	 * 
	 * @return the subroom's name
	 */
	public String getSubroomName() {
		return subroomName.getValue();
	}

	/**
	 * Sets the subroom name input value to the given string.
	 * 
	 * @param subroomName
	 *            the subroom's name
	 */
	public void setSubroomName(String subroomName) {
		this.subroomName.setValue(subroomName);
	}

	/**
	 * Returns the parentRoomId as given by URL.
	 * 
	 * @return the parentRoom's ID
	 */
	public int getParentRoomId() {
		return parentRoomId;
	}

	/**
	 * Sets the parentRoomId.
	 * 
	 * @param parentRoomId
	 *            the parentRoom's ID
	 */
	public void setParentRoomId(int parentRoomId) {
		this.parentRoomId = parentRoomId;
	}

	/**
	 * Returns the user ID of the currently logged in user for usage as the
	 * owner id.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		UserService userService = new UserService(
				HibernateUtils.getSessionFactory());
		User currentUser = userService.getById(currentUserID);

		try {
			currentUser.getId();
		} catch (NullPointerException e) {
			return -1;
		}

		return currentUserID;
	}

	/**
	 * Navigates to the settings View of this room.
	 * 
	 * @param roomId
	 *            the ID of the room whose settings are to be changed.
	 */
	public void gotoToChangeRoomSettingsView(int roomId) {
		getUI().getNavigator().navigateTo("roomSettings/" + roomId);
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {

		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}
		
		// Get the parentRoomId out of the URL parameters.
		if (!event.getParameters().equals("")) {
			parentRoomId = Integer.valueOf(event.getParameters());
		} else {
			parentRoomId = -1;
		}

		initializeSubroomView();
	}

}
