package de.dhbw.citeright.webapplication.view.pages;

import java.util.Collection;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.TreeTable;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.ReloadViewListener;
import de.dhbw.citeright.webapplication.controller.listener.RoomListCollapseListener;
import de.dhbw.citeright.webapplication.controller.listener.RoomListExpandListener;
import de.dhbw.citeright.webapplication.localmodel.adts.RoomListTableEntry;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * This view shows all rooms which are accessible by the user. This covers: -
 * Rooms created by the current user, - Rooms the current user has access to via
 * room share. Administrators can see all rooms
 * 
 * @author Jan (i13015)
 */
public class RoomListView extends MemberAreaLayout {
	private static final long serialVersionUID = 5189931383239605070L;

	public static final String NAME = "Raumliste";

	public UserService userService;

	VerticalLayout contentComponent;

	TreeTable tableAllRooms;

	/**
	 * Initializes the View by calling the constructor of the parent class.
	 */
	public RoomListView() {
		super();

		userService = new UserService();
	}

	/**
	 * Initializes the room list by creating a table containing all rooms the
	 * user has access to.
	 */
	private void initializeRoomList() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		Button reloadPageButton = new Button("Seite neu laden",
				new ReloadViewListener(NAME));
		reloadPageButton.setData(this);
		contentComponent.addComponent(reloadPageButton);

		tableAllRooms = new TreeTable(
				"Folgende Räume stehen Ihnen aktuell zur Verfügung:");
		tableAllRooms.setWidth(100, Unit.PERCENTAGE);

		// Add headers to Table
		tableAllRooms.addContainerProperty("Name", String.class, null);
		tableAllRooms.addContainerProperty("Besitzer", String.class, null);
		tableAllRooms.addContainerProperty("Aktionen", HorizontalLayout.class,
				null);

		// Load the contents of the table
		loadAllAccessibleRoomsInTable(tableAllRooms);
		expandAllRows(tableAllRooms, null);
		removeArrowsAtLeafs(tableAllRooms);

		// Listen on size changes
		updateRoomTableSize();
		tableAllRooms.addExpandListener(new RoomListExpandListener(this));
		tableAllRooms.addCollapseListener(new RoomListCollapseListener(this));

		contentComponent.addComponent(tableAllRooms);
	}

	/**
	 * Requests all accessible rooms from the database and adds them to the
	 * given table object.
	 * 
	 * @param tableObject
	 *            The table to add the rows into.
	 */
	private void loadAllAccessibleRoomsInTable(TreeTable tableObject) {

		int userId = (Integer) UI.getCurrent().getSession()
				.getAttribute("userId");

		List<RoomListTableEntry> tableEntries = getRoomsForUser(userId);

		tableObject.clear();

		// Setting a parent room will be ignored if the room doesn't already
		// exist in the database. So we will have to store the relation between
		// the rows and add it afterwards.
		LinkedList<HashMap<String, Integer>> relationTasks = new LinkedList<HashMap<String, Integer>>();

		// A list for all subordinations to be filled while creating the
		// TreeTable.
		for (RoomListTableEntry currentEntry : tableEntries) {
			tableObject.addItem(currentEntry.toTableRow(this),
					currentEntry.roomId);

			HashMap<String, Integer> relationTask = new HashMap<String, Integer>();
			relationTask.put("roomId", currentEntry.roomId);
			relationTask.put("parentRoomId", currentEntry.parentRoomId);
			relationTasks.add(relationTask);
		}

		for (HashMap<String, Integer> currentRelation : relationTasks) {
			tableObject.setParent(currentRelation.get("roomId"),
					currentRelation.get("parentRoomId"));
		}
	}

	/**
	 * Expand all rows in given table. This is done recursively among all
	 * elements.
	 * 
	 * @param tableObject
	 *            The table which contains the rows to expand.
	 * @param objectsToExpand
	 *            A collection of objects in the table that should be expanded.
	 *            Set to null if you wish to expand all elements in the table.
	 */
	private void expandAllRows(TreeTable tableObject,
			Collection<?> objectsToExpand) {

		if (objectsToExpand == null) {
			objectsToExpand = tableObject.getItemIds();
		}

		for (Object currentEntry : objectsToExpand) {
			tableObject.setCollapsed(currentEntry, false);

			Collection<?> childrenObjects = tableObject
					.getChildren(currentEntry);

			if (childrenObjects != null) {
				expandAllRows(tableObject, childrenObjects);
			}
		}
	}

	/**
	 * Remove the expand/collapse-errors at the leafs.
	 * 
	 * @param tableObject
	 *            The table which contains the rows to expand.
	 */
	private void removeArrowsAtLeafs(TreeTable tableObject) {

		// A list for all subordinations to be filled while creating the
		// TreeTable.
		for (Object currentEntry : tableObject.getItemIds()) {
			Collection<?> allChildren = tableObject.getChildren(currentEntry);
			if (allChildren == null) {
				tableObject.setChildrenAllowed(currentEntry, false);
			}
		}
	}

	public List<RoomListTableEntry> getRoomsForUser(int userId) {
		RoomService roomService = new RoomService(
				HibernateUtils.getSessionFactory());
		UserService userService = new UserService(
				HibernateUtils.getSessionFactory());

		// If the user has admin rights, he may see all rooms.
		boolean currentUserHasAdminRights = false;
		try {
			currentUserHasAdminRights = RightsControl.isAdmin((int) UI
					.getCurrent().getSession().getAttribute("userId"));
		} catch (UserNotFoundException e) {
			// Nothing needs to be done here as we initialized the variable with
			// false.
		}

		List<Room> accessibleRooms;

		if (currentUserHasAdminRights) {
			accessibleRooms = roomService.getAll();
		} else {
			accessibleRooms = roomService.getRoomsForUser(userId);
		}

		LinkedList<RoomListTableEntry> returnList = new LinkedList<RoomListTableEntry>();

		for (Room currentRoom : accessibleRooms) {
			User ownerUser = userService.getById(currentRoom.getOwnerId());
			String ownerUserName = "";

			try {
				ownerUserName = ownerUser.getLastName() + ", "
						+ ownerUser.getFirstName();
			} catch (NullPointerException e) {
				// The user was not found. As ownerUserName was declared with an
				// empty string, this is what will be shown now.
			}

			returnList.add(new RoomListTableEntry(currentRoom.getId(),
					currentRoom.getRoomName(), currentRoom.getParentId(),
					currentRoom.getOwnerId(), ownerUserName));
		}

		return returnList;
	}

	/**
	 * Updates the size of the table container according to the number of
	 * displayed elements.
	 */
	public void updateRoomTableSize() {
		// A hack to set the table height to their acutal height according to
		// the number of elements in the table.
		// http://stackoverflow.com/a/13712120
		tableAllRooms.setPageLength(tableAllRooms.getItemIds().size() + 1);
	}

	/**
	 * Navigates to the files of room with the given room id.
	 * 
	 * @param roomId
	 *            ID of the room
	 */
	public void gotoToDocumentsInRoomView(int roomId) {
		getUI().getNavigator().navigateTo("documentsInRoom/" + roomId);
	}

	/**
	 * Navigates to the View where a new subroom can be created.
	 * 
	 * @param parentRoomId
	 *            the ID of the parent room of the new room.
	 */
	public void gotoToCreateSubroomView(int parentRoomId) {
		getUI().getNavigator().navigateTo("createNewSubroom/" + parentRoomId);
	}

	/**
	 * Navigates to the settings View of this room.
	 * 
	 * @param roomId
	 *            the ID of the room whose settings are to be changed.
	 */
	public void gotoToChangeRoomSettingsView(int roomId) {
		getUI().getNavigator().navigateTo("roomSettings/" + roomId);
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");

		if (currentUserID == -1) {
			return -1;
		}

		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Reload and updates the view
	 */
	public void updateView() {
		initializeRoomList();
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		updateView();
	}

}