package de.dhbw.citeright.webapplication.view.pages;

import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.Panel;
import com.vaadin.ui.PasswordField;
import com.vaadin.ui.Slider;
import com.vaadin.ui.TabSheet;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Button.ClickEvent;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.AcceptUserRequestListener;
import de.dhbw.citeright.webapplication.controller.listener.AdminChangeEmailListener;
import de.dhbw.citeright.webapplication.controller.listener.AdminChangePasswordListener;
import de.dhbw.citeright.webapplication.controller.listener.AdminRegisterUserListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangeFirstNameListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangeLastNameListener;
import de.dhbw.citeright.webapplication.controller.listener.UserChangeEmailListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangeLowerThresholdListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangePasswordListener;
import de.dhbw.citeright.webapplication.controller.listener.ChangeUpperThresholdListener;
import de.dhbw.citeright.webapplication.controller.listener.GetUserInformationListener;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * The view for the Settings.
 * 
 * @author Kai Holzer
 *
 */
public class SettingsView extends MemberAreaLayout {
	private static final long serialVersionUID = 1419885210220508523L;

	public static final String NAME = "settings";

	VerticalLayout contentComponent;

	private UserService userService;

	private TabSheet settingsTabSheet;
	private VerticalLayout userSettingsLayout;
	private VerticalLayout roomSettingsLayout;
	private VerticalLayout adminSettingsLayout;

	private Panel emailPanel;
	private VerticalLayout emailLayout;
	private Label currentEmail;
	private TextField newEmail;
	private TextField newEmailRepeat;

	private Panel passwordPanel;
	private VerticalLayout passwordLayout;
	private PasswordField oldPassword;
	private PasswordField newPassword;
	private PasswordField newPasswordRepeat;

	private Panel thresholdPanel;
	private VerticalLayout thresholdLayout;
	private Slider lowerThresholdSlider;
	private Button lowerThresholdChangeButton;
	private Slider upperThresholdSlider;
	private Button upperThresholdChangeButton;

	private Table userRequestsTable;
	private Panel adminChangePasswordPanel;
	private VerticalLayout adminChangePasswordLayout;
	private TextField adminEmailToChangePassword;
	private PasswordField adminNewPassword;
	private PasswordField adminNewPasswordRepeat;
	private Button adminChangePasswordButton;

	private Panel adminUserInformationPanel;
	private VerticalLayout adminUserInformationLayout;
	private TextField adminUserInformationEmailRequest;
	private TextField adminUserInformationEmail;
	private TextField adminUserInformationFirstName;
	private TextField adminUserInformationLastName;

	private Panel adminNewUserPanel;
	private VerticalLayout adminNewUserLayout;
	private TextField adminNewUserEmail;
	private TextField adminNewUserFirstName;
	private TextField adminNewUserLastName;
	private PasswordField adminNewUserPassword;
	private PasswordField adminNewUserPasswordRepeat;

	/**
	 * Initializes the View and it's components.
	 */
	public SettingsView() {
		userService = new UserService(HibernateUtils.getSessionFactory());
	}

	private void initializeSettingsView() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		content.addComponent(contentComponent);

		settingsTabSheet = new TabSheet();

		// Initialisation of the userSettings tab.
		userSettingsLayout = new VerticalLayout();
		initializeEmailChange();
		initializePasswordChange();
		settingsTabSheet.addTab(userSettingsLayout, "Benutzereinstellungen");
		userSettingsLayout.setSpacing(true);
		userSettingsLayout.setMargin(true);

		// Initialisation of the roomSettings tab.
		roomSettingsLayout = new VerticalLayout();
		initializeThresholdNotification();
		settingsTabSheet.addTab(roomSettingsLayout, "Raumeinstellungen");
		roomSettingsLayout.setSpacing(true);
		roomSettingsLayout.setMargin(true);

		// Initialisation of the adminSettings tab.
		adminSettingsLayout = new VerticalLayout();
		settingsTabSheet.addTab(adminSettingsLayout,
				"Administratoreinstellungen").setEnabled(false);
		adminSettingsLayout.setSpacing(true);
		adminSettingsLayout.setMargin(true);

		// Tests if a user has administrator rights and enables the Settings if
		// he is an administrator
		try {
			if (RightsControl.isAdmin(getCurrentEmail())) {
				settingsTabSheet.getTab(2).setEnabled(true);
				initializeAdminSettings();
			}
		} catch (UserNotFoundException e) {
			// Theres no action because we only want to do something if the user
			// is an administrator
		}

		contentComponent.addComponent(settingsTabSheet);
		layout.addComponent(content);
	}

	/**
	 * Initializes the components for the administrator settings
	 */
	private void initializeAdminSettings() {
		userRequestsTable = new Table("Aktuelle Benutzeranträge");
		userRequestsTable.setSizeFull();
		userRequestsTable.setHeight("400px");

		// Add headers to the table
		userRequestsTable.addContainerProperty("ID", Integer.class, null);
		userRequestsTable.addContainerProperty("Email", String.class, null);
		userRequestsTable.addContainerProperty("Vorname", String.class, null);
		userRequestsTable.addContainerProperty("Nachname", String.class, null);
		userRequestsTable.addContainerProperty("Benutzer freischalten",
				Button.class, null);

		loadUserRequests();

		adminSettingsLayout.addComponent(userRequestsTable);

		initializeAdminPasswordChange();
		initializeAdminDataChange();
		initializeAdminNewUser();

	}

	/**
	 * Initializes the Panel where an administrator can create a new user
	 */
	private void initializeAdminNewUser() {
		adminNewUserPanel = new Panel("Einen neuen Benutzer anlegen");
		adminNewUserLayout = new VerticalLayout();
		adminNewUserLayout.setMargin(true);
		adminNewUserLayout.setSpacing(true);

		adminNewUserEmail = new TextField("Email-Adresse");
		adminNewUserEmail.setWidth("280px");
		adminNewUserLayout.addComponent(adminNewUserEmail);

		adminNewUserFirstName = new TextField("Vorname");
		adminNewUserFirstName.setWidth("280px");
		adminNewUserLayout.addComponent(adminNewUserFirstName);

		adminNewUserLastName = new TextField("Nachname");
		adminNewUserLastName.setWidth("280px");
		adminNewUserLayout.addComponent(adminNewUserLastName);

		adminNewUserPassword = new PasswordField("Passwort");
		adminNewUserPassword.setWidth("280px");
		adminNewUserLayout.addComponent(adminNewUserPassword);

		adminNewUserPasswordRepeat = new PasswordField("Passwort wiederholen");
		adminNewUserPasswordRepeat.setWidth("280px");
		adminNewUserLayout.addComponent(adminNewUserPasswordRepeat);

		adminNewUserLayout.addComponent(new Button("Benutzer anlegen",
				new AdminRegisterUserListener(this)));

		adminNewUserPanel.setContent(adminNewUserLayout);
		adminSettingsLayout.addComponent(adminNewUserPanel);
	}

	/**
	 * Initializes the panel where an administrator can change the email of an
	 * user.
	 */
	private void initializeAdminDataChange() {
		adminUserInformationPanel = new Panel("Daten eines Benutzers ändern");
		adminUserInformationLayout = new VerticalLayout();
		adminUserInformationLayout.setMargin(true);
		adminUserInformationLayout.setSpacing(true);

		adminUserInformationEmailRequest = new TextField();
		adminUserInformationEmailRequest.setCaption("Email eingeben");
		adminUserInformationEmailRequest.setWidth("280px");
		adminUserInformationLayout
				.addComponent(adminUserInformationEmailRequest);

		adminUserInformationLayout
				.addComponent(new Button("Benutzerdaten anzeigen",
						new GetUserInformationListener(this)));

		adminUserInformationEmail = new TextField("aktuelle Email");
		adminUserInformationEmail.setWidth("280px");
		adminUserInformationLayout.addComponent(adminUserInformationEmail);

		adminUserInformationLayout.addComponent(new Button("Email ändern",
				new AdminChangeEmailListener(this)));

		adminUserInformationFirstName = new TextField("aktueller Vorname");
		adminUserInformationFirstName.setWidth("280px");
		adminUserInformationLayout.addComponent(adminUserInformationFirstName);

		adminUserInformationLayout.addComponent(new Button("Vorname ändern",
				new ChangeFirstNameListener(this)));

		adminUserInformationLastName = new TextField("aktueller Nachname");
		adminUserInformationLastName.setWidth("280px");
		adminUserInformationLayout.addComponent(adminUserInformationLastName);

		adminUserInformationLayout.addComponent(new Button("Nachname ändern",
				new ChangeLastNameListener(this)));

		adminUserInformationLayout.addComponent(new Button("zurücksetzen",
				new Button.ClickListener() {
					private static final long serialVersionUID = 6436798528901483793L;

					@Override
					public void buttonClick(ClickEvent event) {
						resetUserInformation();
					}
				}));

		adminUserInformationPanel.setContent(adminUserInformationLayout);
		adminSettingsLayout.addComponent(adminUserInformationPanel);

	}

	/**
	 * Initializes the panel where an administrator can change the password of
	 * an user.
	 */
	private void initializeAdminPasswordChange() {
		adminChangePasswordPanel = new Panel("Passwort eines Benutzers ändern");
		adminChangePasswordLayout = new VerticalLayout();
		adminChangePasswordLayout.setMargin(true);
		adminChangePasswordLayout.setSpacing(true);
		adminChangePasswordLayout.setWidth("350px");

		adminEmailToChangePassword = new TextField();
		adminEmailToChangePassword.setCaption("Email eingeben");
		adminEmailToChangePassword.setWidth("280px");
		adminChangePasswordLayout.addComponent(adminEmailToChangePassword);

		adminNewPassword = new PasswordField();
		adminNewPassword.setCaption("Neues Passwort eingeben");
		adminNewPassword.setWidth("280px");
		adminChangePasswordLayout.addComponent(adminNewPassword);

		adminNewPasswordRepeat = new PasswordField();
		adminNewPasswordRepeat.setCaption("Wiederholen Sie das neue Passwort");
		adminNewPasswordRepeat.setWidth("280px");
		adminChangePasswordLayout.addComponent(adminNewPasswordRepeat);

		adminChangePasswordButton = new Button("Passwort ändern",
				new AdminChangePasswordListener(this));
		adminChangePasswordLayout.addComponent(adminChangePasswordButton);

		adminChangePasswordPanel.setContent(adminChangePasswordLayout);
		adminSettingsLayout.addComponent(adminChangePasswordPanel);
	}

	/**
	 * Initializes the components to change the threshold where you the user
	 * gets a email notification
	 */
	private void initializeThresholdNotification() {
		thresholdPanel = new Panel();
		thresholdLayout = new VerticalLayout();
		thresholdLayout.setMargin(true);
		thresholdLayout.setSpacing(true);
		thresholdPanel.setCaption("Email Benachrichtigung");

		// Initialisation for the lower threshold
		thresholdLayout.addComponent(new Label(
				"Untere Schwellwert für Benachrichtigung mit kurzem Bericht"));

		lowerThresholdSlider = new Slider(0, 100);
		lowerThresholdSlider.setWidth("400px");
		thresholdLayout.addComponent(lowerThresholdSlider);

		lowerThresholdChangeButton = new Button("unteren Schwellwert ändern",
				new ChangeLowerThresholdListener(this));
		thresholdLayout.addComponent(lowerThresholdChangeButton);

		// Initialisation for the upper threshold.
		thresholdLayout
				.addComponent(new Label(
						"Obere Schwellwert für Benachrichtigung mit ausführlichem Bericht"));

		upperThresholdSlider = new Slider(0, 100);
		upperThresholdSlider.setWidth("400px");
		thresholdLayout.addComponent(upperThresholdSlider);

		upperThresholdChangeButton = new Button("oberen Schwellwert ändern",
				new ChangeUpperThresholdListener(this));
		thresholdLayout.addComponent(upperThresholdChangeButton);

		thresholdPanel.setContent(thresholdLayout);
		roomSettingsLayout.addComponent(thresholdPanel);

		if (getCurrentEmail() != "") {
			setUpperThresholdAtSlider();
			setLowerThresholdAtSlider();
		}
	}

	/**
	 * Initializes the components to change the password.
	 */
	private void initializePasswordChange() {

		passwordPanel = new Panel();
		passwordLayout = new VerticalLayout();
		passwordLayout.setMargin(true);
		passwordLayout.setSpacing(true);
		passwordPanel.setCaption("Hier können Sie Ihr Passwort ändern");

		oldPassword = new PasswordField();
		oldPassword.setCaption("Geben Sie Ihr altes Passwort ein.");
		oldPassword.setWidth("280px");
		passwordLayout.addComponent(oldPassword);

		newPassword = new PasswordField();
		newPassword.setCaption("Geben Sie Ihr gewünschtes Passwort ein.");
		newPassword.setWidth("280px");
		passwordLayout.addComponent(newPassword);

		newPasswordRepeat = new PasswordField();
		newPasswordRepeat
				.setCaption("Wiederholen Sie hier Ihr neues Passwort.");
		newPasswordRepeat.setWidth("280px");
		passwordLayout.addComponent(newPasswordRepeat);

		passwordLayout.addComponent(new Button("Passwort ändern",
				new ChangePasswordListener(this)));

		passwordPanel.setContent(passwordLayout);
		userSettingsLayout.addComponent(passwordPanel);

	}

	/**
	 * Initializes the components to change the email.
	 */
	private void initializeEmailChange() {
		emailPanel = new Panel();
		emailLayout = new VerticalLayout();
		emailLayout.setMargin(true);
		emailLayout.setSpacing(true);
		emailPanel.setCaption("Hier können Sie Ihre Email-Adresse ändern");
		currentEmail = new Label("");
		emailLayout.addComponent(new Label("aktuelle Email:"));
		emailLayout.addComponent(currentEmail);

		newEmail = new TextField();
		newEmail.setWidth("280px");
		newEmail.setCaption("Geben Sie hier die neue Email-Adresse ein.");
		emailLayout.addComponent(newEmail);

		newEmailRepeat = new TextField();
		newEmailRepeat.setWidth("280px");
		newEmailRepeat
				.setCaption("Wiederholen Sie hier die neue Email-Adresse.");
		emailLayout.addComponent(newEmailRepeat);

		emailLayout.addComponent(new Button("Email ändern",
				new UserChangeEmailListener(this)));

		emailPanel.setContent(emailLayout);
		userSettingsLayout.addComponent(emailPanel);

		setCurrentEmailLabel();

	}

	/**
	 * returns the email of the new user.
	 * 
	 * @return the value of the text field adminNewUserEmail;
	 */
	public String getNewUserEmail() {
		return adminNewUserEmail.getValue();
	}

	/**
	 * returns the first name of the new user.
	 * 
	 * @return the value of the text field adminNewUserFirstName;
	 */
	public String getNewUserFirstName() {
		return adminNewUserFirstName.getValue();
	}

	/**
	 * returns the last name of the new user.
	 * 
	 * @return the value of the text field adminNewUserLastName;
	 */
	public String getNewUserLastName() {
		return adminNewUserLastName.getValue();
	}

	/**
	 * returns the password of the new user.
	 * 
	 * @return the value of the password field adminNewUserPassword;
	 */
	public String getNewUserPassword() {
		return adminNewUserPassword.getValue();
	}

	/**
	 * returns the password repeat of the new user.
	 * 
	 * @return the value of the password field adminNewUserPasswordRepeat;
	 */
	public String getNewUserPasswordRepeat() {
		return adminNewUserPasswordRepeat.getValue();
	}

	/**
	 * resets all entries in the new user panel.
	 */
	public void resetNewUserPanel() {
		adminNewUserEmail.setValue("");
		adminNewUserFirstName.setValue("");
		adminNewUserLastName.setValue("");
		adminNewUserPassword.setValue("");
		adminNewUserPasswordRepeat.setValue("");
	}

	/**
	 * Sets the user information in the panel adimUserInformationPanel
	 * 
	 * @param email
	 *            the email of the user
	 * @param firstName
	 *            the first name of the user
	 * @param lastName
	 *            the last name of the user
	 */
	public void setUserInformation(String email, String firstName,
			String lastName) {
		adminUserInformationEmail.setValue(email);
		adminUserInformationFirstName.setValue(firstName);
		adminUserInformationLastName.setValue(lastName);
		adminUserInformationEmailRequest.setEnabled(false);
	}

	/**
	 * Resets all entries in the user information panel.
	 */
	public void resetUserInformation() {
		adminUserInformationEmailRequest.setValue("");
		adminUserInformationEmail.setValue("");
		adminUserInformationFirstName.setValue("");
		adminUserInformationLastName.setValue("");
		adminUserInformationEmailRequest.setEnabled(true);
	}

	/**
	 * Returns the value of the textfield adminUserInformInformationRequest
	 * 
	 * @return the email out of the textfield
	 */
	public String getUserInformationEmailRequest() {
		return adminUserInformationEmailRequest.getValue();
	}

	/**
	 * Returns the new email of the user
	 * 
	 * @return the value of the textfield adminUserInformationEmail
	 */
	public String getUserInformationEmail() {
		return adminUserInformationEmail.getValue();
	}

	/**
	 * Returns the new first name of the user
	 * 
	 * @return the value of the textfield adminUserInformationFirstName
	 */
	public String getUserInformationFirstName() {
		return adminUserInformationFirstName.getValue();
	}

	/**
	 * Returns the new last name of the user
	 * 
	 * @return the value of the textfield adminUserInformationLastName
	 */
	public String getUserInformationLastName() {
		return adminUserInformationLastName.getValue();
	}

	/**
	 * loads the users requests and put them in the table.
	 */
	private void loadUserRequests() {
		List<User> users = userService.getUserRequests();

		for (User user : users) {

			int id = user.getId();
			String email = user.getEmail();
			String firstName = user.getFirstName();
			String lastName = user.getLastName();

			Button accept = new Button("freischalten",
					new AcceptUserRequestListener(this));
			accept.setData(id);

			userRequestsTable.addItem(new Object[] { id, email, firstName,
					lastName, accept }, id);

		}
	}

	/**
	 * Deletes a request for a user
	 * 
	 * @param id
	 *            request ID
	 */
	public void deleteUserRequest(int id) {
		userRequestsTable.removeItem(id);
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Returns the current value of the newEmail TextField.
	 * 
	 * @return the token in the newEmail textfield
	 */
	public String getNewEmail() {
		return newEmail.getValue();
	}

	/**
	 * Returns the current value of the newEmailRepeat TextField.
	 * 
	 * @return the token in the newEmailRepeat textfield
	 */
	public String getNewEmailRepeat() {
		return newEmailRepeat.getValue();
	}

	/**
	 * Returns the current value of the attribute email.
	 * 
	 * @return the String in the attribute email.
	 */
	public String getCurrentEmail() {
		return String.valueOf(UI.getCurrent().getSession()
				.getAttribute("email"));
	}

	/**
	 * Sets the currentEmail label
	 */
	public void setCurrentEmailLabel() {
		currentEmail.setValue(String.valueOf(UI.getCurrent().getSession()
				.getAttribute("email")));
	}

	/**
	 * Returns the current value of the oldPassword PasswordField.
	 * 
	 * @return the token in the oldPassword passwordfield
	 */
	public String getOldPassword() {
		return oldPassword.getValue();
	}

	/**
	 * Returns the current value of the newPassword PasswordField.
	 * 
	 * @return the token in the newPassword passwordfield
	 */
	public String getNewPassword() {
		return newPassword.getValue();
	}

	/**
	 * Returns the current value of the newPasswordRepeat PasswordField.
	 * 
	 * @return the token in the newPasswordRepeat passwordfield
	 */
	public String getNewPasswordRepeat() {
		return newPasswordRepeat.getValue();
	}

	/**
	 * Clears all entries in the EmailPanel.
	 */
	public void resetEmailPanel() {
		newEmail.setValue("");
		newEmailRepeat.setValue("");
	}

	/**
	 * Clears all entries in the PaswordPanel.
	 */
	public void resetPasswordPanel() {
		newPassword.setValue("");
		newPasswordRepeat.setValue("");
		oldPassword.setValue("");
	}

	/**
	 * Returns the value of the thresholdNotificationSlider.
	 * 
	 * @return the integer value of the thresholdNotificationSlider.
	 */
	public int getNewUpperThreshold() {
		return upperThresholdSlider.getValue().intValue();
	}

	/**
	 * Returns the value of the current upper threshold.
	 * 
	 * @return the integer value of the current threshold.
	 */
	public int getUpperThreshold() {
		return userService.getUpperThreshold(getCurrentEmail());
	}

	/**
	 * Sets the value of the upper threshold slider to the current value from
	 * the database.
	 */
	public void setUpperThresholdAtSlider() {
		upperThresholdSlider.setValue((double) getUpperThreshold());
	}

	/**
	 * Returns the value of the thresholdNotificationSlider.
	 * 
	 * @return the integer value of the thresholdNotificationSlider.
	 */
	public int getNewLowerThreshold() {
		return lowerThresholdSlider.getValue().intValue();
	}

	/**
	 * Returns the value of the current upper threshold.
	 * 
	 * @return the integer value of the current threshold.
	 */
	public int getLowerThreshold() {
		return userService.getLowerThreshold(getCurrentEmail());
	}

	/**
	 * Sets the value of the upper threshold slider to the current value from
	 * the database.
	 */
	public void setLowerThresholdAtSlider() {
		lowerThresholdSlider.setValue((double) getLowerThreshold());
	}

	/**
	 * returns the value of the text field adminEmailToChangePassword
	 * 
	 * @return the text in the password field adminEmailToChangePassword
	 */
	public String getEmailForAdminPasswordChange() {
		return adminEmailToChangePassword.getValue();
	}

	/**
	 * returns the value of the password field adminNewPassword
	 * 
	 * @return the text in the password field adminNewPassword
	 */
	public String getPasswordForAdminPasswordChange() {
		return adminNewPassword.getValue();
	}

	/**
	 * returns the value of the password field adminNewPasswordRepeat
	 * 
	 * @return the text in the password field adminNewPasswordRepeat
	 */
	public String getPasswordRepeatForAdminPasswordChange() {
		return adminNewPasswordRepeat.getValue();
	}

	/**
	 * Resets all values in the admin settings panel to change user passwords
	 */
	public void resetAdminPasswordChangeFields() {
		adminEmailToChangePassword.setValue("");
		adminNewPassword.setValue("");
		adminNewPasswordRepeat.setValue("");
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		initializeSettingsView();

	}
}
