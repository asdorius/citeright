package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.TextField;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.listener.SendTokencodeListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * A View where a student can enter a token to access the right upload page for
 * a certain room.
 * 
 * @author Kai Holzer
 */
public class StudentTokenView extends PreLoginLayout implements View {

	private static final long serialVersionUID = -8427154341534056908L;

	public static final String NAME = "studenttoken";
	
	private UserService userService;

	VerticalLayout contentComponent;

	private TextField tokenCode;

	/**
	 * Initializes the View and it's form.
	 */
	public StudentTokenView() {
		super();
		
		userService = new UserService();
	}

	/**
	 * Initializes the StudentTokenView
	 */
	private void initializeStudentTokenView() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		content.addComponent(contentComponent);

		initializeTokenForm();
	}

	/**
	 * Initializes textfield and button to send a token.
	 */
	private void initializeTokenForm() {

		tokenCode = new TextField("Zugangscode");
		tokenCode.setInputPrompt("z.B. xSa3rT4e3D");

		Button sendButton = new Button("absenden");
		sendButton.addClickListener(new SendTokencodeListener(this));

		FormLayout tokenForm = new FormLayout();
		tokenForm.setWidth("300px");
		tokenForm.setCaption("Geben Sie hier Ihren Zugangscode ein");
		tokenForm.addComponent(tokenCode);
		tokenForm.addComponent(sendButton);

		contentComponent.addComponent(tokenForm);
	}

	/**
	 * Returns the current value of the tokenCode TextField.
	 * 
	 * @return the token in the tokenCode textfield
	 */
	public String getTokenCode() {
		return tokenCode.getValue();
	}

	/**
	 * Resets the token input field and redirects the user to the student upload
	 * page for the room.
	 */
	public void gotoUpload() {
		getUI().getNavigator().navigateTo("studentupload/" + getTokenCode());
		tokenCode.setValue("");
	}
	
	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access and sets the focus to the tokenCode field.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		
		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() != -1) {
			return;
		}
		
		initializeStudentTokenView();
		
		tokenCode.focus();
	}

}
