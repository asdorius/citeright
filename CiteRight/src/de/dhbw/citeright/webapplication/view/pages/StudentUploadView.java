package de.dhbw.citeright.webapplication.view.pages;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.FormLayout;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Notification;
import com.vaadin.ui.UI;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.TextField;
import com.vaadin.ui.Upload;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.listener.StudentFileUploadReceiver;
import de.dhbw.citeright.webapplication.controller.listener.StudentURLUploadListener;
import de.dhbw.citeright.webapplication.view.layout.PreLoginLayout;

/**
 * the view to upload a document from a student with a tokencode
 * 
 * @author Kai Holzer
 *
 */

public class StudentUploadView extends PreLoginLayout {
	private static final long serialVersionUID = -129284470677597644L;

	public static final String NAME = "studentupload";
	
	private UserService userService;

	VerticalLayout contentComponent;

	private HorizontalLayout horizontalLayout;
	private StudentFileUploadReceiver uploadReceiver;
	private String tokenCode;
	private TextField studentNameTF;
	private TextField studentMatriculationNumberTF;
	private TextField studentEmailTF;
	private TextField urlTitleTF;
	private TextField urlTF;
	private CheckBox declarationOfAuthorshipCheckBox;

	/**
	 * Initializes the View and their elements.
	 */
	public StudentUploadView() {
		super();
		
		userService = new UserService();
	}

	private void initializeUploadView() {
		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		content.addComponent(contentComponent);

		horizontalLayout = new HorizontalLayout();
		horizontalLayout.setSpacing(true);

		initializeUploadForm();
		initializeUpload();

		contentComponent.addComponent(horizontalLayout);

		layout.addComponent(content);
	}

	/**
	 * Initializes the form where the student has to give some information about
	 * his person.
	 */
	private void initializeUploadForm() {
		FormLayout uploadForm = new FormLayout();
		uploadForm
				.setCaption("Tragen Sie hier die benötigten Informationen ein");
		uploadForm.setWidth("500px");

		studentNameTF = new TextField("Name: ");
		studentNameTF.setRequired(true);

		studentMatriculationNumberTF = new TextField("Matrikelnummer: ");
		studentMatriculationNumberTF.setRequired(true);

		studentEmailTF = new TextField("E-Mail: ");
		studentEmailTF.setRequired(true);

		Label declarationOfAuthorshipLabel = new Label(
				"Hiermit versichere ich die eingereichte Arbeit selbstständig verfasst und keine anderen als die angegebenen Quellen und Hilfsmittel verwendet zu haben.");
		declarationOfAuthorshipLabel.setWidth("350px");

		declarationOfAuthorshipCheckBox = new CheckBox(
				"Versicherung akzeptieren");
		declarationOfAuthorshipCheckBox.setRequired(true);

		uploadForm.addComponent(studentNameTF);
		uploadForm.addComponent(studentMatriculationNumberTF);
		uploadForm.addComponent(studentEmailTF);
		uploadForm.addComponent(declarationOfAuthorshipLabel);
		uploadForm.addComponent(declarationOfAuthorshipCheckBox);

		horizontalLayout.addComponent(uploadForm);

	}

	/**
	 * Initializes the elements for the upload of a file or a URL.
	 */
	private void initializeUpload() {
		VerticalLayout uploadLayout = new VerticalLayout();
		uploadLayout.setSpacing(true);
		uploadLayout
				.addComponent(new Label(
						"Sie können hier entweder eine Datei oder eine URL einreichen."));
		// File upload element for handing in by file upload.
		uploadReceiver = new StudentFileUploadReceiver(this);
		Upload upload = new Upload("Bitte Datei auswählen", uploadReceiver);
		upload.addSucceededListener(uploadReceiver);
		upload.setButtonCaption("Dokument einreichen");
		uploadLayout.addComponent(upload);

		// Textfield for handing in by URL input.
		urlTitleTF = new TextField("Titel der URL angeben");
		urlTitleTF.setWidth("400px");
		urlTF = new TextField("Url eingeben");
		urlTF.setWidth("400px");
		Button urlUploadButton = new Button("URL einreichen",
				new StudentURLUploadListener(this));
		uploadLayout.addComponent(urlTitleTF);
		uploadLayout.addComponent(urlTF);
		uploadLayout.addComponent(urlUploadButton);

		horizontalLayout.addComponent(uploadLayout);

	}

	/**
	 * Checks if a given token is valid.
	 * 
	 * @return returns true if token valid, false otherwise.
	 */
	private boolean isTokenValid(String token) {
		if (tokenCode.length() == 10) {
			return true;
		} else {
			return false;
		}

	}

	/**
	 * Checks if the form was filled out by meaning of, that all necessary
	 * fields are filled.
	 * 
	 * @return true if all fields or checkboxes are filled out, false otherwise.
	 */
	public boolean formFilledOut() {
		if (studentNameTF.getValue().equals("")
				|| studentEmailTF.getValue().equals("")
				|| studentMatriculationNumberTF.getValue().equals("")
				|| declarationOfAuthorshipCheckBox.getValue().equals(false)) {
			return false;
		} else {
			return true;
		}
	}

	/**
	 * Returns the email of the student
	 * 
	 * @return the value of the studentEmailTF
	 */
	public String getStudentEmail() {
		return studentEmailTF.getValue();
	}

	/**
	 * Returns the matriculation number of the student
	 * 
	 * @return the value of the studentMatriculationNumberTF
	 */
	public String getStudentMatriculationNumber() {
		return studentMatriculationNumberTF.getValue();
	}

	/**
	 * Returns the name of the student
	 * 
	 * @return the value of the studentNameTF
	 */
	public String getStudentName() {
		return studentNameTF.getValue();
	}

	/**
	 * Returns the current value of the url TextField.
	 * 
	 * @return the text in the url textfield
	 */
	public String getUploadUrl() {
		return urlTF.getValue();
	}

	/**
	 * Returns the current value of the urlTitle TextField.
	 * 
	 * @return the text in the urlTitle textfield
	 */
	public String getUploadUrlTitle() {
		return urlTitleTF.getValue();
	}

	/**
	 * Returns the current value of the tokenCode TextField.
	 * 
	 * @return the text in the tokenCode textfield
	 */
	public String getTokenCode() {
		return tokenCode;
	}
	
	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");
		
		if (currentUserID == -1) {
			return -1;
		}
		
		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access. Besides that, the given token will be checked if it is correct.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);
		
		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() != -1) {
			return;
		}

		// Get the token out of the URL parameters.
		tokenCode = event.getParameters();

		// Check if token is valid.
		if (!isTokenValid(tokenCode)) {
			Notification.show("Fehlerhafter Tokencode", Type.ERROR_MESSAGE);
			getUI().getNavigator().navigateTo("studenttoken");
			return;
		}
		
		initializeUploadView();
	}
}
