package de.dhbw.citeright.webapplication.view.pages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.DashboardGotoRoomButtonListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener.OnDemandStreamResource;
import de.dhbw.citeright.webapplication.controller.listener.ReloadViewListener;
import de.dhbw.citeright.webapplication.localmodel.adts.AttributedButton;
import de.dhbw.citeright.webapplication.localmodel.adts.OnDemandReportSource;
import de.dhbw.citeright.webapplication.localmodel.adts.RecentDocumentTableEntry;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * The view for the dashboard.
 * 
 * @author Jan (i13015)
 */
public class DashboardView extends MemberAreaLayout {
	private static final long serialVersionUID = 1308098181529459644L;
	public static final String NAME = "dashboard";

	VerticalLayout contentComponent;

	Table tableLastHandedInDocuments;

	private UserService userService;
	private RoomService roomService;
	private DocumentService documentService;

	/**
	 * Initializes the View by calling the constructor of the parent class.
	 */
	public DashboardView() {
		userService = new UserService(HibernateUtils.getSessionFactory());
		roomService = new RoomService(HibernateUtils.getSessionFactory());
		documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Initializes the dashboard with a welcome screen and a table of documents,
	 * that have recently been handed in.
	 */
	private void initializeDashboard() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		Button reloadPageButton = new Button("Seite neu laden",
				new ReloadViewListener(NAME));
		reloadPageButton.setData(this);
		contentComponent.addComponent(reloadPageButton);

		// Divide the Dashboard with a horizontal layout in an information text
		// and the table of recent handed in documents.
		VerticalLayout dashboardVertical = new VerticalLayout();

		Label informationText = new Label();
		informationText.setValue("Herzlich Willkommen beim Plagiatsprüfer.");

		dashboardVertical.addComponent(informationText);

		tableLastHandedInDocuments = new Table("Aktuell eingereichte Dokumente");
		tableLastHandedInDocuments.setWidth(100, Unit.PERCENTAGE);

		// Add headers to Table
		tableLastHandedInDocuments.addContainerProperty("Datum", String.class,
				null);
		tableLastHandedInDocuments.addContainerProperty("Raum", String.class,
				null);
		tableLastHandedInDocuments.addContainerProperty("Student",
				String.class, null);
		tableLastHandedInDocuments.addContainerProperty("Status", String.class,
				null);
		tableLastHandedInDocuments.addContainerProperty("Plagiatsmaß",
				String.class, null);
		tableLastHandedInDocuments.addContainerProperty("Aktionen",
				HorizontalLayout.class, null);

		// Load the contents of the table
		loadRecentDocumentsInTable(tableLastHandedInDocuments);

		// A hack to set the table height to their acutal height according to
		// the number of elements in the table.
		// http://stackoverflow.com/a/13712120
		tableLastHandedInDocuments.setPageLength(tableLastHandedInDocuments
				.getItemIds().size() + 1);

		dashboardVertical.addComponent(tableLastHandedInDocuments);

		contentComponent.addComponent(dashboardVertical);
	}

	/**
	 * Loads recent handed in documents out of the database and adds their
	 * metadata to the table given in the parameter.
	 * 
	 * @param tableObject
	 *            The table to add the rows into.
	 */
	private void loadRecentDocumentsInTable(Table tableObject) {
		List<RecentDocumentTableEntry> tableEntries = getLastDocumentsForUser(
				getCurrentUserId(),
				CiteRightConstants.MAXIMUM_AGE_OF_RECENT_FILES);
		tableObject.clear();

		for (RecentDocumentTableEntry currentEntry : tableEntries) {
			tableObject.addItem(formatTableRow(currentEntry),
					currentEntry.documentId);
		}
	}

	/**
	 * Formats a given table entry so that the values are human readable.
	 * 
	 * @param tableEntry
	 *            The entry that shall be formatted.
	 * @return An array of the cells as their individual objects.
	 */
	private Object[] formatTableRow(RecentDocumentTableEntry tableEntry) {

		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		if (tableEntry.documentStatus == CiteRightConstants.STATUS_WAITING_IN_QUEUE) {
			documentStatusOutput = "In Warteschlange zur Prüfung";
		} else if (tableEntry.documentStatus == CiteRightConstants.STATUS_IN_AUDIT) {
			documentStatusOutput = "Prüfung läuft";
		} else if (tableEntry.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			documentStatusOutput = "Prüfung abgeschlossen";
		} else if (tableEntry.documentStatus == CiteRightConstants.STATUS_AUDIT_FAILED) {
			documentStatusOutput = "Prüfung fehlgeschlagen";
		} else { // Document Status = Unknown
			documentStatusOutput = "Status unbekannt";
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String
				.valueOf(tableEntry.plagiarismDegree) + " %";

		// Only show plagiarism degree if audit has finished.
		if (tableEntry.documentStatus != CiteRightConstants.STATUS_AUDIT_FINISHED) {
			plagiarismDegreeOutput = "";
		}

		// Add buttons for the view of files and audit reports.
		HorizontalLayout actionButtonsLayout = new HorizontalLayout();

		AttributedButton showGotoRoomButton = new AttributedButton();
		showGotoRoomButton.setCaption("Zum Raum");
		showGotoRoomButton.setDescription("Dateien in Raum '"
				+ tableEntry.roomName + "' anzeigen");
		showGotoRoomButton.setAttribute("roomId", tableEntry.roomId);
		showGotoRoomButton
				.addClickListener(new DashboardGotoRoomButtonListener(this));
		actionButtonsLayout.addComponent(showGotoRoomButton);

		// Only show the audit report button when the audit has finished
		if (tableEntry.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			AttributedButton showAuditReportButton = new AttributedButton();
			showAuditReportButton.setCaption("Bericht");
			showAuditReportButton.setDescription("Den Prüfbericht ansehen");
			showAuditReportButton.setAttribute("documentId",
					tableEntry.documentId);
			actionButtonsLayout.addComponent(showAuditReportButton);

			OnDemandStreamResource reportStreamResource = new OnDemandReportSource(
					tableEntry.documentId);
			OnDemandDownloadListener reportDownloader = new OnDemandDownloadListener(
					reportStreamResource);
			reportDownloader.extend(showAuditReportButton);
		}

		return new Object[] {
				new SimpleDateFormat(
						CiteRightConstants.SIMPLEDATEFORMAT_DATETIME)
						.format(tableEntry.dateOfEntry), tableEntry.roomName,
				tableEntry.studentNameAndMatriculationNumber,
				documentStatusOutput, plagiarismDegreeOutput,
				actionButtonsLayout };
	}

	/**
	 * Generates a list of table rows that represent recently handed in
	 * documents.
	 * 
	 * @param userId
	 *            the user for which the display shall be made.
	 * @param ageInHours
	 *            the maximum age of a document to call it "recent".
	 * @return a list of table rows
	 */
	public List<RecentDocumentTableEntry> getLastDocumentsForUser(int userId,
			int ageInHours) {
		// A linked list is perfectly fine here as we will only iterate once and
		// in order over the list.
		LinkedList<RecentDocumentTableEntry> returnList = new LinkedList<RecentDocumentTableEntry>();

		// Store list of rooms as arraylist for easy access in comparison
		// purposes.
		ArrayList<Integer> accessibleRooms = new ArrayList<Integer>();

		for (Room currentRoom : roomService.getRoomsForUser(getCurrentUserId(),
				false)) {
			accessibleRooms.add(currentRoom.getId());
		}

		// Store a hashmap of all room names to show the room names in the
		// table.
		HashMap<Integer, Room> allRooms = new HashMap<Integer, Room>();
		for (Room currentRoom : roomService.getAll()) {
			allRooms.put(currentRoom.getId(), currentRoom);
		}

		// Get a list of recently uploaded documents. If there is one in one of
		// the room this user has access to, show it. Administrators can see all
		// documents.
		boolean currentUserIsAdmin = false;
		try {
			currentUserIsAdmin = RightsControl.isAdmin(getCurrentUserId());
		} catch (UserNotFoundException e) {
			// The user wasn't found. hopefully this case will never occur.
		}

		for (Document currentDocument : documentService
				.getNewDocuments(ageInHours)) {
			if (currentUserIsAdmin
					|| accessibleRooms.contains(currentDocument.getParentId())) {

				// Skip this document if it was automatically uploaded.
				if (currentDocument.isForeignSource()) {
					continue;
				}

				// Catch null values in student matriculation number.
				String matriculationNumber = "";

				if (currentDocument.getStudentMatriculationNumber() != null) {
					matriculationNumber = currentDocument
							.getStudentMatriculationNumber();
				}

				// Catch null values in student name.
				String studentName = "";

				if (currentDocument.getStudentName() != null) {
					studentName = currentDocument.getStudentName();
				}

				// Catch null values in plagiarism degree.
				int plagiarismDegree = 0;

				if (currentDocument.getPlagiarismDegree() != null) {
					plagiarismDegree = Math.round(currentDocument
							.getPlagiarismDegree() * 100);
				}

				returnList.add(new RecentDocumentTableEntry(currentDocument
						.getId(), currentDocument.getTimestamp(), allRooms.get(
						currentDocument.getParentId()).getRoomName(),
						currentDocument.getParentId(), studentName,
						matriculationNumber, currentDocument.getStatusId(),
						plagiarismDegree));
			}
		}

		return returnList;
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");

		if (currentUserID == -1) {
			return -1;
		}

		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Navigates to the files of room with the given room id.
	 * 
	 * @param roomId
	 *            ID of the room
	 */
	public void gotoToDocumentsInRoomView(int roomId) {
		getUI().getNavigator().navigateTo("documentsInRoom/" + roomId);
	}

	/**
	 * Reload and updates the view
	 */
	public void updateView() {
		initializeDashboard();
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {
		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		updateView();
	}

}
