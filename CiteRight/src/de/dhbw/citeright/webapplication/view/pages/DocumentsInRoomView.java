package de.dhbw.citeright.webapplication.view.pages;

import java.util.LinkedList;
import java.util.List;

import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.AccessService;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener.OnDemandStreamResource;
import de.dhbw.citeright.webapplication.controller.listener.ReloadViewListener;
import de.dhbw.citeright.webapplication.localmodel.adts.DocumentsInRoomTableEntry;
import de.dhbw.citeright.webapplication.localmodel.adts.OnDemandMultipleReportSource;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.layout.MemberAreaLayout;

/**
 * This view shows all documents in a given room. But only if the user has
 * access to the room.
 * 
 * @author Jan (i13015)
 */
public class DocumentsInRoomView extends MemberAreaLayout {
	private static final long serialVersionUID = 826769709204473136L;
	public static final String NAME = "documentsInRoom";

	private int roomId = -1;

	VerticalLayout contentComponent;

	private UserService userService;
	private RoomService roomService;
	private AccessService accessService;
	private DocumentService documentService;

	boolean isUserAdmin;
	boolean isUserRoomOwner;
	boolean hasUserRoomShare;

	public Room thisRoom;
	List<DocumentsInRoomTableEntry> tableEntries;

	Table tableAllDocuments;

	/**
	 * Initializes the View by calling the constructor of the parent class.
	 */
	public DocumentsInRoomView() {
		userService = new UserService(HibernateUtils.getSessionFactory());
		roomService = new RoomService(HibernateUtils.getSessionFactory());
		accessService = new AccessService(HibernateUtils.getSessionFactory());
		documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Initializes the room list by creating a table containing all rooms the
	 * user has access to.
	 */
	private void initializeDocumentList() {

		// Make our own container that we can redraw on each page visit.
		if (contentComponent != null) {
			content.removeComponent(contentComponent);
		}

		contentComponent = new VerticalLayout();
		contentComponent.setSpacing(true);
		content.addComponent(contentComponent);

		Button reloadPageButton = new Button("Seite neu laden",
				new ReloadViewListener(NAME));
		reloadPageButton.setData(this);
		contentComponent.addComponent(reloadPageButton);

		// Ensure that the room exists.
		thisRoom = roomService.getById(roomId);

		String roomName;

		try {
			roomName = thisRoom.getRoomName();
		} catch (NullPointerException e) {
			// Room doesn't exist
			contentComponent.addComponent(new Label(
					"Auf den Raum konnte nicht zugegriffen werden."));
			return;
		}

		// Ensure that the user has the rights to view the documents in this
		// room.
		isUserAdmin = false;
		isUserRoomOwner = false;
		hasUserRoomShare = false;

		try {
			isUserAdmin = RightsControl.isAdmin(getCurrentUserId());
		} catch (UserNotFoundException e) {
			// User not found, nothing to do here. This case can hardly exist.
		}

		if (thisRoom.getOwnerId().intValue() == getCurrentUserId()) {
			isUserRoomOwner = true;
		}

		if (accessService.getRoomShare(getCurrentUserId(), roomId) != null) {
			hasUserRoomShare = true;
		}

		// Check if one was true
		if (!isUserAdmin && !isUserRoomOwner && !hasUserRoomShare) {
			contentComponent
					.addComponent(new Label(
							"Sie haben keine Berechtigung die Dokumente des Raums anzusehen."));
			return;
		}

		// Create information text for this View
		Label informationText = new Label("Sie sehen die Dokumente des Raums '"
				+ roomName + "'");
		contentComponent.addComponent(informationText);

		updateDocumentsTable();

		contentComponent.addComponent(tableAllDocuments);

		// Add print multiple button whose listener will get the checked
		// documents and print their reports.
		Button printMultipleButton = new Button();
		printMultipleButton.setCaption("Ausgewählte Berichte drucken");
		contentComponent.addComponent(printMultipleButton);

		OnDemandStreamResource reportStreamResource = new OnDemandMultipleReportSource(
				this);
		OnDemandDownloadListener reportDownloader = new OnDemandDownloadListener(
				reportStreamResource);
		reportDownloader.extend(printMultipleButton);
	}

	public void updateDocumentsTable() {
		tableAllDocuments = new Table();
		tableAllDocuments.setWidth(100, Unit.PERCENTAGE);

		// Add headers to Table
		tableAllDocuments.addContainerProperty("Druck", CheckBox.class, null);
		tableAllDocuments.addContainerProperty("Datum", String.class, null);
		tableAllDocuments.addContainerProperty("Student", String.class, null);
		tableAllDocuments.addContainerProperty("Status", String.class, null);
		tableAllDocuments.addContainerProperty("Plagiatsmaß", String.class,
				null);
		tableAllDocuments.addContainerProperty("Aktionen",
				HorizontalLayout.class, null);

		// Load data into table
		loadDocumentsOfRoomInTable();

		// A hack to set the table height to their acutal height according to
		// the number of elements in the table.
		// http://stackoverflow.com/a/13712120
		tableAllDocuments
				.setPageLength(tableAllDocuments.getItemIds().size() + 1);
	}

	/**
	 * Loads all document of this room into the allDocuments-table.
	 * 
	 */
	private void loadDocumentsOfRoomInTable() {
		tableEntries = getDocumentsOfRoom();

		tableAllDocuments.clear();

		for (DocumentsInRoomTableEntry currentEntry : tableEntries) {
			tableAllDocuments.addItem(currentEntry.toTableRow(this,
					(isUserAdmin || isUserRoomOwner)), currentEntry.documentId);
		}
	}

	/**
	 * Retrieve the documents in this room from the database.
	 * 
	 * @return a list of documents.
	 */
	private List<DocumentsInRoomTableEntry> getDocumentsOfRoom() {
		List<DocumentsInRoomTableEntry> returnList = new LinkedList<DocumentsInRoomTableEntry>();

		for (Document currentDocument : documentService
				.getDocumentsInRoom(this.roomId)) {

			// Skip this document if it was automatically uploaded.
			if (currentDocument.isForeignSource()) {
				continue;
			}

			// Catch null values in student matriculation number.
			String matriculationNumber = "";

			if (currentDocument.getStudentMatriculationNumber() != null) {
				matriculationNumber = currentDocument
						.getStudentMatriculationNumber();
			}

			// Catch null values in student name.
			String studentName = "";

			if (currentDocument.getStudentName() != null) {
				studentName = currentDocument.getStudentName();
			}

			// Catch null values in plagiarism degree.
			int plagiarismDegree = 0;

			if (currentDocument.getPlagiarismDegree() != null) {
				plagiarismDegree = Math.round(currentDocument
						.getPlagiarismDegree() * 100);
			}

			returnList.add(new DocumentsInRoomTableEntry(currentDocument
					.getId(), currentDocument.getFilename(), currentDocument
					.getTimestamp(), studentName, matriculationNumber,
					currentDocument.getStatusId(), plagiarismDegree));
		}

		return returnList;
	}

	/**
	 * Return a list of document IDs containing all elements that are checked.
	 * 
	 * @return checked documents' IDs.
	 */
	public List<Integer> getDocumentIdsOfCheckedDocuments() {
		LinkedList<Integer> returnList = new LinkedList<Integer>();

		for (DocumentsInRoomTableEntry currentEntry : this.tableEntries) {
			if (currentEntry.printCheckBox.getValue()) {
				returnList.add(currentEntry.documentId);
			}
		}

		return returnList;
	}

	/**
	 * Returns the user ID of the currently logged in user.
	 * 
	 * @return the current user's ID
	 */
	public int getCurrentUserId() {
		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");

		if (currentUserID == -1) {
			return -1;
		}

		User currentUser = userService.getById(currentUserID);

		if (currentUser == null) {
			return -1;
		} else {
			return currentUserID;
		}
	}

	/**
	 * Reload and updates the view
	 */
	public void updateView() {
		initializeDocumentList();
	}

	/**
	 * Is called when the View has been loaded
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	public void enter(ViewChangeEvent event) {

		super.enter(event);

		// Prevent the access to this View if the user is not logged in.
		if (getCurrentUserId() == -1) {
			return;
		}

		// Get the roomId out of the URL parameters.
		if (!event.getParameters().equals("")) {
			roomId = Integer.valueOf(event.getParameters());
		} else {
			roomId = -1;
		}

		updateView();
	}

}