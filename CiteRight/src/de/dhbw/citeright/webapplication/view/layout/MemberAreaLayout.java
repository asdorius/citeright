package de.dhbw.citeright.webapplication.view.layout;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.webapplication.view.pages.DashboardView;
import de.dhbw.citeright.webapplication.view.pages.LecturerUploadView;
import de.dhbw.citeright.webapplication.view.pages.RoomListView;
import de.dhbw.citeright.webapplication.view.pages.SettingsView;
import de.dhbw.citeright.webapplication.view.pages.StartView;

/**
 * This is the layout for all Views when a User has logged in
 * 
 * @author Kai Holzer
 */
public class MemberAreaLayout extends VerticalLayout implements View {
	private static final long serialVersionUID = -5278796844815283449L;
	protected VerticalLayout layout;
	protected VerticalLayout content;

	public MemberAreaLayout() {
		layout = new VerticalLayout();
		content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);

		setMenuBar();
		layout.addComponent(content);
		addComponent(layout);
	}

	/**
	 * Generates a menubar at the top of the page
	 */
	private void setMenuBar() {
		MenuBar menu = new MenuBar();
		menu.setWidth(100, Sizeable.Unit.PERCENTAGE);
		menu.addItem("", new ThemeResource("images/CiteRightWhiteSmall.png"),
				menuCommand);
		menu.addItem("Dashboard", menuCommand);
		menu.addItem("Raumliste", menuCommand);
		menu.addItem("Upload", menuCommand);
		menu.addItem("Einstellungen", menuCommand);
		menu.addItem("Logout", menuCommand);
		layout.addComponent(menu);
	}

	/**
	 * Is called when a button in the menubar is pressed
	 */
	MenuBar.Command menuCommand = new MenuBar.Command() {
		private static final long serialVersionUID = -4476440929051538239L;

		@Override
		public void menuSelected(MenuItem selectedItem) {
			switch (selectedItem.getText()) {
			case "":
				getUI().getNavigator().navigateTo(DashboardView.NAME);
				break;
			case "Dashboard":
				getUI().getNavigator().navigateTo(DashboardView.NAME);
				break;
			case "Raumliste":
				getUI().getNavigator().navigateTo(RoomListView.NAME);
				break;
			case "Einstellungen":
				getUI().getNavigator().navigateTo(SettingsView.NAME);
				break;
			case "Logout":
				Notification.show("Erfolgreich!", "Sie wurden ausgeloggt",
						Type.HUMANIZED_MESSAGE);
				getSession().setAttribute("email", "");
				getSession().setAttribute("userId", -1);
				getUI().getNavigator().navigateTo(StartView.NAME);
				break;
			case "Upload":
				getUI().getNavigator().navigateTo(LecturerUploadView.NAME);
				break;
			}
		}
	};

	/**
	 * gets called if a view did load
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		String loggedUser = String.valueOf(UI.getCurrent().getSession()
				.getAttribute("email"));

		// Redirect the user to the login screen if he is not logged in.
		if (loggedUser.equals("")) {
			getUI().getNavigator().navigateTo("");
			return;
		}
	}
}
