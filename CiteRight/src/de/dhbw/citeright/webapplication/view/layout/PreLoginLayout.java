package de.dhbw.citeright.webapplication.view.layout;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener.ViewChangeEvent;
import com.vaadin.server.Sizeable;
import com.vaadin.server.ThemeResource;
import com.vaadin.ui.MenuBar;
import com.vaadin.ui.MenuBar.MenuItem;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;

import de.dhbw.citeright.webapplication.view.pages.RegistrationView;
import de.dhbw.citeright.webapplication.view.pages.StartView;
import de.dhbw.citeright.webapplication.view.pages.StudentTokenView;

/**
 * This is the layout for all Views before a User was logged in.
 * 
 * @author Kai Holzer
 */
public class PreLoginLayout extends VerticalLayout implements View {
	private static final long serialVersionUID = 8462173378047470682L;
	protected VerticalLayout layout;
	protected VerticalLayout content;

	/**
	 * Initializes the Layout
	 */
	public PreLoginLayout() {
		super();

		initializePreLoginLayout();
	}

	private void initializePreLoginLayout() {
		layout = new VerticalLayout();
		content = new VerticalLayout();
		content.setMargin(true);
		content.setSpacing(true);

		setMenuBar();
		layout.addComponent(content);
		addComponent(layout);
	}

	/**
	 * Generates a menubar at the top of the page
	 */
	private void setMenuBar() {
		MenuBar menu = new MenuBar();
		menu.setWidth(100, Sizeable.Unit.PERCENTAGE);
		menu.addItem("", new ThemeResource("images/CiteRightWhiteSmall.png"),
				menuCommand);
		menu.addItem("Studentenupload", menuCommand);
		menu.addItem("Registrieren", menuCommand);

		layout.addComponent(menu);
	}

	/**
	 * Is called when a button in the menubar is pressed
	 */
	MenuBar.Command menuCommand = new MenuBar.Command() {
		private static final long serialVersionUID = -7440488171045183206L;

		/**
		 * Overried the standard menuSelected method of MenuBar with our
		 * navigation procedures for the Views.
		 * 
		 * @param selectedItem
		 *            An Object holding information on the selected item in the
		 *            menu bar.
		 */
		@Override
		public void menuSelected(MenuItem selectedItem) {
			switch (selectedItem.getText()) {
			case "Studentenupload":
				getUI().getNavigator().navigateTo(StudentTokenView.NAME);
				break;
			case "Registrieren":
				getUI().getNavigator().navigateTo(RegistrationView.NAME);
			case "":
				getUI().getNavigator().navigateTo(StartView.NAME);
				break;
			}
		}
	};

	/**
	 * Is called when a view was loaded. Informs the parent class about the
	 * access. If a user is already logged in, he will be redirected to the
	 * dashboard to prevent the display of the login page.
	 * 
	 * @param event
	 *            The event object of the change.
	 */
	@Override
	public void enter(ViewChangeEvent event) {
		String loggedUser = String.valueOf(UI.getCurrent().getSession()
				.getAttribute("email"));

		// Redirect the user to the member area if he is logged in
		if (!loggedUser.equals("")) {
			getUI().getNavigator().navigateTo("dashboard");
			return;
		}
	}

}
