package de.dhbw.citeright.webapplication.localmodel.exceptions;

/**
 * This Exception can be thrown when one tries to access a User object that
 * doesn't exist.
 * 
 * @author Jan (i13015)
 *
 */
public class UserNotFoundException extends Exception {
	private static final long serialVersionUID = -747898350140994816L;

	/**
	 * Creates a new UserNotFoundException.
	 */
	public UserNotFoundException() {
		super();
	}

	/**
	 * Creates a new UserNotFoundException with a message.
	 * @param message the message
	 */
	public UserNotFoundException(String message) {
		super(message);
	}
}
