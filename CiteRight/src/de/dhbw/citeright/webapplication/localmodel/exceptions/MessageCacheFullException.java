package de.dhbw.citeright.webapplication.localmodel.exceptions;
/**
 * Is thrown, when the cache of a ConnectionActor overflows.
 * @author Florian Knecht
 *
 */
public class MessageCacheFullException extends Exception {
	private static final long serialVersionUID = 4418813015612110795L;

}
