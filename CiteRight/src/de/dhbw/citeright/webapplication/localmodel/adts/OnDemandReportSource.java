package de.dhbw.citeright.webapplication.localmodel.adts;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.text.SimpleDateFormat;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener.OnDemandStreamResource;

/**
 * Creates a report source for use with {@link OnDemandDownloadListener}. Sets
 * the report data as late as possible.
 * 
 * @author Jan (i13015)
 *
 */
public class OnDemandReportSource implements OnDemandStreamResource {
	private static final long serialVersionUID = -3481187715565564830L;

	private final Integer documentId;
	private DocumentService documentService;
	private Document thisDocument;

	/**
	 * Instantiates the source with the report for the document identified by
	 * the given document ID.
	 * 
	 * @param documentId
	 *            the report to download on click.
	 */
	public OnDemandReportSource(Integer documentId) {
		super();
		this.documentId = documentId;
		this.documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Checks, whether the current data assigned to this object ist valid.
	 * 
	 * @return true if valid, false otherwise.
	 */
	private boolean isDocumentDataUpToDate() {
		if (thisDocument == null) {

			// Ensure document exists.
			thisDocument = documentService.getById(documentId);
			if (thisDocument == null) {
				Notification.show("Fehler",
						"Das Dokument mit dieser ID existiert nicht.",
						Type.ERROR_MESSAGE);
				return false;
			}

		}

		// Ensure the document is in status finished.
		if (thisDocument.getStatusId() != CiteRightConstants.STATUS_AUDIT_FINISHED) {
			Notification
					.show("Fehler",
							"Das Dokument wurde noch nicht erfolgreich geprüft. Ein Bericht steht daher nicht zur Verfügung.",
							Type.ERROR_MESSAGE);
			return false;
		}

		return true;
	}

	/**
	 * Returns the data of the assigned report.
	 * 
	 * @return report data.
	 */
	@Override
	public InputStream getStream() {
		// Update Document information
		if (!isDocumentDataUpToDate()) {
			return null;
		}

		// Fetch document data from database.
		byte[] documentData = thisDocument.getAuditReport();

		// Ensure the document has a report attached.
		if (documentData == null) {
			Notification.show("Fehler",
					"Zu diesem Dokument steht kein Bericht zur Verfügung.",
					Type.ERROR_MESSAGE);
			return null;
		}

		return new ByteArrayInputStream(documentData);
	}

	/**
	 * Returns the desired file name for the report.
	 * 
	 * @return the file name.
	 */
	@Override
	public String getFilename() {
		// Update Document information
		if (!isDocumentDataUpToDate()) {
			return null;
		}

		String filenameDownload = thisDocument.getFilename();

		if (filenameDownload == null) {
			filenameDownload = new SimpleDateFormat(
					CiteRightConstants.SIMPLEDATEFORMAT_FILENAME)
					.format(thisDocument.getTimestamp());
		}

		return "report_for_" + filenameDownload + ".pdf";
	}

}
