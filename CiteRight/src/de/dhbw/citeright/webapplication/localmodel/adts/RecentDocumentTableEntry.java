package de.dhbw.citeright.webapplication.localmodel.adts;

import java.text.SimpleDateFormat;
import java.util.Date;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;

/**
 * Holds the values of a table row in the recent handed in documents-table at
 * the dashboard.
 * 
 * @author Jan (i13015)
 */
public class RecentDocumentTableEntry {
	public final Integer documentId;
	public final Date dateOfEntry;
	public final String roomName;
	public final Integer roomId;
	public final String studentNameAndMatriculationNumber;
	public final Integer documentStatus;
	public final Integer plagiarismDegree;

	/**
	 * Initializes the table entry.
	 * 
	 * @param documentId
	 *            ID of the document to reference it for download.
	 * @param dateOfEntry
	 *            Date object containing the date when the file was handed in.
	 * @param roomName
	 *            Name of the room in which this document is placed.
	 * @param studentNameAndMatriculationNumber
	 *            For now a string containing the student's name and
	 *            matriculation number.
	 * @param documentStatus
	 *            The current audit status of the document.
	 * @param plagiarismDegree
	 *            The plagiarism degree of the document after audit has
	 *            finished.
	 */
	public RecentDocumentTableEntry(Integer documentId, Date dateOfEntry,
			String roomName, Integer roomId, String studentName,
			String studentMatriculationNumber, Integer documentStatus,
			Integer plagiarismDegree) {
		this.documentId = documentId;
		this.dateOfEntry = dateOfEntry;
		this.roomName = roomName;
		this.roomId = roomId;
		this.plagiarismDegree = plagiarismDegree;
		
		if (documentStatus == null) {
			this.documentStatus = CiteRightConstants.STATUS_UNKNOWN;
		} else {
			this.documentStatus = documentStatus;
		}

		if (studentMatriculationNumber.equals("")) {
			this.studentNameAndMatriculationNumber = studentName;
		} else {
			this.studentNameAndMatriculationNumber = studentName + " ("
					+ studentMatriculationNumber + ")";
		}
	}

	/**
	 * Outputs the stored data for debugging purposes.
	 * 
	 * @return A String containing all attributes formatted.
	 */
	@Override
	public String toString() {
		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		if (this.documentStatus == CiteRightConstants.STATUS_WAITING_IN_QUEUE) {
			documentStatusOutput = "In Warteschlange zur Prüfung";
		} else if (this.documentStatus == CiteRightConstants.STATUS_IN_AUDIT) {
			documentStatusOutput = "Prüfung läuft";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			documentStatusOutput = "Prüfung abgeschlossen";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FAILED) {
			documentStatusOutput = "Prüfung fehlgeschlagen";
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String.valueOf(this.plagiarismDegree)
				+ " %";

		return new SimpleDateFormat(CiteRightConstants.SIMPLEDATEFORMAT_DATETIME).format(this.dateOfEntry) + "; "
				+ this.studentNameAndMatriculationNumber + "; "
				+ documentStatusOutput + "; " + plagiarismDegreeOutput;
	}
}
