package de.dhbw.citeright.webapplication.localmodel.adts;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.UI;

import de.dhbw.citeright.webapplication.controller.RightsControl;
import de.dhbw.citeright.webapplication.controller.listener.RoomListActionButtonListener;
import de.dhbw.citeright.webapplication.localmodel.exceptions.UserNotFoundException;
import de.dhbw.citeright.webapplication.view.pages.RoomListView;

/**
 * Holds the values of a table row in the room list table.
 * 
 * @author Jan (i13015)
 */
public class RoomListTableEntry {
	public final Integer roomId;
	public final String roomName;
	public final Integer parentRoomId;
	public final Integer roomOwnerId;
	public final String roomOwnerName;

	/**
	 * Initializes the table entry with the given data.
	 * 
	 * @param roomId
	 *            ID of the room to reference it for opening.
	 * @param roomName
	 *            Displayable name of the room.
	 * @param parentRoomId
	 *            ID of the parent room.
	 * @param roomOwnerId
	 *            User-ID of the Owner.
	 * @param roomOwnerName
	 *            Displayable name of the room's owner's name.
	 * 
	 */
	public RoomListTableEntry(Integer roomId, String roomName,
			Integer parentRoomId, Integer roomOwnerId, String roomOwnerName) {
		this.roomId = roomId;
		this.roomName = roomName;
		this.parentRoomId = parentRoomId;
		this.roomOwnerId = roomOwnerId;
		this.roomOwnerName = roomOwnerName;
	}

	/**
	 * Formats a given table entry so that the values are human readable.
	 * 
	 * @return An array of the cells as their individual objects.
	 */
	public Object[] toTableRow(RoomListView parentView) {

		int currentUserID = (int) UI.getCurrent().getSession()
				.getAttribute("userId");

		// Generate a HorizontalLayout to arrange the action buttons in the
		// cell.
		HorizontalLayout actionButtonsLayout = new HorizontalLayout();

		AttributedButton buttonShowFilesInRoom = new AttributedButton();
		buttonShowFilesInRoom.setCaption("Öffnen");
		buttonShowFilesInRoom.setDescription("Dateien des Raumes ansehen");
		buttonShowFilesInRoom.setAttribute("roomId", this.roomId);
		buttonShowFilesInRoom.setAttribute("action", "showFilesInRoom");
		buttonShowFilesInRoom
				.addClickListener(new RoomListActionButtonListener(parentView));
		actionButtonsLayout.addComponent(buttonShowFilesInRoom);

		// Try to find out if the current user has admin rights. Fall on the
		// safe side if not.
		boolean currentUserHasAdminRights = false;
		try {
			currentUserHasAdminRights = RightsControl.isAdmin(currentUserID);
		} catch (UserNotFoundException e) {
			// Nothing needs to be done here as we initialized the variable with
			// false.
		}

		// Only show the following buttons if the user is the owner of the
		// room or is administrator of the system.
		if (this.roomOwnerId == currentUserID || currentUserHasAdminRights) {
			AttributedButton buttonCreateSubroom = new AttributedButton();
			buttonCreateSubroom.setCaption("Unterraum");
			buttonCreateSubroom.setDescription("Unterraum erstellen");
			buttonCreateSubroom.setAttribute("roomId", this.roomId);
			buttonCreateSubroom.setAttribute("action", "createSubroom");
			buttonCreateSubroom
					.addClickListener(new RoomListActionButtonListener(
							parentView));
			actionButtonsLayout.addComponent(buttonCreateSubroom);

			AttributedButton buttonRoomSettings = new AttributedButton();
			buttonRoomSettings.setCaption("Einstellungen");
			buttonRoomSettings.setDescription("Einstellungen des Raums bearbeiten");
			buttonRoomSettings.setAttribute("roomId", this.roomId);
			buttonRoomSettings.setAttribute("action", "modifySettings");
			buttonRoomSettings
					.addClickListener(new RoomListActionButtonListener(
							parentView));
			actionButtonsLayout.addComponent(buttonRoomSettings);
		}

		return new Object[] { this.roomName, this.roomOwnerName,
				actionButtonsLayout };
	}

	/**
	 * Returns the ID of the represented room.
	 * 
	 * @return the room's ID
	 */
	public Integer getRoomId() {
		return this.roomId;
	}

	/**
	 * Returns the ID of the parent of this room.
	 * 
	 * @return the parent room's ID
	 */
	public Integer getParentRoomId() {
		return this.parentRoomId;
	}

	/**
	 * Outputs the stored data for debugging purposes.
	 * 
	 * @return A String containing all attributes formatted.
	 */
	@Override
	public String toString() {
		return this.roomName + " (" + String.valueOf(this.roomId) + "); "
				+ this.roomOwnerName + " (" + String.valueOf(this.roomOwnerId)
				+ ")";
	}
}
