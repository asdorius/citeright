package de.dhbw.citeright.webapplication.localmodel.adts;

import java.util.HashMap;

import com.vaadin.server.Resource;
import com.vaadin.ui.Button;

/**
 * Extends the standard Vaadin Button with a Button that can hold attributes.
 * 
 * @author Jan (i13015)
 *
 */
public class AttributedButton extends Button {
	private static final long serialVersionUID = 9169769007444860334L;
	private HashMap<String, Object> attributes = new HashMap<String, Object>();
	
	/**
	 * Creates a new push button.
	 */
	public AttributedButton() {
		super();
	}
	
	/**
	 * Creates a new push button with the given icon.
	 * @param icon the icon
	 */
	public AttributedButton(Resource icon) {
		super(icon);
	}
	
	/**
	 * Creates a new push button with the given caption.
	 * @param caption the Button caption
	 */
	public AttributedButton(java.lang.String caption) {
		super(caption);
	}
	
	/**
	 * Creates a new push button with a click listener.
	 * @param caption the Button caption
	 * @param listener the Button click listener
	 */
	public AttributedButton(java.lang.String caption, Button.ClickListener listener) {
		super(caption, listener);
	}
	
	/**
	 * Creates a new push button with the given caption and icon.
	 * @param caption the caption
	 * @param icon the icon
	 */
	public AttributedButton(java.lang.String caption, Resource icon) {
		super(caption, icon);
	}
	
	/**
	 * Set an attribute for this Button.
	 * @param key Key for the Attribute.
	 * @param value Value to store for this key.
	 */
	public void setAttribute(String key, Object value) {
		attributes.put(key, value);
	}
	
	/**
	 * Returns a value for a given attribute 
	 * @param key Key for the Attribute.
	 * @return value that is assigned to the key. null if key doesn't exist.
	 */
	public Object getAttribute(String key) {
		if (attributes.containsKey(key)) {
			return attributes.get(key);
		} else {
			return null;
		}
	}

}
