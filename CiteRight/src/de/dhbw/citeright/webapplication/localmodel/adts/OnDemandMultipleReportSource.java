package de.dhbw.citeright.webapplication.localmodel.adts;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;

import com.vaadin.ui.Notification;
import com.vaadin.ui.Notification.Type;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener.OnDemandStreamResource;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;

/**
 * Creates a report source for use with {@link OnDemandDownloadListener}. Sets
 * the document data as late as possible. Reads the reports to download out of
 * the list in the given view.
 * 
 * @author Jan (i13015)
 *
 */
public class OnDemandMultipleReportSource implements OnDemandStreamResource {
	private static final long serialVersionUID = -3481187715565564830L;

	private final DocumentsInRoomView documentsInRoomView;
	private DocumentService documentService;

	/**
	 * Instantiates the source with the list of reports.
	 * 
	 * @param documentsInRoomView
	 *            The room where the list of reports can be found.
	 */
	public OnDemandMultipleReportSource(DocumentsInRoomView documentsInRoomView) {
		super();
		this.documentsInRoomView = documentsInRoomView;
		this.documentService = new DocumentService(
				HibernateUtils.getSessionFactory());
	}

	/**
	 * Returns the data of the selected reports.
	 * 
	 * @return report data.
	 */
	@Override
	public InputStream getStream() {
		// Ensure there is at least one document checked.
		List<Integer> checkedDocuments = documentsInRoomView
				.getDocumentIdsOfCheckedDocuments();

		if (checkedDocuments.size() == 0) {
			Notification
					.show("Fehler!",
							"Bitte wählen Sie mindestens ein Dokument aus zu dem der Bericht ausgedruckt werden soll.",
							Type.ERROR_MESSAGE);
			return null;
		}

		// Create a list of byte arrays containing all reports that are to be
		// merged.
		List<byte[]> individualReports = new LinkedList<byte[]>();

		for (Integer currentDocumentId : checkedDocuments) {
			// Ensure the document exists.
			Document currentDocument = documentService
					.getById(currentDocumentId);

			if (currentDocument == null) {
				Notification.show("Fehler!", "Das Dokument mit der ID '"
						+ currentDocumentId + "' existiert nicht.",
						Type.ERROR_MESSAGE);
				continue;
			}

			// Ensure the document is in status AUDIT_FINISHED.
			if (currentDocument.getStatusId() != CiteRightConstants.STATUS_AUDIT_FINISHED) {
				Notification.show("Fehler!", "Das Dokument mit der ID '"
						+ currentDocumentId
						+ "' wurde noch nicht erfolgreich geprüft.",
						Type.ERROR_MESSAGE);
				continue;
			}

			// Ensure the report exists
			byte[] currentDocumentData = currentDocument.getAuditReport();
			if (currentDocumentData == null) {
				Notification.show("Fehler!", "Das Dokument mit der ID '"
						+ currentDocumentId + "' hat keinen Bericht.",
						Type.ERROR_MESSAGE);
				continue;
			}

			// Add report to the list of files to merge.
			individualReports.add(currentDocumentData);
		}

		// Ensure we have at least one report
		int numberOfReports = individualReports.size();
		byte[] documentData;

		if (numberOfReports == 0) {
			Notification.show("Fehler!",
					"Es stehen keine Berichte zum Druck zur Verfügung.",
					Type.ERROR_MESSAGE);
			return null;
		} else if (numberOfReports == 1) {
			documentData = individualReports.get(0);
		} else {
			PDFMergerUtility pdfMerger = new PDFMergerUtility();

			// Don't use foreach here because that would cause Java to copy all
			// single element into memory. This is not necessary especially when
			// handling with big data ressources.
			for (int currentIndex = 0; currentIndex < numberOfReports; currentIndex++) {
				pdfMerger.addSource(new ByteArrayInputStream(individualReports
						.get(currentIndex)));
			}

			ByteArrayOutputStream mergedPdfOutput = new ByteArrayOutputStream();
			pdfMerger.setDestinationStream(mergedPdfOutput);

			try {
				pdfMerger.mergeDocuments();
			} catch (COSVisitorException | IOException e) {
				Notification
						.show("Fehler",
								"Die Berichte konnten nicht heruntergeladen werden, weil die einzelnen Berichte nicht zu einer Datei zusammengefasst werden konnten.",
								Type.ERROR_MESSAGE);
				return null;
			}

			documentData = mergedPdfOutput.toByteArray();
		}

		return new ByteArrayInputStream(documentData);
	}

	/**
	 * Returns the desired file name for the document.
	 * 
	 * @return the file name.
	 */
	@Override
	public String getFilename() {
		return "report_binder.pdf";
	}

}
