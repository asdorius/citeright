package de.dhbw.citeright.webapplication.localmodel.adts;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.vaadin.ui.Button;
import com.vaadin.ui.CheckBox;
import com.vaadin.ui.HorizontalLayout;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.webapplication.controller.listener.DocumentsInRoomRemoveDocumentButtonListener;
import de.dhbw.citeright.webapplication.controller.listener.DocumentsInRoomRestartAuditButtonListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener;
import de.dhbw.citeright.webapplication.controller.listener.OnDemandDownloadListener.OnDemandStreamResource;
import de.dhbw.citeright.webapplication.view.pages.DocumentsInRoomView;

/**
 * Holds the values of a table row in the documents table at the
 * DocumentsInRoomView.
 * 
 * @author Jan (i13015)
 */
public class DocumentsInRoomTableEntry {
	public final Integer documentId;
	public final String documentName;
	public final Date dateOfEntry;
	public final String studentNameAndMatriculationNumber;
	public final Integer documentStatus;
	public final Integer plagiarismDegree;
	public final CheckBox printCheckBox;

	/**
	 * Initializes the table entry.
	 * 
	 * @param documentId
	 *            ID of the document to reference it for download.
	 * @param dateOfEntry
	 *            Date object containing the date when the file was handed in.
	 * @param studentName
	 *            A String containing the student's name.
	 * @param studentMatriculationNumber
	 *            A string containing the student's matriculation number.
	 * @param documentStatus
	 *            The current audit status of the document.
	 * @param plagiarismDegree
	 *            The plagiarism degree of the document after audit has
	 *            finished.
	 */
	public DocumentsInRoomTableEntry(Integer documentId, String documentName,
			Date dateOfEntry, String studentName,
			String studentMatriculationNumber, Integer documentStatus,
			Integer plagiarismDegree) {
		this.documentId = documentId;
		this.documentName = documentName;
		this.dateOfEntry = dateOfEntry;
		this.plagiarismDegree = plagiarismDegree;

		if (documentStatus == null) {
			this.documentStatus = CiteRightConstants.STATUS_UNKNOWN;
		} else {
			this.documentStatus = documentStatus;
		}

		if (studentMatriculationNumber.equals("")) {
			this.studentNameAndMatriculationNumber = studentName;
		} else {
			this.studentNameAndMatriculationNumber = studentName + " ("
					+ studentMatriculationNumber + ")";
		}

		this.printCheckBox = new CheckBox();
	}

	/**
	 * Formats a given table entry so that the values are human readable.
	 * 
	 * @param userHasEditRights
	 *            States if the user has the rights to restart an audit of a
	 *            document.
	 * @return An array of the cells as their individual objects.
	 */
	public Object[] toTableRow(DocumentsInRoomView parentView,
			boolean userHasEditRights) {

		// Disable the checkbox for multi report printing if audit is not
		// finished.
		if (this.documentStatus != CiteRightConstants.STATUS_AUDIT_FINISHED) {
			this.printCheckBox.setEnabled(false);
		}

		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		if (this.documentStatus == CiteRightConstants.STATUS_WAITING_IN_QUEUE) {
			documentStatusOutput = "In Warteschlange zur Prüfung";
		} else if (this.documentStatus == CiteRightConstants.STATUS_IN_AUDIT) {
			documentStatusOutput = "Prüfung läuft";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			documentStatusOutput = "Prüfung abgeschlossen";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FAILED) {
			documentStatusOutput = "Prüfung fehlgeschlagen";
		} else if (this.documentStatus == CiteRightConstants.STATUS_UNKNOWN) {
			documentStatusOutput = "Status unbekannt";
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String.valueOf(this.plagiarismDegree)
				+ " %";

		// Only show plagiarism degree if audit has finished.
		if (this.documentStatus != CiteRightConstants.STATUS_AUDIT_FINISHED) {
			plagiarismDegreeOutput = "";
		}

		// Add buttons for the view of files and audit reports.
		HorizontalLayout actionButtonsLayout = new HorizontalLayout();

		Button showOriginalFileButton = new Button();
		showOriginalFileButton.setCaption("Ansehen");
		showOriginalFileButton.setDescription("Eingereichte Originaldatei '"
				+ this.documentName + "' herunterladen");
		actionButtonsLayout.addComponent(showOriginalFileButton);

		OnDemandStreamResource documentStreamResource = new OnDemandDocumentSource(
				this.documentId);
		OnDemandDownloadListener documentDownloader = new OnDemandDownloadListener(
				documentStreamResource);
		documentDownloader.extend(showOriginalFileButton);

		// Only show the audit report button when the audit has finished.
		if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			AttributedButton showAuditReportButton = new AttributedButton(
					"Bericht");
			showAuditReportButton.setAttribute("documentId", this.documentId);
			showAuditReportButton.setCaption("Bericht");
			showAuditReportButton
					.setDescription("Den Prüfbericht für dieses Dokument herunterladen");
			actionButtonsLayout.addComponent(showAuditReportButton);

			OnDemandStreamResource reportStreamResource = new OnDemandReportSource(
					this.documentId);
			OnDemandDownloadListener reportDownloader = new OnDemandDownloadListener(
					reportStreamResource);
			reportDownloader.extend(showAuditReportButton);
		}

		// Only show the restart audit button if the first audit has finished
		// and the user has owner or admin rights on this room.
		// Only show the audit report button when the audit has finished.
		if ((this.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED || this.documentStatus == CiteRightConstants.STATUS_AUDIT_FAILED)
				&& userHasEditRights) {
			AttributedButton showRestartAuditButton = new AttributedButton();
			showRestartAuditButton.setAttribute("documentId", this.documentId);
			showRestartAuditButton.setCaption("Neustart");
			showRestartAuditButton
					.setDescription("Die Plagiatsprüfung für das Dokument neu starten.");
			showRestartAuditButton
					.addClickListener(new DocumentsInRoomRestartAuditButtonListener(
							parentView));
			actionButtonsLayout.addComponent(showRestartAuditButton);
		}

		AttributedButton removeDocumentButton = new AttributedButton();
		removeDocumentButton.setAttribute("documentId", this.documentId);
		removeDocumentButton.setCaption("Löschen");
		removeDocumentButton.setDescription("Dokument '" + this.documentName
				+ "' löschen");
		removeDocumentButton
				.addClickListener(new DocumentsInRoomRemoveDocumentButtonListener(
						parentView));
		actionButtonsLayout.addComponent(removeDocumentButton);

		return new Object[] {
				this.printCheckBox,
				new SimpleDateFormat(
						CiteRightConstants.SIMPLEDATEFORMAT_DATETIME)
						.format(this.dateOfEntry),
				this.studentNameAndMatriculationNumber, documentStatusOutput,
				plagiarismDegreeOutput, actionButtonsLayout };
	}

	/**
	 * Outputs the stored data for debugging purposes.
	 * 
	 * @return A String containing all attributes formatted.
	 */
	@Override
	public String toString() {
		// Format the DocumentStatus so that it is human-readable.
		String documentStatusOutput = "";

		if (this.documentStatus == CiteRightConstants.STATUS_WAITING_IN_QUEUE) {
			documentStatusOutput = "In Warteschlange zur Prüfung";
		} else if (this.documentStatus == CiteRightConstants.STATUS_IN_AUDIT) {
			documentStatusOutput = "Prüfung läuft";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FINISHED) {
			documentStatusOutput = "Prüfung abgeschlossen";
		} else if (this.documentStatus == CiteRightConstants.STATUS_AUDIT_FAILED) {
			documentStatusOutput = "Prüfung fehlgeschlagen";
		} else if (this.documentStatus == CiteRightConstants.STATUS_UNKNOWN) {
			documentStatusOutput = "Status unbekannt";
		}

		// Format the plagiarism degree to be human-readable.
		String plagiarismDegreeOutput = String.valueOf(this.plagiarismDegree)
				+ " %";

		return new SimpleDateFormat(
				CiteRightConstants.SIMPLEDATEFORMAT_DATETIME)
				.format(this.dateOfEntry)
				+ "; "
				+ this.studentNameAndMatriculationNumber
				+ "; "
				+ documentStatusOutput + "; " + plagiarismDegreeOutput;
	}
}
