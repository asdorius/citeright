package de.dhbw.citeright.essentials.remoteactor;

import java.io.Serializable;
import java.util.List;

/**
 * Message that is used for the communication to process Documents
 * 
 * @author Enrico Schrödter
 *
 */
public class ProcessDocumentMessage implements Serializable {

	/**
	 * Needed for transfer over Network
	 */
	private static final long serialVersionUID = -42104712487042795L;

	int documentID = 0;
	List<Integer> roomIDs;

	/**
	 * 
	 * @param docID
	 *            Database-ID of the Document that has just been uploaded
	 */
	public ProcessDocumentMessage(List<Integer> roomIDs, int docID) {
		documentID = docID;
		this.roomIDs = roomIDs;
	}

	/**
	 * 
	 * @return the room ids
	 */
	public List<Integer> getRoomIDs() {
		return roomIDs;
	}

	/**
	 * 
	 * @return the documents id
	 */
	public int getDocumentID() {
		return documentID;
	}

}
