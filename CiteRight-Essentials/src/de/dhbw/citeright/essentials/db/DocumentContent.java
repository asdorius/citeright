package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the DocumentContent entity
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "DocumentContent")
public class DocumentContent {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	private Integer id;
	@Column(name = "File")
	private Integer documentId;

	/**
	 * Default constructor mainly used by Hibernate
	 */
	public DocumentContent() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(Integer fileId) {
		this.documentId = fileId;
	}

}
