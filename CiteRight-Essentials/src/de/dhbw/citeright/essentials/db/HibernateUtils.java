package de.dhbw.citeright.essentials.db;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

/**
 * Signleton that creates and returns a Hibernate SessionFactory
 * 
 * @author Enrico Schrödter
 *
 */
public class HibernateUtils {
	private static  SessionFactory sessionFactory;


	/**
	 * Returns the current SessionFactory
	 * 
	 * @return the current SessionFactory instance
	 */
	public static SessionFactory getSessionFactory() {
		
		if(sessionFactory == null){
			try {
				Configuration configuration = new Configuration()
				.configure(
						HibernateUtils.class
						.getResource("/hibernate.cfg.xml"))
						.addAnnotatedClass(Action.class)
						.addAnnotatedClass(Role.class)
						.addAnnotatedClass(User.class)
						.addAnnotatedClass(Room.class)
						.addAnnotatedClass(Document.class)
						.addAnnotatedClass(PossibleMatch.class)
						.addAnnotatedClass(Section.class)
						.addAnnotatedClass(DocumentContent.class)
						.addAnnotatedClass(Access.class)
						.addAnnotatedClass(IncludedRoom.class)
						.addAnnotatedClass(Reference.class)
						.addAnnotatedClass(RoleRight.class);
				StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
				serviceRegistryBuilder.applySettings(configuration.getProperties());
				ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
				sessionFactory = configuration.buildSessionFactory(serviceRegistry);
			} catch (Throwable ex) {
				System.err.println("Initial SessionFactory creation failed." + ex);
				throw new ExceptionInInitializerError(ex);
			}
			
		}
		
		return sessionFactory;
	}
}
