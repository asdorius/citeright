package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Section Entity of the Database.
 * 
 * @author Enrico Schrödter
 *
 */
@Entity
@Table(name = "Section")
public class Section {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	private Integer id;
	@Column(name = "Document_ID")
	private Integer documentId;
	@Column(name = "headword")
	private String headwords;
	@Column(name = "OrderingNumber")
	private Integer orderingNumber;
	@Column(name = "SectionText")
	private String sectionText;

	public Section() {

	}

	public Section(String sectionText, String headWords, int document,
			int orderingNumber) {
		this.sectionText = sectionText;
		this.headwords = headWords;
		this.documentId = document;
		this.orderingNumber = orderingNumber;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getDocumentId() {
		return documentId;
	}

	public void setDocumentId(int documentId) {
		this.documentId = documentId;
	}

	public String getHeadwords() {
		return headwords;
	}

	public void setHeadwords(String headWord) {
		this.headwords = headWord;
	}

	public Integer getOrderingNumber() {
		return orderingNumber;
	}

	public void setOrderingNumber(int orderingNumber) {
		this.orderingNumber = orderingNumber;
	}

	public String getSectionText() {
		return sectionText;
	}

	public void setSectionText(String sectionText) {
		this.sectionText = sectionText;
	}
}
