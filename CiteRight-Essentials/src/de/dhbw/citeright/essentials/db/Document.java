package de.dhbw.citeright.essentials.db;

import java.sql.Timestamp;
import java.util.Arrays;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Document Entity of the Database
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "Document")
public class Document {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GeneratedValue()
	private Integer id;
	@Column(name = "Filename")
	private String filename;
	@Column(name = "Filetype")
	private String filetype;
	@Column(name = "data")
	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[] data;
	@Column(name = "UploadDate")
	private Timestamp timestamp;
	@Column(name = "Parent")
	private Integer parentId;
	@Column(name = "Reference")
	private Integer referenceId = null;
	@Column(name = "studentMatriculationNumber")
	private String studentMatriculationNumber;
	@Column(name = "studentEmail")
	private String studentEmail;
	@Column(name = "studentName")
	private String studentName;
	@Column(name = "plagiarismDegree")
	private Float plagiarismDegree;
	@Column(name = "status")
	private Integer statusId = 5;
	@Column(name = "auditReport")
	@Basic(fetch = FetchType.LAZY)
	@Lob
	private byte[] auditReport;
	@Column(name = "isForeignSource")
	private boolean isForeignSource = false;

	/**
	 * Indicates if the document has a foreign source
	 * 
	 * @return true if it has a foreign source, else false
	 */
	public boolean isForeignSource() {
		return isForeignSource;
	}

	/**
	 * Sets if the document is a foreign source
	 * 
	 * @param isForeignSource
	 *            true if so, else false
	 */
	public void setForeignSource(boolean isForeignSource) {
		this.isForeignSource = isForeignSource;
	}

	/**
	 * Returns to audit report
	 * 
	 * @return the audit report
	 */
	public byte[] getAuditReport() {
		return auditReport;
	}

	/**
	 * Sets the audit report of the current document
	 * 
	 * @param auditReport
	 *            the audit report to set
	 */
	public void setAuditReport(byte[] auditReport) {
		this.auditReport = auditReport;
	}

	/**
	 * @return the statusId
	 */
	public Integer getStatusId() {
		return statusId;
	}

	/**
	 * @param statusId
	 *            the statusId to set
	 */
	public void setStatusId(Integer statusId) {
		this.statusId = statusId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param id
	 *            the Id of the file
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 * @param referenceId
	 *            the reference Id
	 */
	public Document(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 * @param referenceId
	 *            the reference Id
	 */
	public Document(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId, int referenceId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
		this.referenceId = referenceId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 */
	public Document(String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	/**
	 * Creates a File with the given information
	 * 
	 * @param id
	 *            the Id of the file
	 * @param filename
	 *            the name of the file
	 * @param filetype
	 *            the type of the file
	 * @param data
	 *            the data as byte array
	 * @param timestamp
	 *            the creation Date
	 * @param parentId
	 *            the parents Id
	 */
	public Document(int id, String filename, String filetype, byte[] data,
			Timestamp timestamp, int parentId) {
		this.id = id;
		this.filename = filename;
		this.filetype = filetype;
		this.data = data;
		this.timestamp = timestamp;
		this.parentId = parentId;
	}

	/**
	 * The default constructor that is mainly used by Hibernate
	 */
	public Document() {

	}

	@Override
	public String toString() {
		return "File [id=" + id + ", filename=" + filename + ", filetype="
				+ filetype + ", data=" + Arrays.toString(data) + ", timestamp="
				+ timestamp + ", parentId=" + parentId + ", referenceId="
				+ referenceId + "]";
	}

	/**
	 * @param id
	 *            the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @param parentId
	 *            the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}

	/**
	 * @param referenceId
	 *            the referenceId to set
	 */
	public void setReferenceId(Integer referenceId) {
		this.referenceId = referenceId;
	}

	/**
	 * @return the filename
	 */
	public String getFilename() {
		return filename;
	}

	/**
	 * @param filename
	 *            the filename to set
	 */
	public void setFilename(String filename) {
		this.filename = filename;
	}

	/**
	 * @return the filetype
	 */
	public String getFiletype() {
		return filetype;
	}

	/**
	 * @param filetype
	 *            the filetype to set
	 */
	public void setFiletype(String filetype) {
		this.filetype = filetype;
	}

	/**
	 * @return the data
	 */
	public byte[] getData() {
		return data;
	}

	/**
	 * @param data
	 *            the data to set
	 */
	public void setData(byte[] data) {
		this.data = data;
	}

	/**
	 * @return the timestamp
	 */
	public Timestamp getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp
	 *            the timestamp to set
	 */
	public void setTimestamp(Timestamp timestamp) {
		this.timestamp = timestamp;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}

	/**
	 * @return the studentMatriculationNumber
	 */
	public String getStudentMatriculationNumber() {
		return studentMatriculationNumber;
	}

	/**
	 * @param studentMatriculationNumber
	 *            the studentMatriculationNumber to set
	 */
	public void setStudentMatriculationNumber(String studentMatriculationNumber) {
		this.studentMatriculationNumber = studentMatriculationNumber;
	}

	/**
	 * @return the studentEmail
	 */
	public String getStudentEmail() {
		return studentEmail;
	}

	/**
	 * @param studentEmail
	 *            the studentEmail to set
	 */
	public void setStudentEmail(String studentEmail) {
		this.studentEmail = studentEmail;
	}

	/**
	 * @return the studentName
	 */
	public String getStudentName() {
		return studentName;
	}

	/**
	 * @param studentName
	 *            the studentName to set
	 */
	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	/**
	 * @return the plagiarismDegree
	 */
	public Float getPlagiarismDegree() {
		return plagiarismDegree;
	}

	/**
	 * @param plagiarismDegree
	 *            the plagiarismDegree to set
	 */
	public void setPlagiarismDegree(Float plagiarismDegree) {
		this.plagiarismDegree = plagiarismDegree;
	}

	/**
	 * @return the referenceId
	 */
	public Integer getReferenceId() {
		return referenceId;
	}

}