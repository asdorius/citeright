package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Included Room Entity of the Database. It tells
 * which rooms shall be contained in the plagiarism audit.
 * 
 * @author entwickler
 *
 */
@Entity
@Table(name = "IncludedRoom")
public class IncludedRoom {
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	Integer id;
	@Column(name = "RoomId")
	Integer roomid;
	@Column(name = "IncludedRoomId")
	Integer includedRoomId;

	public IncludedRoom() {

	}

	public IncludedRoom(int roomid, int includedRoomId) {
		this.roomid = roomid;
		this.includedRoomId = includedRoomId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getRoomid() {
		return roomid;
	}

	public void setRoomid(Integer roomid) {
		this.roomid = roomid;
	}

	public Integer getIncludedRoomId() {
		return includedRoomId;
	}

	public void setIncludedRoomId(Integer includedRoomId) {
		this.includedRoomId = includedRoomId;
	}

}
