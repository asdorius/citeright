package de.dhbw.citeright.essentials.db;

public enum Language {
	GERMAN, ENGLISH, FRENCH
}
