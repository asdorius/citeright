package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.helper.RandomStringHelper;
import de.dhbw.citeright.essentials.mail.MailingSingleton;

/**
 * Class that handles the User related interactions with the Database
 * 
 * @author Enrico Schrödter
 *
 */
public class UserService extends AbstractService<User> {
	/**
	 * Creates an UserService instance that used the given SessionFactory for
	 * its Hibernate Sessions
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that should be used
	 */
	public UserService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	public UserService() {
		super(HibernateUtils.getSessionFactory());
	}

	/**
	 * Checks if the given login information are correct
	 * 
	 * @param email
	 *            the given email of the user
	 * @param password
	 *            the given password of the user
	 * @return valid login information or not
	 */
	public boolean login(String email, String password) {
		User user = getUserByEmail(email);
		if (user != null) {
			return user.getPassword().equals(password)
					&& user.getRoleId() != CiteRightConstants.WAITING_FOR_ACTIVATION;
		}
		return false;
	}

	/**
	 * Registration for new users Wrapper for create
	 * 
	 * @param user
	 *            the user that wants to get registred
	 * @return id of the new user
	 */
	public int registrate(User user) {
		user.setRoleId(3);
		int userId = create(user);

		RoomService roomService = new RoomService(sessionFactory);
		Room room = new Room();
		room.setRoomName(user.getEmail());
		room.setOwnerId(userId);
		room.setToken(RandomStringHelper.getRandomString(10));
		room.setParentId(CiteRightConstants.ROOT_ROOM_ID);

		roomService.create(room);

		return userId;
	}

	/**
	 * Returns the User identified by the email or null
	 * 
	 * @param email
	 *            the email that identifies the user
	 * @return the user if one exists, else null
	 */
	public User getUserByEmail(String email) {
		Session session = null;
		try {
			session = sessionFactory.openSession();
			Transaction transaction = session.beginTransaction();

			Query query = session.createQuery("FROM User WHERE email = :email");
			query.setParameter("email", email);

			List<User> queryResult = query.list();
			transaction.commit();
			if (queryResult.size() > 0) {
				return queryResult.get(0);
			}

			return null;
		} catch (Exception e) {
			return null;
		} finally {
			session.close();
		}
	}

	/**
	 * Changes the Email of the User. Calls update(User) itself.
	 * 
	 * @param oldEmail
	 *            the old email of the User, has to be the email that is stored
	 *            at the DB
	 * @param newEmail
	 *            the updated email of the User, that should be stored. After
	 *            the call this will be the new email address of the user
	 * @return true on success, else false
	 */
	public boolean changeEmail(String oldEmail, String newEmail) {
		User user = getUserByEmail(oldEmail);
		if (user != null) {
			user.setEmail(newEmail);
			return update(user);
		}

		return false;
	}

	/**
	 * Changes the first name of the user. Calls update(user) itself.
	 * 
	 * @param email
	 *            the email of the user where the first name should get changed
	 * @param firstName
	 *            the new first name of the user
	 * @return true on success, else false
	 */
	public boolean changeFirstName(String email, String firstName) {
		User user = getUserByEmail(email);
		if (user != null) {
			user.setFirstName(firstName);
			return update(user);
		}
		return false;
	}

	/**
	 * Changes the last name of the user. Calls update(user) itself.
	 * 
	 * @param email
	 *            the email of the user where the last name should get changed
	 * @param lastName
	 *            the new last name of the user
	 * @return true on success, else false
	 */
	public boolean changeLastName(String email, String lastName) {
		User user = getUserByEmail(email);
		if (user != null) {
			user.setLastName(lastName);
			return update(user);
		}
		return false;
	}

	/**
	 * Changes the password of an user
	 * 
	 * @param email
	 *            the email that identifies the user
	 * @param oldPassword
	 *            his old password, which means his password before change
	 * @param newPassword
	 *            the password that the user uses from now on
	 * @return true on success, else false
	 */
	public boolean changePassword(String email, String oldPassword,
			String newPassword) {
		User user = getUserByEmail(email);
		if (user != null && user.getPassword().equals(oldPassword)) {
			user.setPassword(newPassword);
			return update(user);
		}

		return false;
	}

	/**
	 * Changes the password of a user, without knowing the old one. Use wisely.
	 * 
	 * @param email
	 *            the email that identifies the user
	 * @param newPassword
	 *            the new password that should be set for him
	 * @return true on success, else false
	 */
	public boolean changePassword(String email, String newPassword) {
		User user = getUserByEmail(email);
		if (user != null) {
			user.setPassword(newPassword);
			return update(user);
		}

		return false;
	}

	/**
	 * Sets a new generated password for the User identified by the email
	 * address
	 * 
	 * @param email
	 *            the email address that identifies the User
	 * @return true on success, else false
	 */
	public boolean resetPassword(String email) {
		User user = getUserByEmail(email);
		if (user != null) {
			String password = RandomStringHelper.getRandomString(8);
			user.setPassword(password);
			return update(user);
		}

		return false;
	}

	/**
	 * Returns a list of users that are waiting for activation. That are users
	 * that are defined by a special id in their role field.
	 * 
	 * @return the list of not activated users
	 */
	public List<User> getUserRequests() {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		List<User> users = session.createQuery(
				"FROM User u WHERE u.roleId = "
						+ CiteRightConstants.WAITING_FOR_ACTIVATION).list();

		transaction.commit();
		session.close();

		return users;
	}

	/**
	 * Returns a list of users that have administrator privileges
	 * 
	 * @return the list of users with administrator privileges
	 */
	public List<User> getAdministrators() {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		List<User> users = session.createQuery(
				"FROM User u WHERE u.roleId = "
						+ CiteRightConstants.ADMINISTRATOR).list();

		transaction.commit();
		session.close();

		return users;
	}

	/**
	 * Activates an User
	 * 
	 * @param id
	 *            the id of the user that should be activated
	 * @return true on success, else false
	 */
	public boolean acceptUserRequest(int id) {
		User user = getById(id);
		user.setRoleId(CiteRightConstants.LECTURER);

		MailingSingleton mailing = MailingSingleton.getInstance();
		mailing.sendActivationMailToUser(user);
		return update(user);
	}

	/**
	 * Returns the upper Threshold of the user identified by the email
	 * 
	 * @param email
	 *            the users email
	 * @return return the upper threshold if a user with the email exists, else
	 *         null
	 */
	public int getUpperThreshold(String email) {
		User user = getUserByEmail(email);
		return user.getUpperThreshold();
	}

	/**
	 * Changes the upper threshold of the user identified by email
	 * 
	 * @param email
	 *            the email that identifies the user whose threshold should be
	 *            changed
	 * @param threshold
	 *            the new value for the upper threshold
	 * @return true on success, else false
	 */
	public boolean changeUpperThreshold(String email, int threshold) {
		User user = getUserByEmail(email);
		user.setUpperThreshold(threshold);
		return update(user);
	}
	/**
	 * Returns the lower Threshold of the user identified by the email
	 * 
	 * @param email
	 *            the users email
	 * @return return the lower threshold if a user with the email exists, else
	 *         null
	 */
	public int getLowerThreshold(String email) {
		User user = getUserByEmail(email);
		return user.getLowerThreshold();
	}
	/**
	 * Changes the lower threshold of the user identified by email
	 * 
	 * @param email
	 *            the email that identifies the user whose threshold should be
	 *            changed
	 * @param threshold
	 *            the new value for the lower threshold
	 * @return true on success, else false
	 */
	public boolean changeLowerThreshold(String email, int threshold) {
		User user = getUserByEmail(email);
		user.setLowerThreshold(threshold);
		return update(user);
	}
}
