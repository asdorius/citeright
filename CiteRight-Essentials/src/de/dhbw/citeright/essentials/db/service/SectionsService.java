package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Section;

/**
 * Class that provides basic functionality to mange Section objects
 * 
 * @author Torsten Hopf
 *
 */
public class SectionsService extends AbstractService<Section> {
	/**
	 * Creates a SectionService instance with the given SessionFactory
	 * 
	 * @param sessionFactory
	 *            the sessionFactory that should be used to create Hibernate
	 *            sessions
	 */
	public SectionsService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Creates a SectionsService instance that uses the standard SessinFactory
	 * of the CiteRight application.
	 * 
	 * @see HibernateUtils
	 */
	public SectionsService() {
		super(HibernateUtils.getSessionFactory());
	}

	public List<Section> getSectionsOfDocument(int documentId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Section WHERE Document_ID = :documentId");
		query.setParameter("documentId", documentId);

		List<Section> queryResult = query.list();
		transaction.commit();
		session.close();

		return queryResult;
	}

}
