package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.IncludedRoom;

/**
 * Provides basic functionality for handling IncludedRoom objects
 * 
 * @author Enrico Schrödter
 *
 */
public class IncludedRoomService extends AbstractService<IncludedRoom> {
	/**
	 * Creates an IncludedRoomService instance with the given SessionFactory
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that is used for creating hibernate
	 *            sessions
	 */
	public IncludedRoomService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Default constructor that is mainly used by Hibernate
	 */
	public IncludedRoomService() {
		super(HibernateUtils.getSessionFactory());
	}

	/**
	 * Returns the id of the room that is included
	 * 
	 * @param roomId
	 *            the id of the room
	 * @param includedRoomId
	 *            the id of the included room
	 * @return the room that is included
	 */
	public IncludedRoom get(int roomId, int includedRoomId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM IncludedRoom WHERE RoomId = :roomId AND IncludedRoomId = :includedRoomId");
		query.setParameter("roomId", roomId);
		query.setParameter("includedRoomId", includedRoomId);

		List<IncludedRoom> queryResult = query.list();

		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		} else {
			return null;
		}
	}
}
