package de.dhbw.citeright.essentials.db.service;

import org.hibernate.SessionFactory;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Role;
/**
 * Class that provides basic funtionality to handle Role objects
 * @author Torsten Hopf
 *
 */
public class RoleService extends AbstractService<Role>{
	/**
	 * Creates a RoleService instance with the given Hibernate SessionFactory
	 * @param sessionFactory the SessionFactory that should be used for Hibernate sessions
	 */
	RoleService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	/**
	 * Creates a RoleService instance that uses the standard SessinFactory
	 * of the CiteRight application.
	 * @see HibernateUtils
	 */
	RoleService() {
		super(HibernateUtils.getSessionFactory());
	}

}
