package de.dhbw.citeright.essentials.db.service;

import org.hibernate.SessionFactory;

import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.HibernateUtils;
/**
 * Service that provides basic functionality for Action objects
 * @author Enrico Schrödter
 *
 */
public class ActionService extends AbstractService<Action>{
	/**
	 * Creates a new ActionService that uses the given SessionFactory
	 * @param sessionFactory the Session factory that is used for creating Hibernate sessions
	 */
	ActionService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	/**
	 * Creates an ActionService by using the default SessionFactory used by the CiteRight application.
	 * @see HibernateUtils
	 */
	ActionService() {
		super(HibernateUtils.getSessionFactory());
	}

}
