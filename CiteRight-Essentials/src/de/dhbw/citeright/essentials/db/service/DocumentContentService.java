package de.dhbw.citeright.essentials.db.service;

import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.HibernateUtils;

/**
 * Service that provides basic functionality for handling DocumentContent
 * objects
 * 
 * @author Enrico Schrödter
 *
 */
public class DocumentContentService extends AbstractService<DocumentContent> {
	/**
	 * Creates a DocumentContentService that used the given SessionFactory for
	 * its HibernateSessions
	 * 
	 * @param sessionFactory
	 *            the session factory that provides HibernateSessions
	 */
	public DocumentContentService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Creates a DocumentContentService with the default SessionFactory used by
	 * the CiteRight application
	 * 
	 * @see HibernateUtils
	 */
	public DocumentContentService() {
		super(HibernateUtils.getSessionFactory());
	}

	/**
	 * Returns the DocumentContent objects of a room as List
	 * 
	 * @param roomId
	 *            the id of the room that contains the Documents whose content
	 *            is needed
	 * @return the list of DocumentContent objects
	 */
	public List<DocumentContent> getDocumentContentsOfRoom(int roomId) {
		DocumentService documentService = new DocumentService(sessionFactory);

		List<Document> documents = documentService.getDocumentsInRoom(roomId);
		List<DocumentContent> documentContents = new LinkedList();

		for (Document document : documents) {
			documentContents.add(getDocumentContentByDocumenteId(document
					.getId()));
		}

		return documentContents;
	}

	public DocumentContent getDocumentContentByDocumenteId(Integer documentId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM DocumentContent WHERE File = :documentId");
		query.setParameter("documentId", documentId);

		List<DocumentContent> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		} else {
			return null;
		}
	}

}
