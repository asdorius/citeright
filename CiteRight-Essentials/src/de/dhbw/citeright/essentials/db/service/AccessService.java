package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.HibernateUtils;
/**
 * Service that provides basic funtionality for Access objects
 * @author Enrico Schrödter
 *
 */
public class AccessService extends AbstractService<Access> {

	/**
	 * Creates a new instance of the AccessService
	 * 
	 * @param sessionFactory
	 *            the session factory that should be used for creating a Session
	 */
	public AccessService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	public AccessService() {
		super(HibernateUtils.getSessionFactory());
	}
	

	/**
	 * Returns the Access-object that matched on the given UserId and RoomId
	 * 
	 * @param userId
	 *            the user id
	 * @param roomId
	 *            the room id
	 * @return the Access that matched on the given userId and roomId
	 *         combination.
	 */
	public Access getRoomShare(Integer userId, Integer roomId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Access WHERE User_ID = :userId AND Room_ID = :roomId");
		query.setParameter("userId", userId);
		query.setParameter("roomId", roomId);

		List<Access> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		}

		return null;
	}

	/**
	 * Returns all Access objects that matched on the roomId. This can be used
	 * to get all users that have a room share for the room with the given
	 * roomId.
	 * 
	 * @param roomId
	 *            the room id
	 * @return the Accesses that matched on the given roomId.
	 */
	public List<Access> getRoomSharesForRoom(Integer roomId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Access WHERE Room_ID = :roomId");
		query.setParameter("roomId", roomId);

		List<Access> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult;
		}

		return null;
	}
	
	/**
	 * Returns all Access objects that matched on the userId. This can be used
	 * to get all rooms to which the user with the given userId has a room share to.
	 * 
	 * @param userId
	 *            the user id
	 * @return the Accesses that matched on the given userId.
	 */
	public List<Access> getRoomSharesForUser(Integer userId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Access WHERE User_ID = :userId");
		query.setParameter("userId", userId);

		List<Access> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult;
		}

		return null;
	}

}
