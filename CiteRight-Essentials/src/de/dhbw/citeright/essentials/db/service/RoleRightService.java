package de.dhbw.citeright.essentials.db.service;

import org.hibernate.SessionFactory;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.RoleRight;
/**
 * Class that provides basic functionality to handle RoleRight objects
 * @author Enrico Schrödter
 *
 */
public class RoleRightService extends AbstractService<RoleRight>{
	/**
	 * Creates a RoleRightService instance with the given SessionFactory
	 * @param sessionFactory the SessionFactory that should be used for Hibernate sessions
	 */
	RoleRightService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Creates a RoleRightService instance that uses the standard SessinFactory
	 * of the CiteRight application.
	 * @see HibernateUtils
	 */
	RoleRightService() {
		super(HibernateUtils.getSessionFactory());
	}

}
