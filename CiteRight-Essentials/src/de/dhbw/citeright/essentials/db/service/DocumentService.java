package de.dhbw.citeright.essentials.db.service;

import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Section;

/**
 * Class that handles Document related interaction with the Database
 * 
 * @author Torsten Hopf
 *
 */
public class DocumentService extends AbstractService<Document> {
	/**
	 * Creates an DocumentService instanc with the given SessionFactory
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that is used for Hibernate Sessions
	 */
	public DocumentService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Creates an DocumentService instance with the default SessionFactory used
	 * by the CiteRight application
	 * 
	 * @see HibernateUtils
	 */
	public DocumentService() {
		super(HibernateUtils.getSessionFactory());
	}

	/**
	 * Returns a list with the newest documents
	 * 
	 * @param ageInHours
	 *            the age in hours
	 * @return the list of the documents that have been modified in the last
	 *         hours
	 */
	public List<Document> getNewDocuments(int ageInHours) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Document WHERE timestamp > :uploadData AND parentId != :websiteroom");
		query.setParameter("uploadData", new Date(System.currentTimeMillis()
				- ageInHours * 60 * 60 * 1000));
		query.setParameter("websiteroom", CiteRightConstants.WEBSITE_ROOMID);

		List<Document> queryResult = query.list();
		transaction.commit();
		session.close();

		return queryResult;
	}

	/**
	 * Searches for all documents in the given room.
	 * 
	 * @param roomId
	 *            the ID of the room to search in.
	 * @return a list of document objects representing all documents in the
	 *         given room.
	 */
	public List<Document> getDocumentsInRoom(Integer roomId) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM Document WHERE Parent = :roomId");
		query.setParameter("roomId", roomId);

		List<Document> queryResult = query.list();
		transaction.commit();
		session.close();

		return queryResult;
	}
}
