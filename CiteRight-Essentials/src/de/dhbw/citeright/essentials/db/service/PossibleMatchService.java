package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.PossibleMatch;
import de.dhbw.citeright.essentials.db.Section;

/**
 * Class that gives basic funtionality for handling PossibleMatch objects
 * 
 * @author Enrico Schrödter
 *
 */
public class PossibleMatchService extends AbstractService<PossibleMatch> {
	/**
	 * Creates a PossibleMatchService instance with the given SessionFactory
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that is used for Hibernate sessions
	 */
	public PossibleMatchService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Searches for all documents in the given room.
	 * 
	 * @param roomId
	 *            the ID of the room to search in.
	 * @return a list of document objects representing all documents in the
	 *         given room.
	 */

	public PossibleMatchService() {
		super(HibernateUtils.getSessionFactory());
	}

	/**
	 * Get the PossibleMatch for the given Section identified by its id
	 * 
	 * @param sectionId
	 *            the Section identified by its id
	 * @param minMeasurement
	 *            the minimal plagiarism degree that shall be used
	 * @return the PossibleMatch if there is one that fits the requirements,
	 *         else null
	 */
	public PossibleMatch getPossibleMatchBySection(int sectionId,
			float minMeasurement) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session
				.createQuery("FROM PossibleMatch WHERE Section1 = :section AND Measurement > :measurement ORDER BY Measurement DESC");
		query.setParameter("section", sectionId);
		query.setParameter("measurement", minMeasurement);
		query.setMaxResults(1);

		List<PossibleMatch> queryResult = query.list();

		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		}
		return null;
	}

	/**
	 * Get the PossibleMatch for the given Section
	 * 
	 * @param Section
	 *            the Section that should be used
	 * @param minMeasurement
	 *            the minimal plagiarism degree that shall be used
	 * @return the PossibleMatch if there is one that fits the requirements,
	 *         else null
	 */
	public PossibleMatch getPossibleMatchBySection(Section section,
			float minMeasurement) {
		return getPossibleMatchBySection(section.getId(), minMeasurement);
	}

	/**
	 * Get the PossibleMatch for the given Section
	 * 
	 * @param sectionId
	 *            the Section that has a possible match
	 * @return the PossibleMatch if there is one that fits the requirements,
	 *         else null
	 */
	public PossibleMatch getPossibleMatchBySection(Section section) {
		return getPossibleMatchBySection(section.getId(), 0);
	}
	
	/**
	 * Get the PossibleMatch for the given Section identified by its id
	 * 
	 * @param sectionId
	 *            the Section identified by its id
	 * @return the PossibleMatch if there is one that fits the requirements,
	 *         else null
	 */
	public PossibleMatch getPossibleMatchBySection(int sectionId) {
		return getPossibleMatchBySection(sectionId, 0);
	}
}
