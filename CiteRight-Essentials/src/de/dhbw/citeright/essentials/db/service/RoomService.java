package de.dhbw.citeright.essentials.db.service;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;

/**
 * Class that handles the Room based interactions with the database
 * 
 * @author Enrico Schrödter
 *
 */
public class RoomService extends AbstractService<Room> {
	/**
	 * Creates a new instance of the RoomService
	 * 
	 * @param sessionFactory
	 *            the sessoin factory that should be used for creating a Session
	 */
	public RoomService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}
	/**
	 * Creates a RoomService instance that uses the standard SessinFactory
	 * of the CiteRight application.
	 * @see HibernateUtils
	 */
	public RoomService() {
		super(HibernateUtils.getSessionFactory());
	}
	
	

	/**
	 * Returns the Room that is identified by the given Token
	 * 
	 * @param token
	 *            the Token of the Room
	 * @return the Room that is identified by the given Token
	 */
	public Room getRoomByToken(String token) {
		System.out.println(token);
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session.createQuery("FROM Room WHERE token = :token");
		query.setParameter("token", token);

		List<Room> queryResult = query.list();
		transaction.commit();
		session.close();

		if (queryResult.size() > 0) {
			return queryResult.get(0);
		}

		return null;
	}

	/**
	 * Returns the Rooms that the User has access to. This are Rooms shared
	 * with the User and his own Rooms.
	 * 
	 * @param id
	 *            the Id of the User
	 * @param writableOnly
	 *            Returns only rooms that are writable for the user
	 * @return the accessable Rooms
	 */
	public List<Room> getRoomsForUser(Integer id, boolean writableOnly) {
		// Prepare Hibernate stuff
		Session session = sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		Query ownedQuery = session
				.createQuery("FROM Room WHERE Owner = :ownerId AND ID != :websiteroom");
		ownedQuery.setParameter("ownerId", id);
		ownedQuery.setParameter("websiteroom", CiteRightConstants.WEBSITE_ROOMID);

		// get all Rooms owned by User
		List<Room> ownedRooms = ownedQuery.list();

		// The user is not allowed to write in rooms, that only has access over
		// a room share. So we won't return these rooms if told to skip them.
		if (!writableOnly) {
			// now get all rooms the User has access to
			Query accessablesQuery = session
					.createQuery("FROM Access WHERE User_ID = :accessUserId");
			accessablesQuery.setParameter("accessUserId", id);

			List<Access> accessables = accessablesQuery.list();

			List<Room> accessableRooms = new ArrayList<Room>();
			for (Access access : accessables) {
				accessableRooms.add(getById(access.getRoomId()));
			}

			// merge
			ownedRooms.addAll(accessableRooms);
		}

		tx.commit();
		session.close();
		return ownedRooms;
	}

	/**
	 * Returns the Rooms that the User has access to. This are Rooms shared
	 * with the User and his own Rooms.
	 * 
	 * @param id
	 *            the Id of the User
	 * @return the accessable Rooms
	 */
	public List<Room> getRoomsForUser(Integer id) {
		return getRoomsForUser(id, false);
	}

	/**
	 * Returns the Rooms that the User has access to. This are Rooms shared
	 * with the User and his own Rooms.
	 * 
	 * @param user
	 *            the user that has access to the Rooms
	 * @return the accessable Rooms
	 */
	public List<Room> getRoomsForUser(User user) {
		return getRoomsForUser(user.getId(), false);
	}
	
	public List<Room> getIncludedRooms(Room room){
		return getIncludedRooms(room.getId());
	}
	
	public List<Room> getIncludedRooms(int roomid){
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		Query query = session.createQuery("FROM IncludedRoom WHERE RoomId = :roomId");
		query.setParameter("roomId", roomid);

		List<IncludedRoom> queryResult = query.list();
		
		List<Room> rooms = new LinkedList<Room>();
		
		for(IncludedRoom includeRoom: queryResult){
			rooms.add(getById(includeRoom.getIncludedRoomId()));
		}
		 
		transaction.commit();
		session.close();

		return rooms;
	}
}
