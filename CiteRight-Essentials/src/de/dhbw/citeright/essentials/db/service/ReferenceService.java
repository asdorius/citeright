package de.dhbw.citeright.essentials.db.service;

import org.hibernate.SessionFactory;

import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.Reference;

/**
 * Class that provides basic functionality for handling Reference objects
 * 
 * @author Torsten Hopf
 *
 */
public class ReferenceService extends AbstractService<Reference> {
	/**
	 * Creates a ReferenceService instance with the given SessionFactory
	 * 
	 * @param sessionFactory
	 *            the SessionFactory that should be used for creating Hibernate
	 *            sessions
	 */
	public ReferenceService(SessionFactory sessionFactory) {
		super(sessionFactory);
	}

	/**
	 * Creates a ReferenceService instance that uses the standard SessinFactory
	 * of the CiteRight application.
	 * @see HibernateUtils
	 */
	public ReferenceService() {
		super(HibernateUtils.getSessionFactory());
	}
}
