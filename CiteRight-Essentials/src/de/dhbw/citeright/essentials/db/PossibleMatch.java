package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the PossibleMatch entity of the Database. It tells how
 * equal to sections are.
 * 
 * @author Enrico Schrödter
 *
 */
@Entity
@Table(name = "PossibleMatch")
public class PossibleMatch {
	@Id
	@Column(name = "ID", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	private Integer id;
	@Column(name = "Section1")
	private Integer sectionId1;
	@Column(name = "Section2")
	private Integer sectionId2;
	@Column(name = "Measurement")
	private Float measurement;

	public PossibleMatch() {

	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Integer getSectionId1() {
		return sectionId1;
	}

	public void setSectionId1(int sectionId1) {
		this.sectionId1 = sectionId1;
	}

	public Integer getSectionId2() {
		return sectionId2;
	}

	public void setSectionId2(int sectionId2) {
		this.sectionId2 = sectionId2;
	}

	public Float getMeasurement() {
		return measurement;
	}

	public void setMeasurement(float measurement) {
		this.measurement = measurement;
	}

}
