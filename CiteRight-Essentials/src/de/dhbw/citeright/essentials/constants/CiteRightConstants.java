package de.dhbw.citeright.essentials.constants;

import java.text.SimpleDateFormat;

/**
 * Class that contains relevant constants for CiteRight
 * 
 * @author Torsten Hopf
 *
 */
public class CiteRightConstants {

	/**
	 * ID of the Role waiting for activation
	 */
	public static final Integer WAITING_FOR_ACTIVATION = 3;
	/**
	 * Address of the CiteRight Web App
	 */
	public static final String ADDRESS_WEB_APP = "http://185.82.20.252:8080/CiteRight";
	/**
	 * The Room that is the root for all Rooms
	 */
	public static final Integer ROOT_ROOM_ID = 2;
	/**
	 * ID of the lecturer role
	 */
	public static final Integer LECTURER = 2;
	/**
	 * ID of the administrator role
	 */
	public static final Integer ADMINISTRATOR = 1;
	/**
	 * Location of the akka config file relative to the main directory of the
	 * WebApp
	 */
	public static final String WEBAPP_CONFIG_FILE_LOCATION = "/cfg/akka.config";
	/**
	 * Location of the akka config file relative to the main directory of the
	 * Server
	 */
	public static final String SERVER_CONFIG_FILE_LOCATION = "/cfg/akka.config";
	/**
	 * Akka-Protocol address of the WorkloadServer's main listener.
	 */
	public static final String SERVER_INSTANCE_LOCATION = "akka.tcp://WorkloadServer@127.0.0.1:2553/user/server";
	/**
	 * Status Id that indicates, that the document is currently in plagiarism
	 * audit
	 */
	public static final Integer STATUS_IN_AUDIT = 1;
	/**
	 * Status Id that indicates, that the document is waiting in the audit queue
	 */
	public static final Integer STATUS_WAITING_IN_QUEUE = 2;
	/**
	 * Status Id that indicates, that the audit has finished
	 */
	public static final Integer STATUS_AUDIT_FINISHED = 3;
	/**
	 * Status Id that indicates, that the audit has failed
	 */
	public static final Integer STATUS_AUDIT_FAILED = 4;
	/**
	 * Status Id that indicates, that document status is unknown
	 */
	public static final Integer STATUS_UNKNOWN  = 5;
	/**
	 * Specifies how old a document may be to be called "recent". Value is in
	 * hours.
	 */
	public static final Integer MAXIMUM_AGE_OF_RECENT_FILES = 72;
	/**
	 * Specifies a String that can be used to format a date with {@link SimpleDateFormat}
	 */
	public static final String SIMPLEDATEFORMAT_DATE = "dd.MM.yyyy";
	/**
	 * Specifies a String that can be used to format a date and time with {@link SimpleDateFormat}
	 */
	public static final String SIMPLEDATEFORMAT_DATETIME = "dd.MM.yyyy HH:mm";
	/**
	 * Specifies a String that can be used to format a time with {@link SimpleDateFormat}
	 */
	public static final String SIMPLEDATEFORMAT_TIME = "HH:mm";
	/**
	 * Specifies a String that can be used to format a date as ISO with {@link SimpleDateFormat}
	 */
	public static final String SIMPLEDATEFORMAT_ISO = "yyyy-MM-dd HH:mm:ss";
	/**
	 * Specifies a String that can be used to format a date as filename with {@link SimpleDateFormat}
	 */
	public static final String SIMPLEDATEFORMAT_FILENAME = "yyyyMMdd_HHmmss";
	/**
	 * Specifies a int that represents the room id from the root room.
	 */
	public static final int ROOT_ROOMID = 2;
	/**
	 * Specifies a int that represents the room id from the website room.
	 */
	public static final int WEBSITE_ROOMID = 10;
	/**
	 * Secifies the maximal size of a document.
	 */
	public static final int MAX_FILESIZE = 15000000;

	// To prevent instantiation of Constant class
	private CiteRightConstants() {

		// DONT EVENT THINK ABOUT IT

	}
}
