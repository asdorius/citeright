package de.dhbw.citeright.essentials.helper;

import java.util.Random;

/**
 * Class that is used to generate Tokens
 * 
 * @author Enrico Schrödter
 *
 */
public class RandomStringHelper {
	/**
	 * Returns a random String of the given length. Possible are only lower case
	 * letters a-z and numbers 0-9
	 * 
	 * @param lenght
	 *            the length of the random String to generate
	 * @return a random String of the given length
	 */
	public static String getRandomString(int lenght) {
		char[] values = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j',
				'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v',
				'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7',
				'8', '9' };

		StringBuffer buffer = new StringBuffer();
		Random rnd = new Random();

		for (int i = 0; i < lenght; i++) {
			buffer.append(values[rnd.nextInt(values.length)]);
		}

		return buffer.toString();
	}
}
