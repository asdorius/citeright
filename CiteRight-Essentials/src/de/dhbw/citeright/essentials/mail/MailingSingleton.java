package de.dhbw.citeright.essentials.mail;

import java.util.List;
import java.util.Properties;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.Authenticator;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.util.ByteArrayDataSource;

import de.dhbw.citeright.essentials.constants.CiteRightConstants;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.HibernateUtils;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.db.service.UserService;

/**
 * Class that provides the functionality of sending mails in the context of the
 * Citeright-Application. The official CiteRight mailing account is used. This
 * functionality contains sending mails with status information, password
 * recovery, user activiation and every other context where a mail would be
 * helpful.
 * 
 * @author Torsten Hopf
 *
 */
public class MailingSingleton {

	private static MailingSingleton INSTANCE;

	private Properties properties;
	private final String fromEmail = "CiteRight.Notification@gmail.com";
	private final String password = "DHBWHorb2015";

	private MailingSingleton() {
		properties = new Properties();
		properties.put("mail.smtp.auth", "true");
		properties.put("mail.smtp.port", "587");
		properties.put("mail.smtp.socketFactory.port", "587");
		properties.put("mail.smtp.starttls.enable", "true");
		properties.put("mail.smtp.host", "smtp.gmail.com");
		properties.put("mail.smtp.socketFactory.class",
				"javax.net.ssl.SSLSocketFactory");
	}

	/**
	 * Returns the current instance of the MailingSingleton and creates one
	 * before, if none exists. It is used for sending mails in the CiteRight
	 * context.
	 * 
	 * @return the current instance of the MalingSingleton
	 */
	public static MailingSingleton getInstance() {

		if (INSTANCE == null)
			INSTANCE = new MailingSingleton();

		return INSTANCE;
	}

	/**
	 * Sends a mail to the User's Email Adress, that contains his password.
	 * 
	 * @param user
	 *            the User that needs his email adress.
	 */
	public void sendPasswordToUser(User user) {
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("Ihr Password für CiteRight");

			final String emailText = "Anbei ihr Passwort: <br/> "
					+ user.getPassword();

			message.setContent(emailText, "text/html");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Sends an activation mail to the given user, after it's status changed
	 * from waiting for activation to an activated one.
	 * 
	 * @param user
	 *            the freshly activated user
	 */
	public void sendActivationMailToUser(User user) {

		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		session.setDebug(true);
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("Ihre Aktivierung bei CiteRight");

			final String emailText = "Guten Tag "
					+ user.getFirstName()
					+ ", <br/>"
					+ "ein anderer Nutzer hat Sie für die CiteRight Application freigeschaltet! Von nun an können Sie sich unter"
					+ CiteRightConstants.ADDRESS_WEB_APP
					+ " anmelden!<br/>"
					+ "Dies ist eine automatisch generierte Email, bitte antworten Sie nicht darauf<br/>"
					+ "Viele Grüße,<br/>" + "Ihr CiteRight Team";

			message.setContent(emailText, "text/html");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}

	}

	/**
	 * Sends an email to all users with administrator privileges, that a new
	 * user is waiting for his activation
	 * 
	 * @param newUser
	 *            the user that wants to be activated
	 */
	public void sendMailToAllAdminsAfterRegistration(User newUser) {
		UserService service = new UserService(
				HibernateUtils.getSessionFactory());
		List<User> administrators = service.getAdministrators();
		for (User admin : administrators) {
			sendMailNewUsersWaiting(newUser, admin);
		}
	}

	/**
	 * Sends a mail to the given administrator that a new user wants to get
	 * activated
	 * 
	 * @param newUser
	 *            the user that wants to get activated
	 * @param admin
	 *            the administrator that can activate the user
	 */
	public void sendMailNewUsersWaiting(User newUser, User admin) {
		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		session.setDebug(true);
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(admin.getEmail()));
			message.setSubject("Neuer Registrierungsantrag bei CiteRight");

			final String emailText = "Guten Tag "
					+ admin.getFirstName()
					+ ", <br/>"
					+ "ein anderer Nutzer hat sich für die CiteRight Webapp registriert<br/>"
					+ "Die Daten des Nutzers: <br/>"
					+ "Name:\t "
					+ newUser.getFirstName()
					+ " "
					+ newUser.getLastName()
					+ "<br/>"
					+ "Email:\t"
					+ newUser.getEmail()
					+ "<br/>"
					+ "<a href=\"http:// "
					+ CiteRightConstants.ADDRESS_WEB_APP
					+ "\" /><br/>"
					+ " anmelden!<br/>"
					+ "Dies ist eine automatisch generierte Email, bitte antworten Sie nicht darauf.<br/>"
					+ "Viele Grüße,<br/>" + "Ihr CiteRight Team";

			message.setContent(emailText, "text/html");

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

	/**
	 * Sends an Email after the document process has finished to the User
	 * 
	 * @param user
	 *            the User that should receive the mail
	 * @param documentContent
	 *            the documentContent that ran through the process
	 */
	public void sendMailWithDocumentStatusFinished(User user,
			DocumentContent documentContent, boolean detailedReport) {
		DocumentService dbDocumentService = new DocumentService();
		Document document = dbDocumentService.getById(documentContent
				.getDocumentId());

		String detailedReportEmailContent = "";
		if (detailedReport) {
			detailedReportEmailContent = "Anbei ist ein detailierter Bericht in Form einer PDF.<br/>";
		}

		Session session = Session.getInstance(properties, new Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(fromEmail, password);
			}
		});
		session.setDebug(true);
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(fromEmail));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(user.getEmail()));
			message.setSubject("CiteRight Prüfungsergebnis liegt vor");
			final String emailText = "Guten Tag "
					+ user.getFirstName()
					+ ", <br/>"
					+ "Ein Bericht über das Dokument <br/>"
					+ document.getFilename()
					+ "vom Studenten "
					+ document.getStudentName()
					+ " ist nun verfügbar.<br/>"
					+ "Das Dokument hat einen Plagiatswert von: "
					+ document.getPlagiarismDegree()*100
					+ "%.<br/>"
					+ detailedReportEmailContent
					+ "<br/>"
					+ "<a href=\"http:// "
					+ CiteRightConstants.ADDRESS_WEB_APP
					+ "\" /><br/>"
					+ " anmelden!<br/>"
					+ "Dies ist eine automatisch generierte Email, bitte antworten Sie nicht darauf.<br/>"
					+ "Viele Grüße,<br/>" + "Ihr CiteRight Team";

			BodyPart bodyPart = new MimeBodyPart();
			bodyPart.setText(emailText);
			Multipart multipart = new MimeMultipart();
			multipart.addBodyPart(bodyPart);
			bodyPart = new MimeBodyPart();
			byte[] content = document.getAuditReport();

			if (detailedReport) { // detailed Report -> send report as
									// attachement
				DataSource reportPDF = new ByteArrayDataSource(content,
						"application/pdf");
		         bodyPart.setDataHandler(new DataHandler(reportPDF));
		         bodyPart.setFileName(document.getFilename()+".pdf");
		         multipart.addBodyPart(bodyPart);
		         message.setContent(multipart);
		         
				// initialize reportPDF
			} else { // short report
				message.setContent(emailText, "text/html");
			}

			Transport.send(message);
		} catch (MessagingException e) {
			throw new RuntimeException(e);
		}
	}

}
