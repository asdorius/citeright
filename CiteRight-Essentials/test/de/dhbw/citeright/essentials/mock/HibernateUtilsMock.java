package de.dhbw.citeright.essentials.mock;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.Reference;
import de.dhbw.citeright.essentials.db.RoleRight;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.PossibleMatch;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.essentials.db.User;

/**
 * Singleton that provides the Hibernate SessionFactory for the tests
 * 
 * @author Enrico Schrödter
 *
 */
public class HibernateUtilsMock {
	private static final SessionFactory sessionFactory;

	static {
		try {
			System.out.println(HibernateUtilsMock.class.getResource("/")
					.getPath());
			Configuration configuration = new Configuration()
					.configure(
							HibernateUtilsMock.class
									.getResource("/hibernate.test.cfg.xml"))
						.addAnnotatedClass(Access.class)
						.addAnnotatedClass(Action.class)
						.addAnnotatedClass(Role.class)
						.addAnnotatedClass(User.class)
						.addAnnotatedClass(Room.class)
						.addAnnotatedClass(Document.class)
						.addAnnotatedClass(PossibleMatch.class)
						.addAnnotatedClass(Section.class)
						.addAnnotatedClass(DocumentContent.class)
						.addAnnotatedClass(Access.class)
						.addAnnotatedClass(IncludedRoom.class)
						.addAnnotatedClass(Reference.class)
						.addAnnotatedClass(RoleRight.class);
			StandardServiceRegistryBuilder serviceRegistryBuilder = new StandardServiceRegistryBuilder();
			serviceRegistryBuilder.applySettings(configuration.getProperties());
			ServiceRegistry serviceRegistry = serviceRegistryBuilder.build();
			sessionFactory = configuration.buildSessionFactory(serviceRegistry);
		} catch (Throwable ex) {
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}

	/**
	 * Returns the current SessionFactory instance
	 * 
	 * @return the current instance
	 */
	public static SessionFactory getSessionFactory() {
		return sessionFactory;
	}
}
