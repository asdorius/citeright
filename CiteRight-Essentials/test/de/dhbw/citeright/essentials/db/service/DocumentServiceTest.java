package de.dhbw.citeright.essentials.db.service;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.DocumentService;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class DocumentServiceTest {
	private static DocumentService documentService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(2000);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Room room1 = new Room(1, "MusterROOTOrdner", "ASDF1234", null, 1);
		session.save(room1);
		System.out.println("CREATED " + room1.toString());

		Document file1 = new Document();
		file1.setFilename("Fänger im Roggen");
		file1.setFiletype("text/plain");
		file1.setId(1);
		file1.setParentId(room1.getId());
		file1.setTimestamp(new Timestamp(new Date().getTime()));
		file1.setReferenceId(null);
		file1.setData(new byte[] { 22, 44, 55, 66, 77, 88, 99, 111, 22, 44 });
		session.save(file1);
		
		System.out.println("CREATED " + file1.toString());
		
		file1 = new Document();
		file1.setFilename("Altes Dokument");
		file1.setFiletype("text/plain");
		file1.setId(2);
		file1.setParentId(room1.getId());
		file1.setTimestamp(new Timestamp(0));
		file1.setReferenceId(null);
		file1.setData(new byte[] { 22, 44, 55, 66, 77, 88, 99, 111, 22, 44 });
		session.save(file1);

		System.out.println("CREATED " + file1.toString());

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		documentService = new DocumentService(sessionFactory);
		System.out.println("CREATE DUMMY DATA: END");
	}

	@Test
	public void testCreate() {
		Document toCreate = new Document(234, "Kai im Wald", "Text", new byte[] { 0, 2,
				3, 4 }, new Timestamp(new Date().getTime()), 1);
		Integer id = documentService.create(toCreate);
		Assert.assertEquals("Kai im Wald", documentService.getById(id)
				.getFilename());
	}

	@Test
	public void testGetById() {
		Assert.assertNotNull(documentService.getById(1));
	}

	@Test
	public void testGetAll() {
		Assert.assertTrue(documentService.getAll().size() > 0);
	}

	@Test
	public void testUpdate() {
		Document toUpdate = new Document(88, "Fröhlich einen Holzern", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		Integer id = documentService.create(toUpdate);
		toUpdate.setFilename("Fröhlich einen Weggeholzert");
		documentService.update(toUpdate);
		
		Assert.assertEquals("Fröhlich einen Weggeholzert", documentService
				.getById(id).getFilename());

	}

	@Test
	public void testDeleteInt() {
		Document toDelete = new Document(66, "Heute schon den Kai angerufen?", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		Integer id = documentService.create(toDelete);
		
		documentService.delete(id);
		Assert.assertNull(documentService.getById(id));
	}

	@Test
	public void testDeleteT() {
		Document toDelete = new Document(77, "Mein Boot am Kai", "Text",
				new byte[] { 0, 2, 3, 4 }, new Timestamp(new Date().getTime()),
				1);
		documentService.create(toDelete);
		documentService.delete(toDelete);
		
		Assert.assertNull(documentService.getById(77));
	}
	
	@Test
	public void testGetNewDocuments(){
		List<Document> documents = documentService.getNewDocuments(1);
		
		System.out.print(documents);
		
		Assert.assertNotNull(documents);
	}
	
	@Test
	public void getDocumentsInRoomTest(){
		Assert.assertEquals(true, documentService.getDocumentsInRoom(1).size() >= 1);
	}
	
	@Test
	public void getDocumentsInRoomIsEmptyTest(){
		Assert.assertEquals(false, documentService.getDocumentsInRoom(999).size() > 0);
	}

}
