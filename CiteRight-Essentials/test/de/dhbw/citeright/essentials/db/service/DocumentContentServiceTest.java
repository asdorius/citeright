package de.dhbw.citeright.essentials.db.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.DocumentContent;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class DocumentContentServiceTest {
	private static DocumentContentService documentContentService;
	private static Integer documentId;
	private static Integer documentContentId;
	private static Integer roomId;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(2000);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Room room1 = new Room(1, "MusterROOTOrdner", "ASDF1234", null, 1);
		roomId = (int) session.save(room1);
		System.out.println("CREATED " + room1.toString());

		Document file1 = new Document();
		file1.setFilename("Fänger im Roggen");
		file1.setFiletype("text/plain");
		file1.setId(1);
		file1.setParentId(roomId);
		file1.setTimestamp(new Timestamp(new Date().getTime()));
		file1.setReferenceId(null);
		file1.setData(new byte[] { 22, 44, 55, 66, 77, 88, 99, 111, 22, 44 });
		documentId = (int) session.save(file1);

		System.out.println("CREATED " + file1.toString());
		
		DocumentContent documentContent = new DocumentContent();
		documentContent.setDocumentId(documentId);
		documentContentId = (int) session.save(documentContent);
		
		transaction.commit();
		session.close();
		
		
		documentContentService = new DocumentContentService(sessionFactory);
	}
	
	@Test
	public void getDocumentContentsOfRoomTest(){
		Assert.assertEquals(DocumentContentServiceTest.documentContentId, documentContentService.getDocumentContentsOfRoom(DocumentContentServiceTest.roomId).get(0).getId());
	}
	
	@Test
	public void getDocumentContentsOfRoomIsEmptyTest(){
		Assert.assertEquals(0, documentContentService.getDocumentContentsOfRoom(999).size());
	}
	
	@Test
	public void getDocumentContentByDocumenteIdTest(){
		Assert.assertEquals(DocumentContentServiceTest.documentContentId, documentContentService.getDocumentContentByDocumenteId(DocumentContentServiceTest.documentId).getId());
	}
	
	@Test
	public void getDocumentContentByDocumenteIdIsEmptyTest(){
		Assert.assertNull(documentContentService.getDocumentContentByDocumenteId(999));
	}
}
