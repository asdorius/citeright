package de.dhbw.citeright.essentials.db.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class IncludedRoomServiceTest {
	private static IncludedRoomService includedRoomService;
	private static Integer roomid1;
	private static Integer roomid2;
	private static Integer includedRoomId;
	
	@BeforeClass
	public static void setUp() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(3000);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail2.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Room room1 = new Room(3001,"MusterROOTOrdner", "ASDF1234", null, 3000);
		Room room2 = new Room(3002, "SecondMusterOrdner", "1234QWERT", 1, 3000);
		roomid1 = (int) session.save(room1);
		roomid2 = (int) session.save(room2);
		System.out.println("CREATED " + room1.toString());
		System.out.println("CREATED " + room2.toString());
		
		IncludedRoom includedRoom = new IncludedRoom(roomid1, roomid2);
		includedRoomId = (int) session.save(includedRoom);

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		includedRoomService = new IncludedRoomService(sessionFactory);

		System.out.println("CREATE DUMMY DATA: END");
	}
	
	@Test
	public void getRoomTest(){
		Assert.assertEquals(IncludedRoomServiceTest.includedRoomId, includedRoomService.get(IncludedRoomServiceTest.roomid1, IncludedRoomServiceTest.roomid2).getId());
	}
	
	@Test
	public void getRoomIsEmptyTest(){
		Assert.assertNull(includedRoomService.get(IncludedRoomServiceTest.roomid2, IncludedRoomServiceTest.roomid1));
	}
}
