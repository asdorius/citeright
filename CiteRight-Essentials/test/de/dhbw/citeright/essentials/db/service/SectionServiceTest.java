package de.dhbw.citeright.essentials.db.service;

import java.io.Serializable;
import java.sql.Timestamp;
import java.util.Date;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Document;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.Section;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class SectionServiceTest {
	
	private static SectionsService sectionsService;
	private static Integer documentId;
	private static Integer sectionId1;
	private static Integer sectionId2;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setId(2000);
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		session.save(owner);
		System.out.println("CREATED " + owner.toString());

		Room room1 = new Room(1, "MusterROOTOrdner", "ASDF1234", null, 1);
		session.save(room1);
		System.out.println("CREATED " + room1.toString());

		Document file1 = new Document();
		file1.setFilename("Fänger im Roggen");
		file1.setFiletype("text/plain");
		file1.setId(1);
		file1.setParentId(room1.getId());
		file1.setTimestamp(new Timestamp(new Date().getTime()));
		file1.setReferenceId(null);
		file1.setData(new byte[] { 22, 44, 55, 66, 77, 88, 99, 111, 22, 44 });
		documentId = (int) session.save(file1);
		
		System.out.println("CREATED " + file1.toString());
		
		Section section1 = new Section("1", "1", documentId, 0);
		Section section2 = new Section("2", "2", documentId, 1);
		sectionId1 = (int) session.save(section1);
		sectionId2 = (int) session.save(section2);
		

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		sectionsService = new SectionsService(sessionFactory);
		System.out.println("CREATE DUMMY DATA: END");
	}
	
	@Test
	public void getSectionsOfDocumentTest(){
		Assert.assertEquals(2, sectionsService.getSectionsOfDocument(documentId).size());
		Assert.assertEquals(sectionId1, sectionsService.getSectionsOfDocument(documentId).get(0).getId());
		Assert.assertEquals(sectionId2, sectionsService.getSectionsOfDocument(documentId).get(1).getId());
	}
	
	@Test
	public void getSectionsOfDocumentIsEmptyTest(){
		Assert.assertEquals(0, sectionsService.getSectionsOfDocument(999).size());
	}
}
