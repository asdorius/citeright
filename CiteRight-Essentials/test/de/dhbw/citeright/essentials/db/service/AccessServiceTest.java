package de.dhbw.citeright.essentials.db.service;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class AccessServiceTest {
	private static AccessService accessService;
	private static Integer userId1;
	private static Integer roomId1;
	private static Integer roomId2;
	private static Integer accessId1;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		
		Role role = new Role(1, "Test");
		int roleid = (int) session.save(role);
		
		User user1 = new User("firstName", "lastName", "email@email.de", "password", roleid);
		userId1 = (int) session.save(user1);
		
		Room room1 = new Room("roomname1", "qwertz", null, userId1);
		roomId1 = (int) session.save(room1);
		
		Access access1 = new Access();
		access1.setRoomId(roomId1);
		access1.setUserId(userId1);
		accessId1 = (int) session.save(access1);
		
		
		transaction.commit();
		session.close();
		
		
		accessService = new AccessService(sessionFactory);
	}
	
	@Test
	public void getRoomShareTest(){
		Assert.assertEquals(AccessServiceTest.accessId1, accessService.getRoomShare(AccessServiceTest.userId1, AccessServiceTest.roomId1).getId());
	}
	
	@Test
	public void getRoomShareTestNotExicsting(){
		Assert.assertNull(accessService.getRoomShare(AccessServiceTest.userId1, AccessServiceTest.roomId2));
	}
	
	@Test
	public void getRoomSharesForRoomTest(){
		Assert.assertEquals(AccessServiceTest.accessId1, accessService.getRoomSharesForRoom(AccessServiceTest.roomId1).get(0).getId());
	}
	
	@Test
	public void getRoomSharesForRoomTestNotExisting(){
		Assert.assertNull(accessService.getRoomSharesForRoom(999));
	}
	
	@Test
	public void getRoomSharesForUserTest(){
		Assert.assertEquals(AccessServiceTest.accessId1, accessService.getRoomSharesForUser(AccessServiceTest.userId1).get(0).getId());
	}
	
	@Test
	public void getRoomSharesForUserTestNotExisting(){
		Assert.assertNull(accessService.getRoomSharesForUser(999));
	}
}
