package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Access;
import de.dhbw.citeright.essentials.db.IncludedRoom;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.Room;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.RoomService;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class RoomServiceTest {

	private static RoomService roomServiceDAO;
	private static Integer userId1;
	private static Integer userId2;
	private static Integer roomid3;
	private static int roomid2;
	private static int roomid1;

	@BeforeClass
	public static void setUp() throws Exception {
		SessionFactory sessionFactory = HibernateUtilsMock.getSessionFactory();

		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();

		System.out.println("CREATE DUMMY DATA: START");

		Role role = new Role();
		role.setId(1);
		role.setName("Rolle1");

		session.save(role);
		System.out.println("CREATED " + role.toString());

		User owner = new User();
		owner.setFirstName("Max");
		owner.setLastName("Mustermann");
		owner.setEmail("Kai.Holzer@mustermail2.com");
		owner.setRoleId(role.getId());
		owner.setPassword("sicheres_passwort");
		userId1 = (int) session.save(owner);
		System.out.println("CREATED " + owner.toString());
		
		User owner2 = new User();
		owner2.setFirstName("M2ax");
		owner2.setLastName("Mu2stermann");
		owner2.setEmail("Kai.Holzer@muste234rmail2.com");
		owner2.setRoleId(role.getId());
		owner2.setPassword("sicheres_pa2sswort");
		userId2 = (int) session.save(owner2);
		System.out.println("CREATED " + owner2.toString());

		Room room1 = new Room(3001,"MusterROOTOrdner", "ASDF1234", null, userId1);
		Room room2 = new Room(3002, "SecondMusterOrdner", "1234QWERT", 1, userId1);
		Room room3 = new Room(3002, "asda", "asd", 1, userId2);
		roomid1 = (int) session.save(room1);
		roomid2 = (int) session.save(room2);
		roomid3 = (int) session.save(room3);
		System.out.println("CREATED " + room1.toString());
		System.out.println("CREATED " + room2.toString());
		
		Access access = new Access();
		access.setUserId(userId2);
		access.setRoomId(roomid2);
		session.save(access);
		
		IncludedRoom includedRoom = new IncludedRoom(roomid1, roomid3);
		session.save(includedRoom);
		

		transaction.commit();
		System.out.println("COMMITED");
		session.close();

		RoomServiceTest.roomServiceDAO = new RoomService(sessionFactory);

		System.out.println("CREATE DUMMY DATA: END");
	}

	@Test
	public void testGetRoomByToken() {
		roomServiceDAO.create(new Room("Test","TEST123",1,1));
		Room room = roomServiceDAO.getRoomByToken("TEST123");
		Assert.assertNotNull(room);
		Assert.assertEquals("Test", room.getRoomName());
	}

	@Test
	public void testCreate() {
		System.out.println("START CREATE");
		Room room = new Room(255, "Kais Ordner der Sünden", "M1LF2", 1, 1);
		roomServiceDAO.create(room);
		Assert.assertEquals(room.getRoomName(),
				roomServiceDAO.getById(room.getId()).getRoomName());
		System.out.println("END CREATE");
	}

	@Test
	public void testGetById() {
		System.out.println("START GET BY ID");
		Room room = roomServiceDAO.getById(1);
		Assert.assertEquals("MusterROOTOrdner", room.getRoomName());
		System.out.println("END GET BY ID");
	}

	@Test
	public void testGetAll() {
		System.out.println("START GET ALL");
		List<Room> rooms = roomServiceDAO.getAll();
		Assert.assertTrue(rooms.size() > 1 );
		System.out.println("END GET ALL");
	}

	@Test
	public void testUpdate() {
		System.out.println("START UPDATE");
		Room room = roomServiceDAO.getById(1);
		room.setToken("NEWTOKEN");
		roomServiceDAO.update(room);
		Assert.assertEquals("NEWTOKEN",room.getToken());
		System.out.println("END UPDATE");
		}

	@Test
	public void testDeleteInt() {
		System.out.println("START DELETE ID");
		Room roomToDel = new Room("Badeparadies Schwarzwald","SONNE3",1,1);
		roomServiceDAO.create(roomToDel);
		roomServiceDAO.delete(roomToDel.getId());
		Assert.assertNull(roomServiceDAO.getById(roomToDel.getId()));
		System.out.println("END DELETE ID");
		
	}

	@Test
	public void testDeleteT() {
		System.out.println("START DELETE OBJECT");
		Room room = new Room("Was soll das?","WASDENN",1,1);
		Integer id = roomServiceDAO.create(room);
		roomServiceDAO.delete(room);
		Assert.assertNull(roomServiceDAO.getById(id));
		System.out.println("END DELETE OBJECT");
	}
	
	@Test
	public void getRoomsForUserWriteOnlyTest() {
		Assert.assertEquals(1, roomServiceDAO.getRoomsForUser(RoomServiceTest.userId2, true).size());
		Assert.assertEquals(RoomServiceTest.roomid3, roomServiceDAO.getRoomsForUser(RoomServiceTest.userId2, true).get(0).getId());
	}
	
	@Test
	public void getRoomsForUserTest() {
		Assert.assertEquals(2, roomServiceDAO.getRoomsForUser(RoomServiceTest.userId2).size());
	}
	
	@Test
	public void getRoomsForUserIsEmptyTest() {
		Assert.assertEquals(0, roomServiceDAO.getRoomsForUser(999).size());
	}
	
	@Test
	public void getIncludedRoomsTest() {
		Assert.assertEquals(RoomServiceTest.roomid3, roomServiceDAO.getIncludedRooms(RoomServiceTest.roomid1).get(0).getId());
	}

}
