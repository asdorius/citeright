package de.dhbw.citeright.essentials.db.service;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.Action;
import de.dhbw.citeright.essentials.db.Role;
import de.dhbw.citeright.essentials.db.RoleRight;
import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.db.service.UserService;
import de.dhbw.citeright.essentials.mock.HibernateUtilsMock;

public class UserServiceTest{

	private static UserService userDAO;
	private static Integer userId1;
	private static Integer userId2;
	private static int userId3;
	private static int actionId;
	private static int roleId;
	private static User user;
	private static Action action1;
	private static User user2;
	private static SessionFactory sessionFactory;

	@BeforeClass
	public static void setUp(){
        sessionFactory = HibernateUtilsMock.getSessionFactory();
        
        Session session = sessionFactory.openSession();
        Transaction transaction = session.beginTransaction();
        
        System.out.println("CREATE DUMMY DATA: START");
        
        Role role = new Role();
        role.setId(1);
        role.setName("Rolle 1");
        roleId = (int) session.save(role);
        
        user = new User();
        user.setId(1);
        user.setFirstName("Vorame");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(roleId);
        user.setPassword("Passwort");
        userId1 = (int) session.save(user);
        
		user2 = new User();
        user2.setId(2);
        user2.setFirstName("Vorname");
        user2.setLastName("Nachname");
        user2.setEmail("test2@test.de");
        user2.setRoleId(1);
        user2.setPassword("Passwort");
        userId2 = (int) session.save(user2);
        
		User user3 = new User();
		user3.setId(2);
		user3.setFirstName("Vorname");
		user3.setLastName("Nachname");
		user3.setEmail("test123@test.de");
		user3.setRoleId(1);
		user3.setPassword("Passwort");
        userId3 = (int) session.save(user3);
        
        System.out.println("CREATE DUMMY DATA: END");
        
        transaction.commit();
        session.close();
		
        UserServiceTest.userDAO = new UserService(sessionFactory);
	}
	
	@Test
	public void getNullUserById(){
		User user = userDAO.getById(1000);
		Assert.assertEquals(null, user);
	}
	
	@Test
	public void getExistingUserById(){
		User user = userDAO.getById(1);
		Assert.assertEquals(1, user.getId().intValue());
	}
	
	@Test
	public void getAllUsers(){
		List<User> users = userDAO.getAll();
		
		Assert.assertNotNull(users);
	}
	
	@Test
	public void createUser(){
		User user = new User();
		user.setEmail("mail1@mail.de");
		user.setFirstName("Firstname");
		user.setLastName("Lastname");
		user.setPassword("Passwort");
		user.setRoleId(1);
		
		int id = userDAO.create(user);
		
		Assert.assertEquals(userDAO.getById(id).getEmail(), user.getEmail());
	}
	
	@Test
	public void updateExistingUser(){
        User user = new User();
        user.setId(1);
        user.setFirstName("Neuer Vorname");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        userDAO.update(user);
		
		Assert.assertEquals("Neuer Vorname", userDAO.getById(1).getFirstName());
	}
	
	@Test
	public void updateNotExisitingUser(){
        User user = new User();
        user.setId(1000);
        user.setFirstName("Neuer Vorname");
        user.setLastName("Nachname");
        user.setEmail("test@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        Assert.assertEquals(false, userDAO.update(user));
	}
	
	@Test
	public void deleteExistingUser(){
		User user = new User();
        user.setId(2);
        user.setFirstName("Vorname");
        user.setLastName("Nachname");
        user.setEmail("test2@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        userDAO.delete(user);
        
        Assert.assertEquals(null, userDAO.getById(2));
	}
	
	@Test
	public void deleteNotExisitingUser(){
		User user = new User();
        user.setId(1000);
        user.setFirstName("Vorname");
        user.setLastName("Nachname");
        user.setEmail("test2@test.de");
        user.setRoleId(1);
        user.setPassword("Passwort");
        
        Assert.assertEquals(false, userDAO.delete(user));
	}
	
	@Test
	public void testValidLogin(){
		boolean result = userDAO.login("test@test.de", "Passwort");
		
		Assert.assertEquals(true, result);
	}
	
	@Test
	public void testInvalidLogin(){
		boolean result = userDAO.login("test@test.de", "Passwort123");
		
		Assert.assertEquals(false, result);
	}
	
	@Test
	public void testNotExistingUserLogin(){
		boolean result = userDAO.login("test@tes", "Passwort");
		
		Assert.assertEquals(false, result);
	}
	
	@Test
	public void testGetExistingUserByEmail(){
		User user = userDAO.getUserByEmail("test@test.de");
		
		Assert.assertEquals(1, user.getId().intValue());
	}
	
	@Test
	public void testGetNotExistingUserByEmail(){
		User user = userDAO.getUserByEmail("test@test.de123123");
		
		Assert.assertEquals(null, user);
	}
	
	@Test
	public void changeEmailTest(){
		String newEmail = "blu@bla.de";
		userDAO.getById(UserServiceTest.userId3).getEmail();
		userDAO.changeEmail(userDAO.getById(UserServiceTest.userId3).getEmail(), newEmail);
		Assert.assertEquals(newEmail, userDAO.getById(UserServiceTest.userId3).getEmail());
	}
	
	@Test
	public void changeFirstNameTest(){
		String newFirstName = "Blu";
		User user = userDAO.getById(UserServiceTest.userId3);
		userDAO.changeFirstName(user.getEmail(), newFirstName);
		Assert.assertEquals(newFirstName, userDAO.getById(UserServiceTest.userId3).getFirstName());
	}
	
	@Test
	public void changeLastNameTest(){
		String newLastName = "Blu";
		User user = userDAO.getById(UserServiceTest.userId3);
		userDAO.changeLastName(user.getEmail(), newLastName);
		Assert.assertEquals(newLastName, userDAO.getById(UserServiceTest.userId3).getLastName());
	}
	
	@Test
	public void changePasswordTest(){
		String newPassword = "Blu";
		User user = userDAO.getById(UserServiceTest.userId3);
		userDAO.changePassword(user.getEmail(), newPassword);
		Assert.assertEquals(newPassword, userDAO.getById(UserServiceTest.userId3).getPassword());
	}
	
	@Test
	public void resetPasswordTest(){
		User user = userDAO.getById(UserServiceTest.userId3);
		userDAO.resetPassword(user.getEmail());
		Assert.assertNotEquals(user.getPassword(), userDAO.getById(UserServiceTest.userId3).getPassword());
	}
}
