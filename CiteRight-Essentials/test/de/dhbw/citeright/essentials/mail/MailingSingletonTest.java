package de.dhbw.citeright.essentials.mail;

import static org.junit.Assert.fail;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import de.dhbw.citeright.essentials.db.User;
import de.dhbw.citeright.essentials.mail.MailingSingleton;

public class MailingSingletonTest {

	
	@Ignore
	@Test
	public void testGetInstance() {
		MailingSingleton mailer = MailingSingleton.getInstance();
		Assert.assertNotNull(mailer);
	}
	@Ignore
	@Test
	public void testSendPasswordToUser() {
		MailingSingleton mailer = MailingSingleton.getInstance();
		Assert.assertNotNull(mailer);
		User user = new User(1,"Torsten","Hopf","i13009@hb.dhbw-stuttgart.de","1234");
		mailer.sendActivationMailToUser(user);
	}
	@Ignore
	@Test
	public void testSendActivationMailToUser() {
		MailingSingleton mailer = MailingSingleton.getInstance();
		Assert.assertNotNull(mailer);
		User user = new User(1,"Torsten","Hopf","i13009@hb.dhbw-stuttgart.de","1234");
		mailer.sendActivationMailToUser(user);	}

	@Test
	@Ignore
	public void testSendMailWithDocumentStatusFinished() {
		fail("Not yet implemented");
	}

}
