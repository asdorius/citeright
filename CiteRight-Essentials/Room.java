package de.dhbw.citeright.essentials.db;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

/**
 * Class that represents the Room Entity of the Database
 * 
 * @author Torsten Hopf
 *
 */
@Entity
@Table(name = "Room")
public class Room {
	@Id
	@Column(name = "Id", unique = true, nullable = false)
	@GenericGenerator(name = "gen", strategy = "increment")
	@GeneratedValue(generator = "gen")
	Integer id;
	@Column(name = "Name")
	String roomName;
	@Column(name = "Token", unique = true)
	String token;
	@Column(name = "Parent")
	Integer parentId;
	@Column(name = "Owner")
	Integer ownerId;

	/**
	 * Creates a Room object with the given information
	 * 
	 * @param id
	 *            the id of the Room
	 * @param roomName
	 *            the name of the Room
	 * @param token
	 *            the Token that identifies the Room
	 * @param parentId
	 *            the Id of the parent Room
	 * @param ownerId
	 *            the Id of the User that owns the Room
	 */
	public Room(Integer id, String roomName, String token, Integer parentId,
			Integer ownerId) {
		this.id = id;
		this.roomName = roomName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}
	/**
	 * Creates a Room object with the given information
	 * 
	 * @param roomName
	 *            the name of the Room
	 * @param token
	 *            the Token that identifies the Room
	 * @param parentId
	 *            the Id of the parent Room
	 * @param ownerId
	 *            the Id of the User that owns the Room
	 */
	public Room(String roomName, String token, Integer parentId, Integer ownerId) {
		this.roomName = roomName;
		this.token = token;
		this.parentId = parentId;
		this.ownerId = ownerId;
	}
	/**
	 * Default constructor mainly used by Hibernate
	 */
	public Room() {

	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Room [id=" + id + ", roomName=" + roomName + ", token="
				+ token + ", parentId=" + parentId + ", ownerId=" + ownerId
				+ "]";
	}
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the roomName
	 */
	public String getRoomName() {
		return roomName;
	}
	/**
	 * @param roomName the roomName to set
	 */
	public void setRoomName(String roomName) {
		this.roomName = roomName;
	}
	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the parentId
	 */
	public Integer getParentId() {
		return parentId;
	}
	/**
	 * @param parentId the parentId to set
	 */
	public void setParentId(Integer parentId) {
		this.parentId = parentId;
	}
	/**
	 * @return the ownerId
	 */
	public Integer getOwnerId() {
		return ownerId;
	}
	/**
	 * @param ownerId the ownerId to set
	 */
	public void setOwnerId(Integer ownerId) {
		this.ownerId = ownerId;
	}
	

}